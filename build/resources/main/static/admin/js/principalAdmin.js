


var principal = {
    
	global : {
		editResult : null,
		urlApp : null
	},
    
    init: function () {
        
        if(location.href.indexOf("localhost") < 0){
			principal.global.urlApp = "http://35.196.101.193:8080/sinpecado/"
		}else{
			principal.global.urlApp = "http://localhost:8080/";
		}
        
        principal.items.init();
		template.init();
        
    },

	items : {
		
		init : function(){
				
			
				
			principal.items.listarItems();
			//principal.items.listeners.init();
		},
		
		openModalCreate : function(afterFunction, input){
			
			//$("#modal-create-item-content #code").prop('disabled',true);
			
			//$("#createModal").modal('show');
			
			$("#modal-create-item-content").empty();
						
			$("#modal-create-item-content").load( "crearItems", function() {
				
				afterFunction(input.lang);
				
			  	$("#modal-create-item-content #code").prop('disabled',false);
				$("#createModal").modal('show');
				
				
			});
			
			
			
		},
		
		getModalItem : function(jsonRequest){
			
			var params = {
							url : principal.global.urlApp+"items/getAllItems",
							requestBody : jsonRequest,
							customLoad: "afterGetAllItems"
						};
			
			
			template.ajax.post.get(params, principal.items, "modal-create-item-content");
			
			
			
		},
		
		afterGetAllItems : function(result) {
			
			var buttonPrev 	 = null;
			var buttonNext 	 = null;
			var divImageItem = $("#divImageItem");
			var itemData 	 = result.data.content[0];
			
			var divCarouselPrincipal = $("<div>", 
				{	
					"class" : "carousel slide",
					"data-ride" : "carousel",
					"id" : "carouselExampleInterval"
				}
			);
			
			
			var divCarousel = $("<div>", 
				{	
					"class" : "carousel-inner"
				}
			);
			
			$.each(itemData.listItemPhotos ,function(index, image){
				
				var urlImage = image.url + "/" + image.name;
				
				var img = $("<img>", 
					{	
						"src" : 'extrasources/images/' + urlImage, 
						"class" : "card-img-top"
					}
				);
				
				var divItemImage = $("<div>", 
					{	
						"data-interval" : "3000", 
						"class" : "carousel-item" + (index === 0 ? " active" : "")
					}
				).append(img);
				
				divCarousel.append(divItemImage);
			});
			
			
			buttonPrev = $("<a>", 
				{	
					"class" : "carousel-control-prev bg-danger text-white",
					"href" : "#carouselExampleInterval",
					"role" : "button",
					"data-slide" : "prev"
				}
			).append(
				$("<span>", 
				{	
					"class" : "carousel-control-prev-icon",
					"aria-hidden" : true
				}
			),
			$("<span>", 
				{	
					"class" : "sr-only"
				}
			).html("Previous"));
			
			buttonNext = $("<a>", 
				{	
					"class" : "carousel-control-next bg-danger text-white",
					"href" : "#carouselExampleInterval",
					"role" : "button",
					"data-slide" : "next"
				}
			).append(
				$("<span>", 
				{	
					"class" : "carousel-control-next-icon",
					"aria-hidden" : true
				}
			),
			$("<span>", 
				{	
					"class" : "sr-only"
				}
			).html("Next"));
			
			
			
			divCarouselPrincipal.append(divCarousel);
			
			divCarouselPrincipal.append(buttonPrev, buttonNext);
			
			divImageItem.append(divCarouselPrincipal);
			
			/*divImageItem.append(
				$("<img>", 
					{	
						"src" : 'extrasources/images/' + urlImage, 
						"class" : "card-img-top"
					}
				)
			);*/
		},
		
		createItem : function(jsonItem, contentId){
			
			var jsonRequest 	= {};
			var result			= null;
			var params			= null;
			var data			= null;
			var anexosForm 		= $("input[type=file]");
			
			jsonRequest.items = [];
			jsonRequest.items.push(jsonItem);
			
			params = {
							url : principal.global.urlApp+"items/itemsPublishing",
							contentId : contentId
						};
			
			
			data = new FormData();
			
			data.append("items", JSON.stringify(jsonRequest));
			
			
			$.each(anexosForm,function(index, fileItem){
				
				data.append("files", fileItem.files[0]);
				
			});
			
			result = template.ajax.post.sendFormData(params, principal.items.afterSave, data);
			
		},
		
		afterSave: function(){
			
			$("#createModal").modal('hide');
			
			principal.items.listarItems();
			
		},
		
		listarItems: function(filterParams) {
			
			if(filterParams == undefined || filterParams == null){
				filterParams = {};
			}else{
				filterParams = filterParams;
			}
        
			var params = {
							url : principal.global.urlApp+"items/getAllItems",
							filter : filterParams,
							div : "adminBody", 
				            paginator : {
											size : 10
										},
							editKey : "code",
							classEdit : "edit-item",
							afterrender: {
								listeners: {
									action : principal.items.listeners.init
								}
							},
				            columns : [
				                        {
				                            title : "id",
				                            column : "idItem"
				                        },
				                        {
				                            title : "Codigo",
				                            column : "code"
				                        },
				                        {
				                            title : "Descripción",
				                            column : "description"
				                        },
				                        {
				                            title : "Precio",
				                            column : "price"
				                        },
				                        {
				                            title : "Estado",
				                            column : "status"
				                        }
				
				                    ],
				             editar : ""
				            
				        };
	
			template.html.autoTable(params);
			
	        
	    },

		getDataModal: function(value){
			
			var parametros = {};
						
			parametros.code = value; 
			
			principal.items.getModalItem(parametros);
			
		},

		listeners : {
			
			init : function(){
				principal.items.listeners.buttons.init();
			},
			
			buttons : {
				
				init : function(){
					
					principal.items.listeners.buttons.btnCreate();
					principal.items.listeners.buttons.btnFilter();
					principal.items.listeners.buttons.btnCreateItem();
					principal.items.listeners.buttons.btnEditItem();
					
					
				},
				
				btnFilter : function(){
					
					$( "#btnFilter").unbind( "click" );
					$("#btnFilter").click(function(){
						
						$("#modal-filter-item-content").empty();
						
						$("#modal-filter-item-content").load( "filtrarItems", function() {
							
						  $("#filtroModal").modal('show');
						  principal.items.listeners.buttons.btnFilterItem();
				
						});
						
					});
				},
				
				btnCreate : function(){
					
					$( "#btnCreate").unbind( "click" );
					$("#btnCreate").click(function(){
						
						$("#modal-create-item-content").empty();
						
						$("#modal-create-item-content").load( "crearItems", function() {
							
						  	$("#modal-create-item-content #code").prop('disabled',false);
							$("#createModal").modal('show');
							
							
						});
						
						//$("#modal-create-item-content #code").prop('disabled',false);
						  //$("#createModal").modal('show');
						
						
						
					});
				},
				
				btnFilterItem : function(){
					
					$( "#btnFilterItem").unbind( "click" );
					$("#btnFilterItem").click(function(){
						
						var parametros = template.html.getAllValues("modal-filter-item-content");
						
						principal.items.listarItems(parametros);
						
						$("#filtroModal").modal('hide');
					});
					
				},
				
				btnCreateItem : function(){
					
					$( "#btnCreateItem").unbind( "click" );
					$("#btnCreateItem").click(function(){
						
						var contentId = "modal-create-item-content";
						var parametros = template.html.getAllValues(contentId);
						
						principal.items.createItem(parametros, contentId);
						
						
					});
					
				},
				
				btnEditItem : function(){
					
					
					$( ".edit-item").unbind( "click" );
					$(".edit-item").click(function(){
						
						principal.items.openModalCreate(principal.items.getDataModal, this);
						
						
					});
					
				}
				
			}
		}
		
		

	}

    

};
