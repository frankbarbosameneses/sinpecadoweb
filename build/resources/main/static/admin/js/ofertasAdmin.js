
{}

var principal = {
    
	global : {
		editResult : null
	},
    
    init: function () {
        
		$('#createModal #selectCode').autoComplete({
		    resolver: 'custom',
		    events: {
		        search: function (qry, callback) {
		            // let's do a custom ajax call
		            $.ajax(
		                'http://localhost:8080/items/get/getAllItems',
		                {
		                    data: { 'description': qry}
		                }
		            ).done(function (res) {
			
						var dataConvert = [];
						
						$.each(res.data.content, function(index, data){
							var jsonData = {};
							
							jsonData.value = data.code;
							jsonData.text = data.code + " - " + data.description;
							
							dataConvert.push(jsonData);
						});
						
		                callback(dataConvert)
		            });
		        },
				
		    }
		});
		
		
		
		$('#createModal #selectCode').on('autocomplete.select', function(event, item) {
			$('#createModal #code').val(item.value);
		});
        
        principal.ofertas.init();
        
    },

	after : {
				
		ajaxLoad : function(data){
			
			var fechaInicial = data.data.content[0].initialDate.toString().split(".")[0];
			var fechaFinal = data.data.content[0].finalDate.toString().split(".")[0];
			
			$('#createModal #initialDate').val(fechaInicial);
			$('#createModal #finalDate').val(fechaFinal);
		
			$('#createModal #selectCode').autoComplete('set', { value: data.data.content[0].itemCode, text: data.data.content[0].itemCode +"-"+data.data.content[0].itemDescription });
		}
	},

	ofertas : {
		
		init : function(){
			
			
			principal.ofertas.listarOfertas();
			//principal.ofertas.listeners.init();
		},
		
		openModalCreate : function(data){
			
			$("#modal-create-oferta-content #selectCode").prop('disabled',true);
			
			$("#modal-create-oferta-content #initialDate").prop('disabled',true);
			
			$("#modal-create-oferta-content #finalDate").prop('disabled',true);
			
			$("#createModal").modal('show');
			
		},
		
		getModalOferta : function(jsonRequest){
			
			var params = {
							url : "http://localhost:8080/offers/getAllOfertas",
							requestBody : jsonRequest
				
						};
			
			
			template.ajax.post.get(params, principal, "modal-create-oferta-content");
			
			principal.ofertas.openModalCreate();
			
		},
		
		createOferta : function(jsonOferta, contentId){
			
			var jsonRequest = {};
			var result		= null;
			
			jsonRequest = jsonOferta;
			
			var params = {
							url : "http://localhost:8080/offers/offerRegister",
							requestBody : jsonRequest,
							contentId : contentId,
							resetValue : true,
							activeMsg : true
						};
			
			
			result = template.ajax.post.send(params, principal.ofertas.listarOfertas);
			
			$("#createModal").modal('hide');
			
		},
		
		listarOfertas: function(filterParams) {
			
			if(filterParams == undefined || filterParams == null){
				filterParams = {};
			}else{
				filterParams = filterParams;
			}
        
			var params = {
							url : "http://localhost:8080/offers/getAllOfertas",
							filter : filterParams,
							div : "adminBody", 
				            paginator : {
											size : 10
										},
							editKey : "idOffer",
							classEdit : "edit-oferta",
							afterrender: {
								listeners: {
									action : principal.ofertas.listeners.init
								}
							},
				            columns : [
				                        {
				                            title : "id",
				                            column : "idOffer"
				                        },
				                        {
				                            title : "Descripción",
				                            column : "description"
				                        },
										{
				                            title : "Fecha inicial",
				                            column : "initialDate",
											type : "date"
				                        },
										{
				                            title : "Fecha Final",
				                            column : "finalDate",
											type : "date"
				                        },
				                        {
				                            title : "Estado",
				                            column : "status"
				                        }
				
				                    ],
				             editar : ""
				            
				        };
	
			template.html.autoTable(params);
	        
	    },

		listeners : {
			
			init : function(){
				principal.ofertas.listeners.buttons.init();
			},
			
			buttons : {
				
				init : function(){
					
					principal.ofertas.listeners.buttons.btnCreateOferta();
					principal.ofertas.listeners.buttons.btnCreate();
					principal.ofertas.listeners.buttons.btnFilter();
					principal.ofertas.listeners.buttons.btnFilterOferta();
					principal.ofertas.listeners.buttons.btnEditOferta();
				},
				
				btnFilter : function(){
					
					$("#btnFilter").click(function(){
						
						$("#filtroModal").modal('show');
						
					});
				},
				
				btnCreate : function(){
					
					$("#btnCreate").click(function(){
						$("#modal-create-oferta-content #code").prop('disabled',false);
						$("#createModal").modal('show');
						
					});
				},
				
				btnFilterOferta : function(){
					
					$("#btnFilterOferta").click(function(){
						
						var parametros = template.html.getAllValues("modal-filter-oferta-content");
						
						principal.ofertas.listarOfertas(parametros);
						
						$("#filtroModal").modal('hide');
					});
					
				},
				
				btnCreateOferta : function(){
					
					$("#btnCreateOferta").click(function(){
						
						var contentId = "modal-create-oferta-content";
						var parametros = template.html.getAllValues(contentId);
						
						principal.ofertas.createOferta(parametros, contentId);
						
						
					});
					
				},
				
				btnEditOferta : function(){
					
					$(".edit-oferta").click(function(){
						
						var parametros = {};
						
						parametros.idOffer = this.lang; 
						
						principal.ofertas.getModalOferta(parametros);
						
						
					});
					
				}
				
			}
		}
		
		

	}

    

};
