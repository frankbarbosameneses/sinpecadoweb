
var template = {
	
	init: function(){
		template.action.listeners.init();
	},

    global: {
        page: 0
    },

    html: {
	
		validateInputs : function(contentId, visibleMsg){
			
			var jsonResponse	= {};
			var typeMsg			= null;
			var titleMsg		= null;
			var arrayInputs 	= $("#"+contentId+" input[required]");
			var arrayVisibles 	= [];
			var messagge	  	= '<div>Antes de continuar debe ingresar los siguientes Campos: </div>';
			
			messagge += '<ul class="text-left"><br>';
			
			$.each(arrayInputs, function(index, input){
				
				 var isVisible = $("div[for="+input.id+"]").filter(".invalid-feedback").is(":visible");
				 
				 if(isVisible){
					arrayVisibles.push(input);
				}
				
			});
			
			$.each(arrayVisibles, function(index, input){
				
				messagge += '<li>' + $("label[for="+input.id+"]").html() + '</li>';
				
			});
			
			messagge += '</ul>';
			
			if(visibleMsg){
				
				if(arrayVisibles.length > 0){
					
					titleMsg = "Campos faltantes"
					typeMsg = "info";
			
			
					Swal.fire(titleMsg,
							  messagge,
							  typeMsg);
							  
							  
					
				}	
				
			}
			
			if(arrayVisibles.length > 0){
					
			jsonResponse.valid = false;
			jsonResponse.messagge = messagge;
			jsonResponse.requiredInputs = arrayVisibles;
			
			}else{
				jsonResponse.valid = true;
			}	
				
			return jsonResponse
				
		
			
		},
	
		restAllValues : function(contentId){
			
			$( "#"+contentId+" :input" ).map(function() {
				
				$(this).val("");
				
				if($(this)[0] != undefined &&  $(this)[0].type != undefined){
					
					if( $(this)[0].type == "file"){
						$("label[for="+$( this )[0].id+"]").html("Seleccione Imagen");
						$("#"+$( this )[0].lang).val("");
					}
					
				}
			
	
			  }).get();
		},
	
		setAllValues : function(contentId, data) {
			
			var jsonContent = data.data.content;
			
			$( "#"+contentId+" :input" ).map(function() {
				
				if(this.id != ""){
					var value = eval("jsonContent[0]."+this.id);
				
					if(value !== undefined){
						$(this).val(eval("jsonContent[0]."+this.id));
					}
				}
				
			  }).get();

		},
	
		getAllValues : function(contentId) {
			
			var jsonResult 		= null;
			
		    var inputValues = $( "#"+contentId+" :input" ).map(function() {
			
								var result = null;
								
								if($(this)[0].type == "file"){
									
								}else if($(this)[0].type == "datetime-local"){
									
									var value = $(this).val();
									
									value = value.toString().split(".")[0];
									
									value = value.replace("T", " ");
									
									result = '"' + this.id + '" : "' + value + '"';
									
								}else{
									result = '"' + this.id + '" : "' + $(this).val() + '"';
								}
									
									
								return result;
					
							  });


			jsonResult = JSON.parse('{'+inputValues.get().join(" , ")+'}');
			
			$.each(Object.keys(jsonResult), function (indexColumn, column) {
				
				if(column == ""){
					delete jsonResult[column];
				}else{
					var value = eval("jsonResult."+column);
				
					if(value === ""){
						delete jsonResult[column];
					}
				}
				
				
			});
			
			return jsonResult;

		},
	
        tables: function (params) {

        },

        autoTable: function (params) {

            template.ajax.post.getGrid(params);
        },

        paginator: function (params) {

            var idPaginador = null;
            var page1 = (parseInt(template.global.page));
            var page2 = (parseInt(template.global.page) + 1);
            var page3 = (parseInt(template.global.page) + 2);
            
            var index1 = (parseInt(template.global.page) + 1);
            var index2 = (parseInt(template.global.page) + 2);
            var index3 = (parseInt(template.global.page) + 3);

            html = '<nav aria-label="Page navigation example">';
            html += '<ul class="pagination justify-content-center">';


            if (parseInt(template.global.page) === 0) {
                html += '<li class="page-item page-item-prev disabled">';
                html += '<a class="page-link" tabindex="-1" aria-disabled="true">Previous</a>';
                html += '</li>';
            } else {
                html += '<li class="page-item page-item-prev">';
                html += '<a class="page-link" tabindex="-1">Previous</a>';
                html += '</li>';
            }

            html += '<li class="page-item page-item-clic active" lang="' + page1 + '" disabled><a class="page-link">' + index1 + '</a></li>';
            html += '<li class="page-item page-item-clic" lang="' + page2 + '" disabled><a class="page-link">' + index2 + '</a></li>';
            html += '<li class="page-item page-item-clic" lang="' + page3 + '" disabled><a class="page-link">' + index3 + '</a></li>';
            
            html += '<li class="page-item page-item-next">';
            html += '<a class="page-link" href="#">Next</a>';
            html += '</li>';
            
            html += '</ul>';
            html += '</nav>';

            if (params !== undefined && params !== null && params.idPaginador !== undefined) {
                idPaginador = params.idPaginador;
            } else {
                idPaginador = "paginador";
            }

            $("#" + idPaginador).html(html);

            $(".page-item-clic").click(function (event) {
                var value = this.lang;

                $(this).addClass("active");
                template.global.page = parseInt(value);
                template.html.autoTable(params);
            });
            
            $(".page-item-prev").click(function (event) {
                template.global.page--;
                template.html.autoTable(params);
            });
            
            $(".page-item-next").click(function (event) {
                template.global.page++;
                template.html.autoTable(params);
            });
        }

    }

    ,

    json: {
	
        getKeys: function (data) {

            return Object.keys(data[0]);
        }
    },

    action: {
	
        onclickPaginator: {

        },
		
		listeners : {
			
			init : function(){
				
				template.action.listeners.button.btnCloseModal();
				
			},
			
			button : {
				
				btnCloseModal : function(){
					$(".btn-close-modal").click(function(){
						
						template.html.restAllValues($( this ).parent().siblings().filter(".modal-body")[0].id);
						

					});
				}
			}
		}
    },

    ajax: {
	
        post: {
			
			sendFormData : function(params, execute, formData){
				
				jQuery.ajax({
	                url: params.url,
	                data: formData,
				    cache: false,
				    contentType: false,
				    processData: false,
				    method: 'POST',
				    type: 'POST',
	                success: function (result) {
		
		
						execute();
						
						template.html.restAllValues(params.contentId);
	
	                    if(result.success){
							Swal.fire(
									  'Exitoso',
									  'Creado con exito',
									  'success'
									);
						}else{
							Swal.fire(
									  'Precaucion',
									  result.error,
									  'warning'
									);
						}
	
	                },
	                failure: function (error, msg) {
	                    Swal.fire(
									  'Error',
									  'Error al crear Item',
									  'error'
									);
	                }
	            });
			},
			
			send : function(params, execute){
				
				var paramsPost 	= params.requestBody;
	
	            jQuery.ajax({
	                url: params.url,
	                data: JSON.stringify(paramsPost),
	                cache: false,
	                contentType: "application/json",
	                processData: false,
	                method: 'POST',
	                type: 'POST',
	                success: function (result) {
		
						var typeMsg 	= null;
						var titleMsg 	= null;
						var textMsg 	= null;
		
						if(execute != undefined && execute != null){
							execute();
						}
						
						if(params.resetValue){
							template.html.restAllValues(params.contentId);
						}
						
						if(result.success){
							
							if(params.customMsg == undefined && params.customMsg == null){
								typeMsg 	= 'success';
								titleMsg 	= 'Exitoso';
								textMsg 	= 'Creado con exito';
							}
							
						}else{
							
							if(params.customMsg == undefined && params.customMsg == null){
								typeMsg 	= 'warning';
								titleMsg 	= 'Error';
								textMsg 	= result.error;
							}
							
						}
						
						if(params.customMsg != undefined && params.customMsg != null){
							typeMsg 	= params.type;
							titleMsg 	= params.title;
							textMsg 	= params.text;
						}	
	
	
						if(params.activeMsg !== undefined && params.activeMsg){
							Swal.fire(titleMsg,
								  textMsg,
								  typeMsg);
						}
	                    
	
	                },
	                failure: function (error, msg) {
	                    Swal.fire(
									  'Error',
									  'Error de peticion',
									  'error'
									);
	                }
	            });
			},
			
			get : function(params, view, contentId){
				
				var paramsPost 	= params.requestBody;
	
	            jQuery.ajax({
	                url: params.url,
	                data: JSON.stringify(paramsPost),
	                cache: false,
	                contentType: "application/json",
	                processData: false,
	                method: 'POST',
	                type: 'POST',
	                success: function (result) {
		
						if(view.global != undefined && view.global.result != undefined ){
							view.global.result = result;
						}
						
						if(contentId != undefined && contentId != null){
							template.html.setAllValues(contentId, result)
						}
						
						if(view.after != undefined && view.after.ajaxLoad != undefined){
							
							result.paramsGet = params;
							
							view.after.ajaxLoad(result);
						}
						
						if(params.customLoad != undefined ){
							result.paramsGet = params;
							eval("view."+params.customLoad +"(result);");
						}
						
						if(params.result != undefined ){
							eval("view."+params.customResult +" = result;");
						}
	
	                },
	                failure: function (error, msg) {
	                    Swal.fire(
									  'Error',
									  'Error procesando informacion',
									  'error'
									);
	                }
	            });
			},
			
			getGrid : function (params) {

	            var paramsPost 	= null;
	            var columnsPost = null;
	
	            if (params.page !== undefined && params.page !== null) {
	                template.global.page = params.page;
	            }
	
				if(params.filter == undefined || Object.keys(params.filter).length == 0){
					paramsPost = {};
				}else{
					paramsPost = params.filter;
				}
				
				paramsPost.pager = {};
	
	            paramsPost.pager.page = template.global.page;
	            paramsPost.pager.size = params.size == undefined ? 8 :  params.size;
	
	            jQuery.ajax({
	                url: params.url,
	                data: JSON.stringify(paramsPost),
	                cache: false,
	                contentType: "application/json",
	                processData: false,
	                method: 'POST',
	                type: 'POST',
	                success: function (result) {
	
	                    var arrayData;
	                    var html = '<div class="table-responsive-lg">';
	
	                    arrayData = result.data.content;
	
	                    html += '<table class="table">';
	                    html += '<thead">';
	                    html += '<tr>';
	
	                    if (params.columns !== null && params.columns.length > 0) {
	
	                        $.each(params.columns, function (indexColumn, column) {
	                            html += '<th>';
	                            html += column.title;
	                            html += '</th>';

								if((indexColumn + 1) === params.columns.length ){
											html += '<th>';
											html += 	'Editar';
											html += '</th>';
								}
								
	                        });
	                    } else {
	                        
	                        columnsPost = template.json.getKeys(arrayData);
	                        
	                        $.each(columnsPost, function (index, column) {
	                            html += '<th>';
	                            html += column;
	                            html += '</th>';

								if((indexColumn + 1) === params.columns.length ){
											html += '<th>';
											html += 	'Editar';
											html += '</th>';
								}
	                        });
	                    }
	
	                    html += '</tr>';
	                    html += '</thead">';
	
	                    html += '<tbody">';
	
	                    if (arrayData.length > 0) {
		
	                        $.each(arrayData, function (indexData, itemArray) {
	                            html += '<tr>';
	
	                            if (params.columns !== null && params.columns.length > 0) {
	
	                                $.each(params.columns, function (indexColumn, column) {
	
										var value = eval("itemArray." + column.column);
										
										if(column.type == "date"){
											value = value.toString().split(".")[0].replace("T", " ");
										}else{
											value = value.toString();
										}
										
										
	                                    html += '<td>';
	                                    html += value;
	                                    html += '</td>';

										if((indexColumn + 1) === params.columns.length ){
											
											var editKey = eval("itemArray." + params.editKey);
											
											html += "<td>";
											html += 	'<button class="btn btn-primary '+params.classEdit+'" lang="'+editKey+'">Editar</button>';
											html += "</td>";
										}
	
	                                });

	                            } else {
	
	                                $.each(columnsPost, function (indexColumn, column) {
	
	                                    html += '<td>';
	                                    html += eval("itemArray." + column);
	                                    html += '</td>';

										if((indexColumn + 1) === columnsPost.length ){
											
											var editKey = eval("itemArray." + params.editKey);
											
											html += "<td>";
											html += 	'<button class="btn btn-primary '+params.classEdit+'" lang="'+editKey+'">Editar</button>';
											html += "</td>";
										}
	
	                                });
	                            }

								
								html += '</tr>';
	                        });
	                    }else{
							html += '<tr><td colspan="'+params.columns.length+'">No se encontraron registros.</td></tr>'; 		
						}
						
						html += '</tbody">';
	
	                    html += '</table>';
	                    html += '</div>';
	
						$("#" + params.div).html(html);
						
	                    template.html.paginator(params);
						
						if(params.notInit == undefined || notInit)
						params.afterrender.listeners.action();
	
	                },
	                failure: function (formTab, actionExt) {
	                    var data = null;
	                }
	            });
	
	        },

			getItemMedium : function (params) {

	            var paramsPost 	= null;
	
	            if (params.page !== undefined && params.page !== null) {
	                template.global.page = params.page;
	            }
	
				if(params.filter == undefined || Object.keys(params.filter).length == 0){
					paramsPost = {};
				}else{
					paramsPost = params.filter;
				}
				
				paramsPost.pager = {};
	
	            paramsPost.pager.page = template.global.page;
	            paramsPost.pager.size = params.size == undefined ? 8 :  params.size;
	
	            jQuery.ajax({
	                url: params.url,
	                data: JSON.stringify(paramsPost),
	                cache: false,
	                contentType: "application/json",
	                processData: false,
	                method: 'POST',
	                type: 'POST',
	                success: function (result) {
	
	                    var arrayData;
	                    var html = '<div class="table-responsive-lg">';
	
	                    arrayData = result.data.content;
	
	                    if (arrayData.length > 0) {
		
	                        html +='<div class="col mb-5">';
                            html +='    <div class="card h-100 sin-borde">';
                            html +='        <img src="images/principal/oferta1.jpg" class="card-img-top" alt="...">';
                            html +='        <div class="card-body">';
                            html +='            <h5 class="card-title title-corporativo"> Sensual Love  </h5>';
                            html +='            <p class="card-text">Delicious Blueberry</p>';
                            html +='            <h6 class="card-title antes">antes :$41.900</h6>';
                            html +='            <h5 class="card-title">Ahora: $31.900</h5>';
                            html +='            <a href="#" class="btn btn-danger btn-sm">Añadir</a>';
                            html +='        </div>';
                            html +='    </div>';
                            html +='</div>';
	                    }

	                    html += '</div>';
	
						$("#" + params.div).html(html);
						
	                    //template.html.paginator(params);

						params.afterrender.listeners.action();
	
	                },
	                failure: function (formTab, actionExt) {
	                    var data = null;
	                }
	            });
	
	        }
			
		},

        get: function () {

        }
    }

};

