
{}

var principal = {
    
	global : {
		editResult : null
	},
    
    init: function () {
        
        
        principal.items.init();
        
    },

	items : {
		
		init : function(){
			
			
			principal.items.listarItems();
			//principal.items.listeners.init();
		},
		
		openModalCreate : function(data){
			
			$("#modal-create-item-content #code").prop('disabled',true);
			
			$("#createModal").modal('show');
			
		},
		
		getModalItem : function(jsonRequest){
			
			var params = {
							url : "http://localhost:8080/items/getAllItems",
							requestBody : jsonRequest
				
						};
			
			
			template.ajax.post.get(params, principal, "modal-create-item-content");
			
			principal.items.openModalCreate();
			
		},
		
		createItem : function(jsonItem, contentId){
			
			var jsonRequest = {};
			var result		= null;
			
			jsonRequest.items = [];
			jsonRequest.items.push(jsonItem);
			
			var params = {
							url : "http://localhost:8080/items/itemsPublishing",
							requestBody : jsonRequest,
							contentId : contentId,
							resetValue : true
						};
			
			
			result = template.ajax.post.send(params, principal.items.listarItems);
			
			$("#createModal").modal('hide');
			
		},
		
		listarItems: function(filterParams) {
			
			if(filterParams == undefined || filterParams == null){
				filterParams = {};
			}else{
				filterParams = filterParams;
			}
        
			var params = {
							url : "http://localhost:8080/items/getAllItems",
							filter : filterParams,
							div : "adminBody", 
				            paginator : {
											size : 10
										},
							editKey : "code",
							classEdit : "edit-item",
							afterrender: {
								listeners: {
									action : principal.items.listeners.init
								}
							},
				            columns : [
				                        {
				                            title : "id",
				                            column : "idItem"
				                        },
				                        {
				                            title : "Codigo",
				                            column : "code"
				                        },
				                        {
				                            title : "Descripción",
				                            column : "description"
				                        },
				                        {
				                            title : "Precio",
				                            column : "price"
				                        },
				                        {
				                            title : "Estado",
				                            column : "status"
				                        }
				
				                    ],
				             editar : ""
				            
				        };
	
			template.html.autoTable(params);
	        
	    },

		listeners : {
			
			init : function(){
				principal.items.listeners.buttons.init();
			},
			
			buttons : {
				
				init : function(){
					
					principal.items.listeners.buttons.btnCreateItem();
					principal.items.listeners.buttons.btnCreate();
					principal.items.listeners.buttons.btnFilter();
					principal.items.listeners.buttons.btnFilterItem();
					principal.items.listeners.buttons.btnEditItem();
				},
				
				btnFilter : function(){
					
					$("#btnFilter").click(function(){
						
						$("#filtroModal").modal('show');
						
					});
				},
				
				btnCreate : function(){
					
					$("#btnCreate").click(function(){
						$("#modal-create-item-content #code").prop('disabled',false);
						$("#createModal").modal('show');
						
					});
				},
				
				btnFilterItem : function(){
					
					$("#btnFilterItem").click(function(){
						
						var parametros = template.html.getAllValues("modal-filter-item-content");
						
						principal.items.listarItems(parametros);
						
						$("#filtroModal").modal('hide');
					});
					
				},
				
				btnCreateItem : function(){
					
					$("#btnCreateItem").click(function(){
						
						var contentId = "modal-create-item-content";
						var parametros = template.html.getAllValues(contentId);
						
						principal.items.createItem(parametros, contentId);
						
						
					});
					
				},
				
				btnEditItem : function(){
					
					$(".edit-item").click(function(){
						
						var parametros = {};
						
						parametros.code = this.lang; 
						
						principal.items.getModalItem(parametros);
						
						
					});
					
				}
				
			}
		}
		
		

	}

    

};
