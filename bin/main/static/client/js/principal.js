
define('principalClient', function () {

var clientPrincipal = {

	global: {
		idShop: null,
		itemsCarrito: null,
		municipios: [{ "codigo": "91263", "nombre": "EL ENCANTO" }, { "codigo": "91405", "nombre": "LA CHORRERA" }, { "codigo": "91407", "nombre": "LA PEDRERA" }, { "codigo": "91430", "nombre": "LA VICTORIA" }, { "codigo": "91001", "nombre": "LETICIA" }, { "codigo": "91460", "nombre": "MIRITI - PARAN�?" }, { "codigo": "91530", "nombre": "PUERTO ALEGRIA" }, { "codigo": "91536", "nombre": "PUERTO ARICA" }, { "codigo": "91540", "nombre": "PUERTO NARIÑO" }, { "codigo": "91669", "nombre": "PUERTO SANTANDER" }, { "codigo": "91798", "nombre": "TARAPAC�?" }, { "codigo": "05120", "nombre": "C�?CERES" }, { "codigo": "05154", "nombre": "CAUCASIA" }, { "codigo": "05250", "nombre": "EL BAGRE" }, { "codigo": "05495", "nombre": "NECH�?" }, { "codigo": "05790", "nombre": "TARAZ�?" }, { "codigo": "05895", "nombre": "ZARAGOZA" }, { "codigo": "05142", "nombre": "CARACOL�?" }, { "codigo": "05425", "nombre": "MACEO" }, { "codigo": "05579", "nombre": "PUERTO BERRiO" }, { "codigo": "05585", "nombre": "PUERTO NARE" }, { "codigo": "05591", "nombre": "PUERTO TRIUNFO" }, { "codigo": "05893", "nombre": "YONDÓ" }, { "codigo": "05031", "nombre": "AMALFI" }, { "codigo": "05040", "nombre": "ANOR�?" }, { "codigo": "05190", "nombre": "CISNEROS" }, { "codigo": "05604", "nombre": "REMEDIOS" }, { "codigo": "05670", "nombre": "SAN ROQUE" }, { "codigo": "05690", "nombre": "SANTO DOMINGO" }, { "codigo": "05736", "nombre": "SEGOVIA" }, { "codigo": "05858", "nombre": "VEGACH�?" }, { "codigo": "05885", "nombre": "YAL�?" }, { "codigo": "05890", "nombre": "YOLOMBÓ" }, { "codigo": "05038", "nombre": "ANGOSTURA" }, { "codigo": "05086", "nombre": "BELMIRA" }, { "codigo": "05107", "nombre": "BRICEÑO" }, { "codigo": "05134", "nombre": "CAMPAMENTO" }, { "codigo": "05150", "nombre": "CAROLINA" }, { "codigo": "05237", "nombre": "DON MATiAS" }, { "codigo": "05264", "nombre": "ENTRERRIOS" }, { "codigo": "05310", "nombre": "GÓMEZ PLATA" }, { "codigo": "05315", "nombre": "GUADALUPE" }, { "codigo": "05361", "nombre": "ITUANGO" }, { "codigo": "05647", "nombre": "SAN ANDRÉS" }, { "codigo": "05658", "nombre": "SAN JOSÉ DE LA MONTAÑA" }, { "codigo": "05664", "nombre": "SAN PEDRO" }, { "codigo": "05686", "nombre": "SANTA ROSA de osos" }, { "codigo": "05819", "nombre": "TOLEDO" }, { "codigo": "05854", "nombre": "VALDIVIA" }, { "codigo": "05887", "nombre": "YARUMAL" }, { "codigo": "05004", "nombre": "ABRIAQU�?" }, { "codigo": "05044", "nombre": "ANZA" }, { "codigo": "05059", "nombre": "ARMENIA" }, { "codigo": "05113", "nombre": "BURITIC�?" }, { "codigo": "05138", "nombre": "CAÑASGORDAS" }, { "codigo": "05234", "nombre": "DABEIBA" }, { "codigo": "05240", "nombre": "EBÉJICO" }, { "codigo": "05284", "nombre": "FRONTINO" }, { "codigo": "05306", "nombre": "GIRALDO" }, { "codigo": "05347", "nombre": "HELICONIA" }, { "codigo": "05411", "nombre": "LIBORINA" }, { "codigo": "05501", "nombre": "OLAYA" }, { "codigo": "05543", "nombre": "PEQUE" }, { "codigo": "05628", "nombre": "SABANALARGA" }, { "codigo": "05656", "nombre": "SAN JERÓNIMO" }, { "codigo": "05042", "nombre": "SANTAFÉ DE ANTIOQUIA" }, { "codigo": "05761", "nombre": "SOPETRaN" }, { "codigo": "05842", "nombre": "URAMITA" }, { "codigo": "05002", "nombre": "ABEJORRAL" }, { "codigo": "05021", "nombre": "ALEJANDR�?A" }, { "codigo": "05055", "nombre": "ARGELIA" }, { "codigo": "05148", "nombre": "CARMEN DE VIBORAL" }, { "codigo": "05197", "nombre": "COCORN�?" }, { "codigo": "05206", "nombre": "CONCEPCIÓN" }, { "codigo": "05313", "nombre": "GRANADA" }, { "codigo": "05318", "nombre": "GUARNE" }, { "codigo": "05321", "nombre": "GUATAPE" }, { "codigo": "05376", "nombre": "LA CEJA" }, { "codigo": "05400", "nombre": "LA UNIÓN" }, { "codigo": "05440", "nombre": "MARINILLA" }, { "codigo": "05483", "nombre": "NARIÑO" }, { "codigo": "05541", "nombre": "PEÑOL" }, { "codigo": "05607", "nombre": "RETIRO" }, { "codigo": "05615", "nombre": "RIONEGRO" }, { "codigo": "05649", "nombre": "SAN CARLOS" }, { "codigo": "05652", "nombre": "SAN FRANCISCO" }, { "codigo": "05660", "nombre": "SAN LUIS" }, { "codigo": "05667", "nombre": "SAN RAFAEL" }, { "codigo": "05674", "nombre": "SAN VICENTE" }, { "codigo": "05697", "nombre": "SANTUARIO" }, { "codigo": "05756", "nombre": "SONSON" }, { "codigo": "05030", "nombre": "AMAGa" }, { "codigo": "05034", "nombre": "ANDES" }, { "codigo": "05036", "nombre": "ANGELOPOLIS" }, { "codigo": "05091", "nombre": "BETANIA" }, { "codigo": "05093", "nombre": "BETULIA" }, { "codigo": "05125", "nombre": "CAICEDO" }, { "codigo": "05145", "nombre": "CARAMANTA" }, { "codigo": "05101", "nombre": "CIUDAD BOL�?VAR" }, { "codigo": "05209", "nombre": "CONCORDIA" }, { "codigo": "05282", "nombre": "FREDONIA" }, { "codigo": "05353", "nombre": "HISPANIA" }, { "codigo": "05364", "nombre": "JARD�?N" }, { "codigo": "05368", "nombre": "JERICÓ" }, { "codigo": "05390", "nombre": "LA PINTADA" }, { "codigo": "05467", "nombre": "MONTEBELLO" }, { "codigo": "05576", "nombre": "PUEBLORRICO" }, { "codigo": "05642", "nombre": "SALGAR" }, { "codigo": "05679", "nombre": "SANTA BaRBARA" }, { "codigo": "05789", "nombre": "T�?MESIS" }, { "codigo": "05792", "nombre": "TARSO" }, { "codigo": "05809", "nombre": "TITIRIB�?" }, { "codigo": "05847", "nombre": "URRAO" }, { "codigo": "05856", "nombre": "VALPARAISO" }, { "codigo": "05861", "nombre": "VENECIA" }, { "codigo": "05045", "nombre": "APARTADÓ" }, { "codigo": "05051", "nombre": "ARBOLETES" }, { "codigo": "05147", "nombre": "CAREPA" }, { "codigo": "05172", "nombre": "CHIGORODÓ" }, { "codigo": "05475", "nombre": "MURINDÓ" }, { "codigo": "05480", "nombre": "MUTATA" }, { "codigo": "05490", "nombre": "NECOCL�?" }, { "codigo": "05659", "nombre": "SAN JUAN DE URABA" }, { "codigo": "05665", "nombre": "SAN PEDRO DE URABA" }, { "codigo": "05837", "nombre": "TURBO" }, { "codigo": "05873", "nombre": "VIG�?A DEL FUERTE" }, { "codigo": "05079", "nombre": "BARBOSA" }, { "codigo": "05088", "nombre": "BELLO" }, { "codigo": "05129", "nombre": "CALDAS" }, { "codigo": "05212", "nombre": "COPACABANA" }, { "codigo": "05266", "nombre": "ENVIGADO" }, { "codigo": "05308", "nombre": "GIRARDOTA" }, { "codigo": "05360", "nombre": "ITAGUI" }, { "codigo": "05380", "nombre": "LA ESTRELLA" }, { "codigo": "05001", "nombre": "MEDELL�?N" }, { "codigo": "05631", "nombre": "SABANETA" }, { "codigo": "81001", "nombre": "ARAUCA" }, { "codigo": "81065", "nombre": "ARAUQUITA" }, { "codigo": "81220", "nombre": "CRAVO NORTE" }, { "codigo": "81300", "nombre": "FORTUL" }, { "codigo": "81591", "nombre": "PUERTO RONDÓN" }, { "codigo": "81736", "nombre": "SARAVENA" }, { "codigo": "81794", "nombre": "TAME" }, { "codigo": "88564", "nombre": "PROVIDENCIA Y SANTA CATALINA" }, { "codigo": "88001", "nombre": "SAN ANDReS" }, { "codigo": "08001", "nombre": "BARRANQUILLA" }, { "codigo": "08296", "nombre": "GALAPA" }, { "codigo": "08433", "nombre": "MALAMBO" }, { "codigo": "08573", "nombre": "PUERTO COLOMBIA" }, { "codigo": "08758", "nombre": "SOLEDAD" }, { "codigo": "08137", "nombre": "CAMPO DE LA CRUZ" }, { "codigo": "08141", "nombre": "CANDELARIA" }, { "codigo": "08421", "nombre": "LURUACO" }, { "codigo": "08436", "nombre": "MANATi" }, { "codigo": "08606", "nombre": "REPELON" }, { "codigo": "08675", "nombre": "SANTA LUCiA" }, { "codigo": "08770", "nombre": "SUAN" }, { "codigo": "08078", "nombre": "BARANOA" }, { "codigo": "08520", "nombre": "PALMAR DE VARELA" }, { "codigo": "08558", "nombre": "POLONUEVO" }, { "codigo": "08560", "nombre": "PONEDERA" }, { "codigo": "08634", "nombre": "Sabanagrande" }, { "codigo": "08638", "nombre": "SABANALARGA" }, { "codigo": "08685", "nombre": "Santo Tomas" }, { "codigo": "08372", "nombre": "JUAN DE ACOSTA" }, { "codigo": "08549", "nombre": "PIOJÓ" }, { "codigo": "08832", "nombre": "TUBARA" }, { "codigo": "08849", "nombre": "USIACURi" }, { "codigo": "11001", "nombre": "BOGOTA D.C." }, { "codigo": "13188", "nombre": "CICUCO" }, { "codigo": "13300", "nombre": "HATILLO DE LOBA" }, { "codigo": "13440", "nombre": "MARGARITA" }, { "codigo": "13468", "nombre": "MOMPÓS" }, { "codigo": "13650", "nombre": "SAN FERNANDO" }, { "codigo": "13780", "nombre": "TALAIGUA NUEVO" }, { "codigo": "13052", "nombre": "ARJONA" }, { "codigo": "13062", "nombre": "ARROYOHONDO" }, { "codigo": "13140", "nombre": "CALAMAR" }, { "codigo": "13001", "nombre": "CARTAGENA" }, { "codigo": "13222", "nombre": "CLEMENCIA" }, { "codigo": "13433", "nombre": "MAHATES" }, { "codigo": "13620", "nombre": "SAN CRISTOBAL" }, { "codigo": "13647", "nombre": "SAN ESTANISLAO" }, { "codigo": "13673", "nombre": "SANTA CATALINA" }, { "codigo": "13683", "nombre": "SANTA ROSA DE LIMA" }, { "codigo": "13760", "nombre": "SOPLAVIENTO" }, { "codigo": "13836", "nombre": "TURBACO" }, { "codigo": "13838", "nombre": "TURBANA" }, { "codigo": "13873", "nombre": "VILLANUEVA" }, { "codigo": "13030", "nombre": "ALTOS DEL ROSARIO" }, { "codigo": "13074", "nombre": "BARRANCO DE LOBA" }, { "codigo": "13268", "nombre": "EL PEÑON" }, { "codigo": "13580", "nombre": "REGIDOR" }, { "codigo": "13600", "nombre": "R�?O VIEJO" }, { "codigo": "13667", "nombre": "SAN MARTIN DE LOBA" }, { "codigo": "13042", "nombre": "ARENAL" }, { "codigo": "13160", "nombre": "CANTAGALLO" }, { "codigo": "13473", "nombre": "MORALES" }, { "codigo": "13670", "nombre": "SAN PABLO" }, { "codigo": "13688", "nombre": "SANTA ROSA DEL SUR" }, { "codigo": "13744", "nombre": "SIMIT�?" }, { "codigo": "13006", "nombre": "ACH�?" }, { "codigo": "13430", "nombre": "MAGANGUÉ" }, { "codigo": "13458", "nombre": "MONTECRISTO" }, { "codigo": "13549", "nombre": "PINILLOS" }, { "codigo": "13655", "nombre": "SAN JACINTO DEL CAUCA" }, { "codigo": "13810", "nombre": "TIQUISIO" }, { "codigo": "13244", "nombre": "CARMEN DE BOL�?VAR" }, { "codigo": "13212", "nombre": "CÓRDOBA" }, { "codigo": "13248", "nombre": "EL GUAMO" }, { "codigo": "13442", "nombre": "MAR�?A LA BAJA" }, { "codigo": "13654", "nombre": "SAN JACINTO" }, { "codigo": "13657", "nombre": "SAN JUAN NEPOMUCENO" }, { "codigo": "13894", "nombre": "ZAMBRANO" }, { "codigo": "15232", "nombre": "CH�?QUIZA" }, { "codigo": "15187", "nombre": "CHIVAT�?" }, { "codigo": "15204", "nombre": "CÓMBITA" }, { "codigo": "15224", "nombre": "CUCAITA" }, { "codigo": "15476", "nombre": "MOTAVITA" }, { "codigo": "15500", "nombre": "OICAT�?" }, { "codigo": "15646", "nombre": "SAMAC�?" }, { "codigo": "15740", "nombre": "SIACHOQUE" }, { "codigo": "15762", "nombre": "SORA" }, { "codigo": "15764", "nombre": "SORAC�?" }, { "codigo": "15763", "nombre": "SOTAQUIR�?" }, { "codigo": "15814", "nombre": "TOCA" }, { "codigo": "15001", "nombre": "TUNJA" }, { "codigo": "15837", "nombre": "TUTA" }, { "codigo": "15861", "nombre": "VENTAQUEMADA" }, { "codigo": "15180", "nombre": "CHISCAS" }, { "codigo": "15223", "nombre": "CUBAR�?" }, { "codigo": "15244", "nombre": "EL COCUY" }, { "codigo": "15248", "nombre": "EL ESPINO" }, { "codigo": "15317", "nombre": "GUACAMAYAS" }, { "codigo": "15332", "nombre": "GÜIC�?N" }, { "codigo": "15522", "nombre": "PANQUEBA" }, { "codigo": "15377", "nombre": "LABRANZAGRANDE" }, { "codigo": "15518", "nombre": "PAJARITO" }, { "codigo": "15533", "nombre": "PAYA" }, { "codigo": "15550", "nombre": "PISBA" }, { "codigo": "15090", "nombre": "BERBEO" }, { "codigo": "15135", "nombre": "CAMPOHERMOSO" }, { "codigo": "15455", "nombre": "MIRAFLORES" }, { "codigo": "15514", "nombre": "P�?EZ" }, { "codigo": "15660", "nombre": "SAN EDUARDO" }, { "codigo": "15897", "nombre": "ZETAQUIRA" }, { "codigo": "15104", "nombre": "BOYAC�?" }, { "codigo": "15189", "nombre": "CIÉNEGA" }, { "codigo": "15367", "nombre": "JENESANO" }, { "codigo": "15494", "nombre": "NUEVO COLÓN" }, { "codigo": "15599", "nombre": "RAMIRIQU�?" }, { "codigo": "15621", "nombre": "RONDÓN" }, { "codigo": "15804", "nombre": "TIBAN�?" }, { "codigo": "15835", "nombre": "TURMEQUÉ" }, { "codigo": "15842", "nombre": "UMBITA" }, { "codigo": "15879", "nombre": "VIRACACH�?" }, { "codigo": "15172", "nombre": "CHINAVITA" }, { "codigo": "15299", "nombre": "GARAGOA" }, { "codigo": "15425", "nombre": "MACANAL" }, { "codigo": "15511", "nombre": "PACHAVITA" }, { "codigo": "15667", "nombre": "SAN LUIS DE GACENO" }, { "codigo": "15690", "nombre": "SANTA MAR�?A" }, { "codigo": "15097", "nombre": "BOAVITA" }, { "codigo": "15218", "nombre": "COVARACH�?A" }, { "codigo": "15403", "nombre": "LA UVITA" }, { "codigo": "15673", "nombre": "SAN MATEO" }, { "codigo": "15720", "nombre": "SATIVANORTE" }, { "codigo": "15723", "nombre": "SATIVASUR" }, { "codigo": "15753", "nombre": "SOAT�?" }, { "codigo": "15774", "nombre": "SUSACÓN" }, { "codigo": "15810", "nombre": "TIPACOQUE" }, { "codigo": "15106", "nombre": "BRICEÑO" }, { "codigo": "15109", "nombre": "BUENAVISTA" }, { "codigo": "15131", "nombre": "CALDAS" }, { "codigo": "15176", "nombre": "CHIQUINQUIR�?" }, { "codigo": "15212", "nombre": "COPER" }, { "codigo": "15401", "nombre": "LA VICTORIA" }, { "codigo": "15442", "nombre": "MARIP�?" }, { "codigo": "15480", "nombre": "MUZO" }, { "codigo": "15507", "nombre": "OTANCHE" }, { "codigo": "15531", "nombre": "PAUNA" }, { "codigo": "15572", "nombre": "PUERTO BOYACa" }, { "codigo": "15580", "nombre": "QU�?PAMA" }, { "codigo": "15632", "nombre": "SABOY�?" }, { "codigo": "15676", "nombre": "SAN MIGUEL DE SEMA" }, { "codigo": "15681", "nombre": "SAN PABLO BORBUR" }, { "codigo": "15832", "nombre": "TUNUNGU�?" }, { "codigo": "15022", "nombre": "ALMEIDA" }, { "codigo": "15236", "nombre": "CHIVOR" }, { "codigo": "15322", "nombre": "GUATEQUE" }, { "codigo": "15325", "nombre": "GUAYAT�?" }, { "codigo": "15380", "nombre": "LA CAPILLA" }, { "codigo": "15761", "nombre": "SOMONDOCO" }, { "codigo": "15778", "nombre": "SUTATENZA" }, { "codigo": "15798", "nombre": "TENZA" }, { "codigo": "15051", "nombre": "ARCABUCO" }, { "codigo": "15185", "nombre": "CHITARAQUE" }, { "codigo": "15293", "nombre": "GACHANTIV�?" }, { "codigo": "15469", "nombre": "MONIQUIR�?" }, { "codigo": "15600", "nombre": "R�?QUIRA" }, { "codigo": "15638", "nombre": "S�?CHICA" }, { "codigo": "15664", "nombre": "SAN JOSÉ DE PARE" }, { "codigo": "15696", "nombre": "SANTA SOF�?A" }, { "codigo": "15686", "nombre": "SANTANA" }, { "codigo": "15776", "nombre": "SUTAMARCH�?N" }, { "codigo": "15808", "nombre": "TINJAC�?" }, { "codigo": "15816", "nombre": "TOGÜ�?" }, { "codigo": "15407", "nombre": "VILLA DE LEYVA" }, { "codigo": "15047", "nombre": "AQUITANIA" }, { "codigo": "15226", "nombre": "CU�?TIVA" }, { "codigo": "15272", "nombre": "FIRAVITOBA" }, { "codigo": "15296", "nombre": "GAMEZA" }, { "codigo": "15362", "nombre": "IZA" }, { "codigo": "15464", "nombre": "MONGUA" }, { "codigo": "15466", "nombre": "MONGU�?" }, { "codigo": "15491", "nombre": "NOBSA" }, { "codigo": "15542", "nombre": "PESCA" }, { "codigo": "15759", "nombre": "SOGAMOSO" }, { "codigo": "15806", "nombre": "TIBASOSA" }, { "codigo": "15820", "nombre": "TÓPAGA" }, { "codigo": "15822", "nombre": "TOTA" }, { "codigo": "15087", "nombre": "BELÉN" }, { "codigo": "15114", "nombre": "BUSBANZ�?" }, { "codigo": "15162", "nombre": "CERINZA" }, { "codigo": "15215", "nombre": "CORRALES" }, { "codigo": "15238", "nombre": "DUITAMA" }, { "codigo": "15276", "nombre": "FLORESTA" }, { "codigo": "15516", "nombre": "PAIPA" }, { "codigo": "15693", "nombre": "SAN ROSA VITERBO" }, { "codigo": "15839", "nombre": "TUTAZ�?" }, { "codigo": "15092", "nombre": "BETÉITIVA" }, { "codigo": "15183", "nombre": "CHITA" }, { "codigo": "15368", "nombre": "JERICÓ" }, { "codigo": "15537", "nombre": "PAZ DE R�?O" }, { "codigo": "15757", "nombre": "SOCHA" }, { "codigo": "15755", "nombre": "SOCOT�?" }, { "codigo": "15790", "nombre": "TASCO" }, { "codigo": "17272", "nombre": "FILADELFIA" }, { "codigo": "17388", "nombre": "LA MERCED" }, { "codigo": "17442", "nombre": "MARMATO" }, { "codigo": "17614", "nombre": "RIOSUCIO" }, { "codigo": "17777", "nombre": "SUP�?A" }, { "codigo": "17433", "nombre": "MANZANARES" }, { "codigo": "17444", "nombre": "MARQUETALIA" }, { "codigo": "17446", "nombre": "MARULANDA" }, { "codigo": "17541", "nombre": "PENSILVANIA" }, { "codigo": "17042", "nombre": "ANSERMA" }, { "codigo": "17088", "nombre": "BELALC�?ZAR" }, { "codigo": "17616", "nombre": "RISARALDA" }, { "codigo": "17665", "nombre": "SAN JOSÉ" }, { "codigo": "17877", "nombre": "VITERBO" }, { "codigo": "17174", "nombre": "CHINCHINa" }, { "codigo": "17001", "nombre": "MANIZALES" }, { "codigo": "17486", "nombre": "NEIRA" }, { "codigo": "17524", "nombre": "PALESTINA" }, { "codigo": "17873", "nombre": "VILLAMARiA" }, { "codigo": "17013", "nombre": "AGUADAS" }, { "codigo": "17050", "nombre": "ARANZAZU" }, { "codigo": "17513", "nombre": "P�?CORA" }, { "codigo": "17653", "nombre": "SALAMINA" }, { "codigo": "17380", "nombre": "LA DORADA" }, { "codigo": "17495", "nombre": "NORCASIA" }, { "codigo": "17662", "nombre": "SAMAN�?" }, { "codigo": "17867", "nombre": "VICTORIA" }, { "codigo": "18029", "nombre": "ALBANIA" }, { "codigo": "18094", "nombre": "BELÉN DE LOS ANDAQUIES" }, { "codigo": "18150", "nombre": "CARTAGENA DEL CHAIR�?" }, { "codigo": "18205", "nombre": "CURRILLO" }, { "codigo": "18247", "nombre": "EL DONCELLO" }, { "codigo": "18256", "nombre": "EL PAUJIL" }, { "codigo": "18001", "nombre": "FLORENCIA" }, { "codigo": "18410", "nombre": "LA MONTAÑITA" }, { "codigo": "18460", "nombre": "MILaN" }, { "codigo": "18479", "nombre": "MORELIA" }, { "codigo": "18592", "nombre": "PUERTO RICO" }, { "codigo": "18610", "nombre": "SAN JOSE DEL FRAGUA" }, { "codigo": "18753", "nombre": "SAN VICENTE DEL CAGU�?N" }, { "codigo": "18756", "nombre": "SOLANO" }, { "codigo": "18785", "nombre": "SOLITA" }, { "codigo": "18860", "nombre": "VALPARAISO" }, { "codigo": "85010", "nombre": "AGUAZUL" }, { "codigo": "85015", "nombre": "CHAMEZA" }, { "codigo": "85125", "nombre": "HATO COROZAL" }, { "codigo": "85136", "nombre": "LA SALINA" }, { "codigo": "85139", "nombre": "MAN�?" }, { "codigo": "85162", "nombre": "MONTERREY" }, { "codigo": "85225", "nombre": "NUNCH�?A" }, { "codigo": "85230", "nombre": "OROCUÉ" }, { "codigo": "85250", "nombre": "PAZ DE ARIPORO" }, { "codigo": "85263", "nombre": "PORE" }, { "codigo": "85279", "nombre": "RECETOR" }, { "codigo": "85300", "nombre": "SABANALARGA" }, { "codigo": "85315", "nombre": "S�?CAMA" }, { "codigo": "85325", "nombre": "SAN LUIS DE PALENQUE" }, { "codigo": "85400", "nombre": "T�?MARA" }, { "codigo": "85410", "nombre": "TAURAMENA" }, { "codigo": "85430", "nombre": "TRINIDAD" }, { "codigo": "85440", "nombre": "VILLANUEVA" }, { "codigo": "85001", "nombre": "YOPAL" }, { "codigo": "19130", "nombre": "CAJIB�?O" }, { "codigo": "19256", "nombre": "EL TAMBO" }, { "codigo": "19392", "nombre": "LA SIERRA" }, { "codigo": "19473", "nombre": "MORALES" }, { "codigo": "19548", "nombre": "PIENDAMO" }, { "codigo": "19001", "nombre": "POPAY�?N" }, { "codigo": "19622", "nombre": "ROSAS" }, { "codigo": "19760", "nombre": "SOTARA" }, { "codigo": "19807", "nombre": "TIMBIO" }, { "codigo": "19110", "nombre": "BUENOS AIRES" }, { "codigo": "19142", "nombre": "CALOTO" }, { "codigo": "19212", "nombre": "CORINTO" }, { "codigo": "19455", "nombre": "MIRANDA" }, { "codigo": "19513", "nombre": "PADILLA" }, { "codigo": "19573", "nombre": "PUERTO TEJADA" }, { "codigo": "19698", "nombre": "SANTANDER DE QUILICHAO" }, { "codigo": "19780", "nombre": "SUAREZ" }, { "codigo": "19845", "nombre": "VILLA RICA" }, { "codigo": "19318", "nombre": "GUAPI" }, { "codigo": "19418", "nombre": "LOPEZ" }, { "codigo": "19809", "nombre": "TIMBIQUI" }, { "codigo": "19137", "nombre": "CALDONO" }, { "codigo": "19355", "nombre": "INZ�?" }, { "codigo": "19364", "nombre": "JAMBALO" }, { "codigo": "19517", "nombre": "PAEZ" }, { "codigo": "19585", "nombre": "PURACE" }, { "codigo": "19743", "nombre": "Silvia" }, { "codigo": "19821", "nombre": "TORIBIO" }, { "codigo": "19824", "nombre": "TOTORO" }, { "codigo": "19022", "nombre": "ALMAGUER" }, { "codigo": "19050", "nombre": "ARGELIA" }, { "codigo": "19075", "nombre": "BALBOA" }, { "codigo": "19100", "nombre": "BOL�?VAR" }, { "codigo": "19290", "nombre": "FLORENCIA" }, { "codigo": "19397", "nombre": "LA VEGA" }, { "codigo": "19450", "nombre": "MERCADERES" }, { "codigo": "19532", "nombre": "PATIA" }, { "codigo": "19533", "nombre": "PIAMONTE" }, { "codigo": "19693", "nombre": "SAN SEBASTIAN" }, { "codigo": "19701", "nombre": "SANTA ROSA" }, { "codigo": "19785", "nombre": "SUCRE" }, { "codigo": "20045", "nombre": "BECERRIL" }, { "codigo": "20175", "nombre": "CHIMICHAGUA" }, { "codigo": "20178", "nombre": "CHIRIGUANA" }, { "codigo": "20228", "nombre": "CURUMAN�?" }, { "codigo": "20400", "nombre": "LA JAGUA DE IBIRICO" }, { "codigo": "20517", "nombre": "PAILITAS" }, { "codigo": "20787", "nombre": "TAMALAMEQUE" }, { "codigo": "20032", "nombre": "ASTREA" }, { "codigo": "20060", "nombre": "BOSCONIA" }, { "codigo": "20238", "nombre": "EL COPEY" }, { "codigo": "20250", "nombre": "EL PASO" }, { "codigo": "20013", "nombre": "AGUST�?N CODAZZI" }, { "codigo": "20621", "nombre": "LA PAZ" }, { "codigo": "20443", "nombre": "MANAURE" }, { "codigo": "20570", "nombre": "PUEBLO BELLO" }, { "codigo": "20750", "nombre": "SAN DIEGO" }, { "codigo": "20001", "nombre": "VALLEDUPAR" }, { "codigo": "20011", "nombre": "AGUACHICA" }, { "codigo": "20295", "nombre": "GAMARRA" }, { "codigo": "20310", "nombre": "GONZ�?LEZ" }, { "codigo": "20383", "nombre": "LA GLORIA" }, { "codigo": "20550", "nombre": "PELAYA" }, { "codigo": "20614", "nombre": "R�?O DE ORO" }, { "codigo": "20710", "nombre": "SAN ALBERTO" }, { "codigo": "20770", "nombre": "SAN MART�?N" }, { "codigo": "27050", "nombre": "ATRATO" }, { "codigo": "27073", "nombre": "BAGADÓ" }, { "codigo": "27099", "nombre": "BOJAYA" }, { "codigo": "27245", "nombre": "EL CARMEN DE ATRATO" }, { "codigo": "27413", "nombre": "LLORÓ" }, { "codigo": "27425", "nombre": "MEDIO ATRATO" }, { "codigo": "27001", "nombre": "QUIBDÓ" }, { "codigo": "27600", "nombre": "RIO QUITO" }, { "codigo": "27006", "nombre": "ACAND�?" }, { "codigo": "27086", "nombre": "BELÉN DE BAJIRA" }, { "codigo": "27150", "nombre": "CARMÉN DEL DARIÉN" }, { "codigo": "27615", "nombre": "RIOSUCIO" }, { "codigo": "27800", "nombre": "UNGU�?A" }, { "codigo": "27075", "nombre": "BAH�?A SOLANO" }, { "codigo": "27372", "nombre": "JURADÓ" }, { "codigo": "27495", "nombre": "NUQU�?" }, { "codigo": "27025", "nombre": "ALTO BAUDÓ" }, { "codigo": "27077", "nombre": "BAJO BAUDÓ" }, { "codigo": "27250", "nombre": "El Litoral del San Juan" }, { "codigo": "27430", "nombre": "MEDIO BAUDÓ" }, { "codigo": "27135", "nombre": "CANTON DE SAN PABLO" }, { "codigo": "27160", "nombre": "CERTEGUI" }, { "codigo": "27205", "nombre": "CONDOTO" }, { "codigo": "27361", "nombre": "ITSMINA" }, { "codigo": "27450", "nombre": "MEDIO SAN JUAN" }, { "codigo": "27491", "nombre": "NÓVITA" }, { "codigo": "27580", "nombre": "R�?O FR�?O" }, { "codigo": "27660", "nombre": "SAN JOSÉ DEL PALMAR" }, { "codigo": "27745", "nombre": "SIP�?" }, { "codigo": "27787", "nombre": "TADÓ" }, { "codigo": "27810", "nombre": "UNION PANAMERICANA" }, { "codigo": "23807", "nombre": "TIERRALTA" }, { "codigo": "23855", "nombre": "VALENCIA" }, { "codigo": "23168", "nombre": "CHIM�?" }, { "codigo": "23300", "nombre": "COTORRA" }, { "codigo": "23417", "nombre": "LORICA" }, { "codigo": "23464", "nombre": "MOMIL" }, { "codigo": "23586", "nombre": "PUR�?SIMA" }, { "codigo": "23001", "nombre": "MONTER�?A" }, { "codigo": "23090", "nombre": "CANALETE" }, { "codigo": "23419", "nombre": "LOS CÓRDOBAS" }, { "codigo": "23500", "nombre": "MOÑITOS" }, { "codigo": "23574", "nombre": "PUERTO ESCONDIDO" }, { "codigo": "23672", "nombre": "SAN ANTERO" }, { "codigo": "23675", "nombre": "SAN BERNARDO DEL VIENTO" }, { "codigo": "23182", "nombre": "CHINÚ" }, { "codigo": "23660", "nombre": "SAHAGÚN" }, { "codigo": "23670", "nombre": "SAN ANDRÉS SOTAVENTO" }, { "codigo": "23068", "nombre": "AYAPEL" }, { "codigo": "23079", "nombre": "BUENAVISTA" }, { "codigo": "23350", "nombre": "LA APARTADA" }, { "codigo": "23466", "nombre": "MONTEL�?BANO" }, { "codigo": "23555", "nombre": "PLANETA RICA" }, { "codigo": "23570", "nombre": "PUEBLO NUEVO" }, { "codigo": "23580", "nombre": "PUERTO LIBERTADOR" }, { "codigo": "23162", "nombre": "CERETÉ" }, { "codigo": "23189", "nombre": "CIÉNAGA DE ORO" }, { "codigo": "23678", "nombre": "SAN CARLOS" }, { "codigo": "23686", "nombre": "SAN PELAYO" }, { "codigo": "25183", "nombre": "CHOCONT�?" }, { "codigo": "25426", "nombre": "MACHETA" }, { "codigo": "25436", "nombre": "MANTA" }, { "codigo": "25736", "nombre": "SESQUILÉ" }, { "codigo": "25772", "nombre": "SUESCA" }, { "codigo": "25807", "nombre": "TIBIRITA" }, { "codigo": "25873", "nombre": "VILLAPINZÓN" }, { "codigo": "25001", "nombre": "AGUA DE DIOS" }, { "codigo": "25307", "nombre": "GIRARDOT" }, { "codigo": "25324", "nombre": "GUATAQU�?" }, { "codigo": "25368", "nombre": "JERUSALÉN" }, { "codigo": "25483", "nombre": "NARIÑO" }, { "codigo": "25488", "nombre": "NILO" }, { "codigo": "25612", "nombre": "RICAURTE" }, { "codigo": "25815", "nombre": "TOCAIMA" }, { "codigo": "25148", "nombre": "CAPARRAP�?" }, { "codigo": "25320", "nombre": "GUADUAS" }, { "codigo": "25572", "nombre": "PUERTO SALGAR" }, { "codigo": "25019", "nombre": "ALB�?N" }, { "codigo": "25398", "nombre": "LA PEÑA" }, { "codigo": "25402", "nombre": "LA VEGA" }, { "codigo": "25489", "nombre": "NIMAIMA" }, { "codigo": "25491", "nombre": "NOCAIMA" }, { "codigo": "25592", "nombre": "QUEBRADANEGRA" }, { "codigo": "25658", "nombre": "SAN FRANCISCO" }, { "codigo": "25718", "nombre": "SASAIMA" }, { "codigo": "25777", "nombre": "SUPAT�?" }, { "codigo": "25851", "nombre": "ÚTICA" }, { "codigo": "25862", "nombre": "VERGARA" }, { "codigo": "25875", "nombre": "VILLETA" }, { "codigo": "25293", "nombre": "GACHALA" }, { "codigo": "25297", "nombre": "GACHETA" }, { "codigo": "25299", "nombre": "GAMA" }, { "codigo": "25322", "nombre": "GUASCA" }, { "codigo": "25326", "nombre": "GUATAVITA" }, { "codigo": "25372", "nombre": "JUN�?N" }, { "codigo": "25377", "nombre": "LA CALERA" }, { "codigo": "25839", "nombre": "UBAL�?" }, { "codigo": "25086", "nombre": "BELTR�?N" }, { "codigo": "25095", "nombre": "BITUIMA" }, { "codigo": "25168", "nombre": "CHAGUAN�?" }, { "codigo": "25328", "nombre": "GUAYABAL DE SIQUIMA" }, { "codigo": "25580", "nombre": "PULI" }, { "codigo": "25662", "nombre": "SAN JUAN DE R�?O SECO" }, { "codigo": "25867", "nombre": "VIAN�?" }, { "codigo": "25438", "nombre": "MEDINA" }, { "codigo": "25530", "nombre": "PARATEBUENO" }, { "codigo": "25151", "nombre": "CAQUEZA" }, { "codigo": "25178", "nombre": "CHIPAQUE" }, { "codigo": "25181", "nombre": "CHOACH�?" }, { "codigo": "25279", "nombre": "FOMEQUE" }, { "codigo": "25281", "nombre": "FOSCA" }, { "codigo": "25335", "nombre": "GUAYABETAL" }, { "codigo": "25339", "nombre": "GUTIÉRREZ" }, { "codigo": "25594", "nombre": "QUETAME" }, { "codigo": "25841", "nombre": "UBAQUE" }, { "codigo": "25845", "nombre": "UNE" }, { "codigo": "25258", "nombre": "EL PEÑÓN" }, { "codigo": "25394", "nombre": "LA PALMA" }, { "codigo": "25513", "nombre": "PACHO" }, { "codigo": "25518", "nombre": "PAIME" }, { "codigo": "25653", "nombre": "SAN CAYETANO" }, { "codigo": "25823", "nombre": "TOPAIPI" }, { "codigo": "25871", "nombre": "VILLAGOMEZ" }, { "codigo": "25885", "nombre": "YACOP�?" }, { "codigo": "25126", "nombre": "CAJIC�?" }, { "codigo": "25175", "nombre": "CH�?A" }, { "codigo": "25200", "nombre": "COGUA" }, { "codigo": "25295", "nombre": "GACHANCIP�?" }, { "codigo": "25486", "nombre": "NEMOCoN" }, { "codigo": "25758", "nombre": "SOPÓ" }, { "codigo": "25785", "nombre": "TABIO" }, { "codigo": "25817", "nombre": "TOCANCIP�?" }, { "codigo": "25899", "nombre": "ZIPAQUIR�?" }, { "codigo": "25099", "nombre": "BOJAC�?" }, { "codigo": "25214", "nombre": "COTA" }, { "codigo": "25260", "nombre": "EL ROSAL" }, { "codigo": "25269", "nombre": "FACATATIV�?" }, { "codigo": "25286", "nombre": "FUNZA" }, { "codigo": "25430", "nombre": "MADRID" }, { "codigo": "25473", "nombre": "MOSQUERA" }, { "codigo": "25769", "nombre": "SUBACHOQUE" }, { "codigo": "25799", "nombre": "TENJO" }, { "codigo": "25898", "nombre": "ZIPACoN" }, { "codigo": "25740", "nombre": "SIBATÉ" }, { "codigo": "25754", "nombre": "SOACHA" }, { "codigo": "25053", "nombre": "ARBEL�?EZ" }, { "codigo": "25120", "nombre": "CABRERA" }, { "codigo": "25290", "nombre": "FUSAGASUG�?" }, { "codigo": "25312", "nombre": "GRANADA" }, { "codigo": "25524", "nombre": "PANDI" }, { "codigo": "25535", "nombre": "PASCA" }, { "codigo": "25649", "nombre": "SAN BERNARDO" }, { "codigo": "25743", "nombre": "SILVANIA" }, { "codigo": "25805", "nombre": "TIBACUY" }, { "codigo": "25506", "nombre": "VENECIA" }, { "codigo": "25035", "nombre": "ANAPOIMA" }, { "codigo": "25040", "nombre": "ANOLAIMA" }, { "codigo": "25599", "nombre": "APULO" }, { "codigo": "25123", "nombre": "CACHIPAY" }, { "codigo": "25245", "nombre": "EL COLEGIO" }, { "codigo": "25386", "nombre": "LA MESA" }, { "codigo": "25596", "nombre": "QUIPILE" }, { "codigo": "25645", "nombre": "SAN ANTONIO DE TEQUENDAMA" }, { "codigo": "25797", "nombre": "TENA" }, { "codigo": "25878", "nombre": "VIOT�?" }, { "codigo": "25154", "nombre": "CARMEN DE CARUPA" }, { "codigo": "25224", "nombre": "CUCUNUB�?" }, { "codigo": "25288", "nombre": "FÚQUENE" }, { "codigo": "25317", "nombre": "GUACHET�?" }, { "codigo": "25407", "nombre": "LENGUAZAQUE" }, { "codigo": "25745", "nombre": "SIMIJACA" }, { "codigo": "25779", "nombre": "SUSA" }, { "codigo": "25781", "nombre": "SUTATAUSA" }, { "codigo": "25793", "nombre": "TAUSA" }, { "codigo": "25843", "nombre": "UBATE" }, { "codigo": "94343", "nombre": "BARRANCO MINA" }, { "codigo": "94886", "nombre": "CACAHUAL" }, { "codigo": "94001", "nombre": "IN�?RIDA" }, { "codigo": "94885", "nombre": "LA GUADALUPE" }, { "codigo": "94663", "nombre": "MAPIRIPaN" }, { "codigo": "94888", "nombre": "MORICHAL" }, { "codigo": "94887", "nombre": "PANA PANA" }, { "codigo": "94884", "nombre": "PUERTO COLOMBIA" }, { "codigo": "94883", "nombre": "SAN FELIPE" }, { "codigo": "95015", "nombre": "CALAMAR" }, { "codigo": "95025", "nombre": "EL RETORNO" }, { "codigo": "95200", "nombre": "MIRAFLORES" }, { "codigo": "95001", "nombre": "SAN JOSÉ DEL GUAVIARE" }, { "codigo": "41013", "nombre": "AGRADO" }, { "codigo": "41026", "nombre": "ALTAMIRA" }, { "codigo": "41298", "nombre": "GARZÓN" }, { "codigo": "41306", "nombre": "GIGANTE" }, { "codigo": "41319", "nombre": "GUADALUPE" }, { "codigo": "41548", "nombre": "PITAL" }, { "codigo": "41770", "nombre": "SUAZA" }, { "codigo": "41791", "nombre": "TARQUI" }, { "codigo": "41016", "nombre": "AIPE" }, { "codigo": "41020", "nombre": "ALGECIRAS" }, { "codigo": "41078", "nombre": "BARAYA" }, { "codigo": "41132", "nombre": "CAMPOALEGRE" }, { "codigo": "41206", "nombre": "COLOMBIA" }, { "codigo": "41349", "nombre": "HOBO" }, { "codigo": "41357", "nombre": "IQUIRA" }, { "codigo": "41001", "nombre": "NEIVA" }, { "codigo": "41524", "nombre": "PALERMO" }, { "codigo": "41615", "nombre": "RIVERA" }, { "codigo": "41676", "nombre": "SANTA MAR�?A" }, { "codigo": "41799", "nombre": "TELLO" }, { "codigo": "41801", "nombre": "TERUEL" }, { "codigo": "41872", "nombre": "VILLAVIEJA" }, { "codigo": "41885", "nombre": "YAGUAR�?" }, { "codigo": "41378", "nombre": "LA ARGENTINA" }, { "codigo": "41396", "nombre": "LA PLATA" }, { "codigo": "41483", "nombre": "N�?TAGA" }, { "codigo": "41518", "nombre": "PAICOL" }, { "codigo": "41797", "nombre": "TESALIA" }, { "codigo": "41006", "nombre": "ACEVEDO" }, { "codigo": "41244", "nombre": "EL�?AS" }, { "codigo": "41359", "nombre": "ISNOS" }, { "codigo": "41503", "nombre": "OPORAPA" }, { "codigo": "41530", "nombre": "PALESTINA" }, { "codigo": "41551", "nombre": "PITALITO" }, { "codigo": "41660", "nombre": "SALADOBLANCO" }, { "codigo": "41668", "nombre": "SAN AGUST�?N" }, { "codigo": "41807", "nombre": "TIMAN�?" }, { "codigo": "44035", "nombre": "ALBANIA" }, { "codigo": "44090", "nombre": "DIBULLA" }, { "codigo": "44430", "nombre": "MAICAO" }, { "codigo": "44560", "nombre": "MANAURE" }, { "codigo": "44001", "nombre": "RIOHACHA" }, { "codigo": "44847", "nombre": "URIBIA" }, { "codigo": "44078", "nombre": "BARRANCAS" }, { "codigo": "44098", "nombre": "DISTRACCION" }, { "codigo": "44110", "nombre": "EL MOLINO" }, { "codigo": "44279", "nombre": "FONSECA" }, { "codigo": "44378", "nombre": "HATONUEVO" }, { "codigo": "44420", "nombre": "LA JAGUA DEL PILAR" }, { "codigo": "44650", "nombre": "SAN JUAN DEL CESAR" }, { "codigo": "44855", "nombre": "URUMITA" }, { "codigo": "44874", "nombre": "VILLANUEVA" }, { "codigo": "47058", "nombre": "ARIGUAN�?" }, { "codigo": "47170", "nombre": "CHIBOLO" }, { "codigo": "47460", "nombre": "NUEVA GRANADA" }, { "codigo": "47555", "nombre": "PLATO" }, { "codigo": "47660", "nombre": "SABANAS DE SAN ANGEL" }, { "codigo": "47798", "nombre": "TENERIFE" }, { "codigo": "47030", "nombre": "ALGARROBO" }, { "codigo": "47053", "nombre": "ARACATACA" }, { "codigo": "47189", "nombre": "CIÉNAGA" }, { "codigo": "47268", "nombre": "EL RETEN" }, { "codigo": "47288", "nombre": "FUNDACION" }, { "codigo": "47570", "nombre": "PUEBLO VIEJO" }, { "codigo": "47980", "nombre": "ZONA BANANERA" }, { "codigo": "47161", "nombre": "CERRO SAN ANTONIO" }, { "codigo": "47205", "nombre": "CONCORDIA" }, { "codigo": "47258", "nombre": "EL PIÑON" }, { "codigo": "47541", "nombre": "PEDRAZA" }, { "codigo": "47551", "nombre": "PIVIJAY" }, { "codigo": "47605", "nombre": "REMOLINO" }, { "codigo": "47675", "nombre": "SALAMINA" }, { "codigo": "47745", "nombre": "SITIONUEVO" }, { "codigo": "47960", "nombre": "ZAPAYAN" }, { "codigo": "47001", "nombre": "SANTA MARTA" }, { "codigo": "47245", "nombre": "EL BANCO" }, { "codigo": "47318", "nombre": "GUAMAL" }, { "codigo": "47545", "nombre": "PIJIÑO DEL CARMEN" }, { "codigo": "47692", "nombre": "SAN SEBASTIAN DE BUENAVISTA" }, { "codigo": "47703", "nombre": "SAN ZENON" }, { "codigo": "47707", "nombre": "SANTA ANA" }, { "codigo": "47720", "nombre": "SANTA BARBARA DE PINTO" }, { "codigo": "50251", "nombre": "EL CASTILLO" }, { "codigo": "50270", "nombre": "EL DORADO" }, { "codigo": "50287", "nombre": "FUENTE DE ORO" }, { "codigo": "50313", "nombre": "GRANADA" }, { "codigo": "50350", "nombre": "LA MACARENA" }, { "codigo": "50370", "nombre": "LA URIBE" }, { "codigo": "50400", "nombre": "LEJAN�?AS" }, { "codigo": "50325", "nombre": "MAPIRIPaN" }, { "codigo": "50330", "nombre": "MESETAS" }, { "codigo": "50450", "nombre": "PUERTO CONCORDIA" }, { "codigo": "50577", "nombre": "PUERTO LLERAS" }, { "codigo": "50590", "nombre": "PUERTO RICO" }, { "codigo": "50683", "nombre": "SAN JUAN DE ARAMA" }, { "codigo": "50711", "nombre": "VISTA HERMOSA" }, { "codigo": "50001", "nombre": "VILLAVICENCIO" }, { "codigo": "50006", "nombre": "ACACiAS" }, { "codigo": "50110", "nombre": "BARRANCA DE UPIA" }, { "codigo": "50150", "nombre": "CASTILLA LA NUEVA" }, { "codigo": "50226", "nombre": "CUMARAL" }, { "codigo": "50245", "nombre": "EL CALVARIO" }, { "codigo": "50318", "nombre": "GUAMAL" }, { "codigo": "50606", "nombre": "RESTREPO" }, { "codigo": "50680", "nombre": "SAN CARLOS GUAROA" }, { "codigo": "50686", "nombre": "SAN JUANITO" }, { "codigo": "50223", "nombre": "SAN LUIS DE CUBARRAL" }, { "codigo": "50689", "nombre": "SAN MART�?N" }, { "codigo": "50124", "nombre": "CABUYARO" }, { "codigo": "50568", "nombre": "PUERTO GAIT�?N" }, { "codigo": "50573", "nombre": "PUERTO LoPEZ" }, { "codigo": "52240", "nombre": "CHACHAGUI" }, { "codigo": "52207", "nombre": "CONSACA" }, { "codigo": "52254", "nombre": "EL PEÑOL" }, { "codigo": "52260", "nombre": "EL TAMBO" }, { "codigo": "52381", "nombre": "LA FLORIDA" }, { "codigo": "52480", "nombre": "NARIÑO" }, { "codigo": "52001", "nombre": "PASTO" }, { "codigo": "52683", "nombre": "SANDON�?" }, { "codigo": "52788", "nombre": "TANGUA" }, { "codigo": "52885", "nombre": "YACUANQUER" }, { "codigo": "52036", "nombre": "ANCUYA" }, { "codigo": "52320", "nombre": "GUAITARILLA" }, { "codigo": "52385", "nombre": "LA LLANADA" }, { "codigo": "52411", "nombre": "LINARES" }, { "codigo": "52418", "nombre": "LOS ANDES" }, { "codigo": "52435", "nombre": "MALLAMA" }, { "codigo": "52506", "nombre": "OSPINA" }, { "codigo": "52565", "nombre": "PROVIDENCIA" }, { "codigo": "52612", "nombre": "RICAURTE" }, { "codigo": "52678", "nombre": "SAMANIEGO" }, { "codigo": "52699", "nombre": "SANTA CRUZ" }, { "codigo": "52720", "nombre": "SAPUYES" }, { "codigo": "52838", "nombre": "TUQUERRES" }, { "codigo": "52079", "nombre": "BARBACOAS" }, { "codigo": "52250", "nombre": "EL CHARCO" }, { "codigo": "52520", "nombre": "FRANCISCO PIZARRO" }, { "codigo": "52390", "nombre": "LA TOLA" }, { "codigo": "52427", "nombre": "Magui" }, { "codigo": "52473", "nombre": "MOSQUERA" }, { "codigo": "52490", "nombre": "OLAYA HERRERA" }, { "codigo": "52621", "nombre": "ROBERTO PAYAN" }, { "codigo": "52696", "nombre": "SANTA BaRBARA" }, { "codigo": "52835", "nombre": "TUMACO" }, { "codigo": "52019", "nombre": "ALBAN" }, { "codigo": "52051", "nombre": "ARBOLEDA" }, { "codigo": "52083", "nombre": "BELEN" }, { "codigo": "52110", "nombre": "BUESACO" }, { "codigo": "52203", "nombre": "COLON" }, { "codigo": "52233", "nombre": "CUMBITARA" }, { "codigo": "52256", "nombre": "EL ROSARIO" }, { "codigo": "52258", "nombre": "El Tablon de Gomez" }, { "codigo": "52378", "nombre": "LA CRUZ" }, { "codigo": "52399", "nombre": "LA UNION" }, { "codigo": "52405", "nombre": "LEIVA" }, { "codigo": "52540", "nombre": "POLICARPA" }, { "codigo": "52685", "nombre": "SAN BERNARDO" }, { "codigo": "52687", "nombre": "SAN LORENZO" }, { "codigo": "52693", "nombre": "SAN PABLO" }, { "codigo": "52694", "nombre": "SAN PEDRO DE CARTAGO" }, { "codigo": "52786", "nombre": "TAMINANGO" }, { "codigo": "52022", "nombre": "ALDANA" }, { "codigo": "52210", "nombre": "CONTADERO" }, { "codigo": "52215", "nombre": "CÓRDOBA" }, { "codigo": "52224", "nombre": "CUASPUD" }, { "codigo": "52227", "nombre": "CUMBAL" }, { "codigo": "52287", "nombre": "FUNES" }, { "codigo": "52317", "nombre": "GUACHUCAL" }, { "codigo": "52323", "nombre": "GUALMATAN" }, { "codigo": "52352", "nombre": "ILES" }, { "codigo": "52354", "nombre": "IMUES" }, { "codigo": "52356", "nombre": "IPIALES" }, { "codigo": "52560", "nombre": "POTOS�?" }, { "codigo": "52573", "nombre": "PUERRES" }, { "codigo": "52585", "nombre": "PUPIALES" }, { "codigo": "54051", "nombre": "ARBOLEDAS" }, { "codigo": "54223", "nombre": "CUCUTILLA" }, { "codigo": "54313", "nombre": "GRAMALOTE" }, { "codigo": "54418", "nombre": "LOURDES" }, { "codigo": "54660", "nombre": "SALAZAR" }, { "codigo": "54680", "nombre": "SANTIAGO" }, { "codigo": "54871", "nombre": "VILLA CARO" }, { "codigo": "54109", "nombre": "BUCARASICA" }, { "codigo": "54250", "nombre": "EL TARRA" }, { "codigo": "54720", "nombre": "SARDINATA" }, { "codigo": "54810", "nombre": "TIBÚ" }, { "codigo": "54003", "nombre": "ABREGO" }, { "codigo": "54128", "nombre": "CACHIR�?" }, { "codigo": "54206", "nombre": "CONVENCIÓN" }, { "codigo": "54245", "nombre": "EL CARMEN" }, { "codigo": "54344", "nombre": "HACAR�?" }, { "codigo": "54385", "nombre": "LA ESPERANZA" }, { "codigo": "54398", "nombre": "LA PLAYA" }, { "codigo": "54498", "nombre": "OCAÑA" }, { "codigo": "54670", "nombre": "SAN CALIXTO" }, { "codigo": "54800", "nombre": "TEORAMA" }, { "codigo": "54001", "nombre": "CÚCUTA" }, { "codigo": "54261", "nombre": "EL ZULIA" }, { "codigo": "54405", "nombre": "LOS PATIOS" }, { "codigo": "54553", "nombre": "PUERTO SANTANDER" }, { "codigo": "54673", "nombre": "SAN CAYETANO" }, { "codigo": "54874", "nombre": "VILLA DEL ROSARIO" }, { "codigo": "54125", "nombre": "C�?COTA" }, { "codigo": "54174", "nombre": "CHITAG�?" }, { "codigo": "54480", "nombre": "MUTISCUA" }, { "codigo": "54518", "nombre": "PAMPLONA" }, { "codigo": "54520", "nombre": "PAMPLONITA" }, { "codigo": "54743", "nombre": "SILOS" }, { "codigo": "54099", "nombre": "BOCHALEMA" }, { "codigo": "54172", "nombre": "CHIN�?COTA" }, { "codigo": "54239", "nombre": "DURANIA" }, { "codigo": "54347", "nombre": "HERR�?N" }, { "codigo": "54377", "nombre": "LABATECA" }, { "codigo": "54599", "nombre": "RAGONVALIA" }, { "codigo": "54820", "nombre": "TOLEDO" }, { "codigo": "86219", "nombre": "COLÓN" }, { "codigo": "86001", "nombre": "MOCOA" }, { "codigo": "86320", "nombre": "ORITO" }, { "codigo": "86568", "nombre": "PUERTO ASIS" }, { "codigo": "86569", "nombre": "PUERTO CAICEDO" }, { "codigo": "86571", "nombre": "PUERTO GUZMAN" }, { "codigo": "86573", "nombre": "PUERTO LEGUIZAMO" }, { "codigo": "86755", "nombre": "SAN FRANCISCO" }, { "codigo": "86757", "nombre": "SAN MIGUEL" }, { "codigo": "86760", "nombre": "SANTIAGO" }, { "codigo": "86749", "nombre": "SIBUNDOY" }, { "codigo": "86865", "nombre": "VALLE DEL GUAMUEZ" }, { "codigo": "86885", "nombre": "VILLA GARZON" }, { "codigo": "63001", "nombre": "ARMENIA" }, { "codigo": "63111", "nombre": "BUENAVISTA" }, { "codigo": "63130", "nombre": "CALARCA" }, { "codigo": "63212", "nombre": "CoRDOBA" }, { "codigo": "63302", "nombre": "GeNOVA" }, { "codigo": "63548", "nombre": "PIJAO" }, { "codigo": "63272", "nombre": "FILANDIA" }, { "codigo": "63690", "nombre": "SALENTO" }, { "codigo": "63190", "nombre": "CIRCASIA" }, { "codigo": "63401", "nombre": "LA TEBAIDA" }, { "codigo": "63470", "nombre": "Montengro" }, { "codigo": "63594", "nombre": "QUIMBAYA" }, { "codigo": "66170", "nombre": "DOSQUEBRADAS" }, { "codigo": "66400", "nombre": "LA VIRGINIA" }, { "codigo": "66440", "nombre": "MARSELLA" }, { "codigo": "66001", "nombre": "PEREIRA" }, { "codigo": "66682", "nombre": "SANTA ROSA DE CABAL" }, { "codigo": "66045", "nombre": "AP�?A" }, { "codigo": "66075", "nombre": "BALBOA" }, { "codigo": "66088", "nombre": "BELÉN DE UMBR�?A" }, { "codigo": "66318", "nombre": "GU�?TICA" }, { "codigo": "66383", "nombre": "LA CELIA" }, { "codigo": "66594", "nombre": "QUINCHiA" }, { "codigo": "66687", "nombre": "SANTUARIO" }, { "codigo": "66456", "nombre": "MISTRATÓ" }, { "codigo": "66572", "nombre": "PUEBLO RICO" }, { "codigo": "68176", "nombre": "CHIMA" }, { "codigo": "68209", "nombre": "CONFINES" }, { "codigo": "68211", "nombre": "CONTRATACIÓN" }, { "codigo": "68245", "nombre": "EL GUACAMAYO" }, { "codigo": "68296", "nombre": "GAL�?N" }, { "codigo": "68298", "nombre": "GAMBITA" }, { "codigo": "68320", "nombre": "GUADALUPE" }, { "codigo": "68322", "nombre": "GUAPOT�?" }, { "codigo": "68344", "nombre": "HATO" }, { "codigo": "68500", "nombre": "OIBA" }, { "codigo": "68522", "nombre": "PALMAR" }, { "codigo": "68524", "nombre": "PALMAS DEL SOCORRO" }, { "codigo": "68720", "nombre": "SANTA HELENA DEL OPÓN" }, { "codigo": "68745", "nombre": "SIMACOTA" }, { "codigo": "68755", "nombre": "SOCORRO" }, { "codigo": "68770", "nombre": "SUAITA" }, { "codigo": "68147", "nombre": "CAPITANEJO" }, { "codigo": "68152", "nombre": "CARCAS�?" }, { "codigo": "68160", "nombre": "CEPIT�?" }, { "codigo": "68162", "nombre": "CERRITO" }, { "codigo": "68207", "nombre": "CONCEPCIÓN" }, { "codigo": "68266", "nombre": "ENCISO" }, { "codigo": "68318", "nombre": "GUACA" }, { "codigo": "68425", "nombre": "MACARAVITA" }, { "codigo": "68432", "nombre": "M�?LAGA" }, { "codigo": "68468", "nombre": "MOLAGAVITA" }, { "codigo": "68669", "nombre": "SAN ANDRÉS" }, { "codigo": "68684", "nombre": "SAN JOSÉ DE MIRANDA" }, { "codigo": "68686", "nombre": "SAN MIGUEL" }, { "codigo": "68051", "nombre": "ARATOCA" }, { "codigo": "68079", "nombre": "BARICHARA" }, { "codigo": "68121", "nombre": "CABRERA" }, { "codigo": "68167", "nombre": "CHARAL�?" }, { "codigo": "68217", "nombre": "COROMORO" }, { "codigo": "68229", "nombre": "CURIT�?" }, { "codigo": "68264", "nombre": "ENCINO" }, { "codigo": "68370", "nombre": "JORD�?N" }, { "codigo": "68464", "nombre": "MOGOTES" }, { "codigo": "68498", "nombre": "OCAMONTE" }, { "codigo": "68502", "nombre": "ONZAGA" }, { "codigo": "68533", "nombre": "P�?RAMO" }, { "codigo": "68549", "nombre": "PINCHOTE" }, { "codigo": "68679", "nombre": "SAN GIL" }, { "codigo": "68682", "nombre": "SAN JOAQU�?N" }, { "codigo": "68855", "nombre": "VALLE DE SAN JOSÉ" }, { "codigo": "68872", "nombre": "VILLANUEVA" }, { "codigo": "68081", "nombre": "BARRANCABERMEJA" }, { "codigo": "68092", "nombre": "BETULIA" }, { "codigo": "68235", "nombre": "EL CARMEN DE CHUCUR�?" }, { "codigo": "68575", "nombre": "PUERTO WILCHES" }, { "codigo": "68655", "nombre": "SABANA DE TORRES" }, { "codigo": "68689", "nombre": "SAN VICENTE DE CHUCUR�?" }, { "codigo": "68895", "nombre": "ZAPATOCA" }, { "codigo": "68001", "nombre": "BUCARAMANGA" }, { "codigo": "68132", "nombre": "CALIFORNIA" }, { "codigo": "68169", "nombre": "CHARTA" }, { "codigo": "68255", "nombre": "EL PLAYÓN" }, { "codigo": "68276", "nombre": "FLORIDABLANCA" }, { "codigo": "68307", "nombre": "GIRÓN" }, { "codigo": "68406", "nombre": "LEBR�?JA" }, { "codigo": "68418", "nombre": "LOS SANTOS" }, { "codigo": "68444", "nombre": "MATANZA" }, { "codigo": "68547", "nombre": "PIEDECUESTA" }, { "codigo": "68615", "nombre": "RIONEGRO" }, { "codigo": "68705", "nombre": "SANTA B�?RBARA" }, { "codigo": "68780", "nombre": "SURATA" }, { "codigo": "68820", "nombre": "TONA" }, { "codigo": "68867", "nombre": "VETAS" }, { "codigo": "68013", "nombre": "AGUADA" }, { "codigo": "68020", "nombre": "ALBANIA" }, { "codigo": "68077", "nombre": "BARBOSA" }, { "codigo": "68101", "nombre": "BOL�?VAR" }, { "codigo": "68179", "nombre": "CHIPAT�?" }, { "codigo": "68190", "nombre": "CIMITARRA" }, { "codigo": "68250", "nombre": "EL PEÑÓN" }, { "codigo": "68271", "nombre": "FLORI�?N" }, { "codigo": "68324", "nombre": "GUAVAT�?" }, { "codigo": "68327", "nombre": "GuEPSA" }, { "codigo": "68368", "nombre": "JESÚS MAR�?A" }, { "codigo": "68377", "nombre": "LA BELLEZA" }, { "codigo": "68397", "nombre": "LA PAZ" }, { "codigo": "68385", "nombre": "LAND�?ZURI" }, { "codigo": "68572", "nombre": "PUENTE NACIONAL" }, { "codigo": "68573", "nombre": "PUERTO PARRA" }, { "codigo": "68673", "nombre": "SAN BENITO" }, { "codigo": "68773", "nombre": "SUCRE" }, { "codigo": "68861", "nombre": "VÉLEZ" }, { "codigo": "70265", "nombre": "GUARANDA" }, { "codigo": "70429", "nombre": "MAJAGUAL" }, { "codigo": "70771", "nombre": "SUCRE" }, { "codigo": "70230", "nombre": "CHAL�?N" }, { "codigo": "70204", "nombre": "COLOSO" }, { "codigo": "70473", "nombre": "MORROA" }, { "codigo": "70508", "nombre": "OVEJAS" }, { "codigo": "70001", "nombre": "SINCELEJO" }, { "codigo": "70221", "nombre": "COVEÑAS" }, { "codigo": "70523", "nombre": "PALMITO" }, { "codigo": "70713", "nombre": "SAN ONOFRE" }, { "codigo": "70820", "nombre": "SANTIAGO DE TOLÚ" }, { "codigo": "70823", "nombre": "TOLÚ VIEJO" }, { "codigo": "70110", "nombre": "BUENAVISTA" }, { "codigo": "70215", "nombre": "COROZAL" }, { "codigo": "70233", "nombre": "EL ROBLE" }, { "codigo": "70235", "nombre": "GALERAS" }, { "codigo": "70418", "nombre": "LOS PALMITOS" }, { "codigo": "70670", "nombre": "SAMPUÉS" }, { "codigo": "70702", "nombre": "SAN JUAN BETULIA" }, { "codigo": "70717", "nombre": "SAN PEDRO" }, { "codigo": "70742", "nombre": "SINCÉ" }, { "codigo": "70124", "nombre": "CAIMITO" }, { "codigo": "70400", "nombre": "LA UNIÓN" }, { "codigo": "70678", "nombre": "SAN BENITO ABAD" }, { "codigo": "70708", "nombre": "SAN MARCOS" }, { "codigo": "73030", "nombre": "AMBALEMA" }, { "codigo": "73055", "nombre": "ARMERO" }, { "codigo": "73270", "nombre": "FALAN" }, { "codigo": "73283", "nombre": "FRESNO" }, { "codigo": "73349", "nombre": "HONDA" }, { "codigo": "73443", "nombre": "MARIQUITA" }, { "codigo": "73520", "nombre": "PALOCABILDO" }, { "codigo": "73148", "nombre": "CARMEN DE APICAL�?" }, { "codigo": "73226", "nombre": "CUNDAY" }, { "codigo": "73352", "nombre": "ICONONZO" }, { "codigo": "73449", "nombre": "MELGAR" }, { "codigo": "73873", "nombre": "VILLARRICA" }, { "codigo": "73067", "nombre": "ATACO" }, { "codigo": "73168", "nombre": "CHAPARRAL" }, { "codigo": "73217", "nombre": "COYAIMA" }, { "codigo": "73483", "nombre": "NATAGAIMA" }, { "codigo": "73504", "nombre": "ORTEGA" }, { "codigo": "73555", "nombre": "PLANADAS" }, { "codigo": "73616", "nombre": "RIOBLANCO" }, { "codigo": "73622", "nombre": "RONCESVALLES" }, { "codigo": "73675", "nombre": "SAN ANTONIO" }, { "codigo": "73026", "nombre": "ALVARADO" }, { "codigo": "73043", "nombre": "ANZO�?TEGUI" }, { "codigo": "73124", "nombre": "CAJAMARCA" }, { "codigo": "73200", "nombre": "COELLO" }, { "codigo": "73268", "nombre": "ESPINAL" }, { "codigo": "73275", "nombre": "FLANDES" }, { "codigo": "73001", "nombre": "IBAGUe" }, { "codigo": "73547", "nombre": "PIEDRAS" }, { "codigo": "73624", "nombre": "ROVIRA" }, { "codigo": "73678", "nombre": "SAN LUIS" }, { "codigo": "73854", "nombre": "VALLE DE SAN JUAN" }, { "codigo": "73024", "nombre": "ALPUJARRA" }, { "codigo": "73236", "nombre": "DOLORES" }, { "codigo": "73319", "nombre": "GUAMO" }, { "codigo": "73563", "nombre": "PRADO" }, { "codigo": "73585", "nombre": "PURIFICACIÓN" }, { "codigo": "73671", "nombre": "SALDAÑA" }, { "codigo": "73770", "nombre": "SU�?REZ" }, { "codigo": "73152", "nombre": "CASABIANCA" }, { "codigo": "73347", "nombre": "HERVEO" }, { "codigo": "73408", "nombre": "LeRIDA" }, { "codigo": "73411", "nombre": "LiBANO" }, { "codigo": "73461", "nombre": "MURILLO" }, { "codigo": "73686", "nombre": "SANTA ISABEL" }, { "codigo": "73861", "nombre": "VENADILLO" }, { "codigo": "73870", "nombre": "VILLAHERMOSA" }, { "codigo": "76036", "nombre": "ANDALUC�?A" }, { "codigo": "76111", "nombre": "BUGA" }, { "codigo": "76113", "nombre": "BUGALAGRANDE" }, { "codigo": "76126", "nombre": "CALIMA" }, { "codigo": "76248", "nombre": "EL CERRITO" }, { "codigo": "76306", "nombre": "GINEBRA" }, { "codigo": "76318", "nombre": "GUACAR�?" }, { "codigo": "76606", "nombre": "RESTREPO" }, { "codigo": "76616", "nombre": "RIOFRIO" }, { "codigo": "76670", "nombre": "SAN PEDRO" }, { "codigo": "76828", "nombre": "TRUJILLO" }, { "codigo": "76834", "nombre": "TULU�?" }, { "codigo": "76890", "nombre": "YOTOCO" }, { "codigo": "76020", "nombre": "ALCALa" }, { "codigo": "76041", "nombre": "ANSERMANUEVO" }, { "codigo": "76054", "nombre": "ARGELIA" }, { "codigo": "76100", "nombre": "BOL�?VAR" }, { "codigo": "76147", "nombre": "CARTAGO" }, { "codigo": "76243", "nombre": "EL �?GUILA" }, { "codigo": "76246", "nombre": "EL CAIRO" }, { "codigo": "76250", "nombre": "EL DOVIO" }, { "codigo": "76400", "nombre": "LA UNIÓN" }, { "codigo": "76403", "nombre": "LA VICTORIA" }, { "codigo": "76497", "nombre": "OBANDO" }, { "codigo": "76622", "nombre": "ROLDANILLO" }, { "codigo": "76823", "nombre": "TORO" }, { "codigo": "76845", "nombre": "ULLOA" }, { "codigo": "76863", "nombre": "VERSALLES" }, { "codigo": "76895", "nombre": "ZARZAL" }, { "codigo": "76109", "nombre": "BUENAVENTURA" }, { "codigo": "76122", "nombre": "CAICEDONIA" }, { "codigo": "76736", "nombre": "SEVILLA" }, { "codigo": "76001", "nombre": "CALI" }, { "codigo": "76130", "nombre": "CANDELARIA" }, { "codigo": "76233", "nombre": "DAGUA" }, { "codigo": "76275", "nombre": "FLORIDA" }, { "codigo": "76364", "nombre": "JAMUND�?" }, { "codigo": "76377", "nombre": "LA CUMBRE" }, { "codigo": "76520", "nombre": "PALMIRA" }, { "codigo": "76563", "nombre": "PRADERA" }, { "codigo": "76869", "nombre": "VIJES" }, { "codigo": "76892", "nombre": "YUMBO" }, { "codigo": "97161", "nombre": "CARURU" }, { "codigo": "97001", "nombre": "MITÚ" }, { "codigo": "97511", "nombre": "PACOA" }, { "codigo": "97777", "nombre": "PAPUNAHUA" }, { "codigo": "97666", "nombre": "TARAIRA" }, { "codigo": "97889", "nombre": "YAVARATÉ" }, { "codigo": "99773", "nombre": "CUMARIBO" }, { "codigo": "99524", "nombre": "LA PRIMAVERA" }, { "codigo": "99001", "nombre": "PUERTO CARREÑO" }, { "codigo": "99624", "nombre": "SANTA ROSAL�?A" }],
		idBeforeShop: null,
		paymentMethods: null,
		initialURL: null,
		urlApp : null
	},
	init: function() {
		
		if(location.href.indexOf("localhost") < 0){
			clientPrincipal.global.urlApp = "http://35.196.101.193:8080/sinpecado/"
		}else{
			clientPrincipal.global.urlApp = "http://localhost:8080/";
		}

		clientPrincipal.global.firstName = localStorage.getItem('firstName');
		clientPrincipal.global.lastName = localStorage.getItem('lastName');
		clientPrincipal.global.email = localStorage.getItem('email');
		clientPrincipal.ofertas.init();
		clientPrincipal.top.init();
		clientPrincipal.sorprendelo.init();
		clientPrincipal.carrito.init();
		
		setTimeout(function() {

			$(".btn-add-car").click(function() {

				var idItem = this.dataset.id;
				var amount = 1;
				localStorage.setItem('lastAction', "add");
				clientPrincipal.carrito.addCarrito(idItem, amount);
			});
		}, 3000);
		
		if (clientPrincipal.global.email === null) {

			$("#infoUser").hide();
		} else {

			$("#fullnameUser").html((clientPrincipal.global.firstName + " " + clientPrincipal.global.lastName).toUpperCase());
			$("#btnLoginUser").hide();
			$("#btnRegisterUser").hide();
		}

		$("#masVendido").hide();
	},
	
	main : {
		
		client : {
			
			init : function(){
				
				$(function () {
	                $('[data-toggle="tooltip"]').tooltip()
	            });
	
	            $("#navmenu").hide();
	
	            $(window).scroll(function (event) {
	                var scroll = $(window).scrollTop();
	
	                if (scroll > 400) {
	                    $("#navmenu").show();
	                } else if (scroll < 400) {
	                    $("#navmenu").hide();
	                }
	            });
	
	            $(".items-buscar").hide();
	            $("#btnVer").hide();
	
	            $("#btnDiscrecion").click(function () {
	                $(".productos .card-img-top").hide();
	                $(".sorprende .card-img-top").hide();
	                $(".informate .card-img-top").hide();
	                $("#contenidoCarrito .card-img-top").hide();
	                $("#btnDiscrecion").hide();
	                $("#btnVer").show();
	            });
	
	            $("#btnVer").click(function () {
	                $(".productos .card-img-top").show();
	                $(".sorprende .card-img-top").show();
	                $(".informate .card-img-top").show();
	                $("#contenidoCarrito .card-img-top").show();
	                $("#btnVer").hide();
	                $("#btnDiscrecion").show();
	            });
	
	            $("#btnBuscar").click(function () {
	                $(".items-buscar").show();
	                $(".items-menu").hide();
	            });
	
	            $("#navbarDropdown").hover(function () {
	                $(this).addClass("hover");
	                $("#dropDownDifraz").dropdown('toggle');
	            }, function () {
	            });
	
	            $("#dropDownDifraz").hover(function () {
	
	            }, function () {
	                $("#dropDownDifraz").dropdown('hide');
	            });
	
	            $("#navbarSupportedContent").hover(function () {
	
	            }, function () {
	                $("#dropDownDifraz").dropdown('hide');
	            });
	            

			},
			
			openViewProduct : function(){
				
				$(".img-producto").unbind('click');
				$(".img-producto").click(function(event){
					
					var contentImagen = null;
					
					contentImagen = $(this).parent().parent();
					
					$("#modalDetalle").modal('show');
					
					$("#contenidoDetalle").append($(this).parent());
					
					$("#modalDetalle").unbind('hidden.bs.modal');
					$("#modalDetalle").on('hidden.bs.modal', function (event) {
						contentImagen.append($("#contenidoDetalle").children());
						
					});
					
				});
			}
		}
	},
	
	ofertas: {

		global: {
			result: null
		},
		init: function() {

			var jsonRequest = {};
			jsonRequest.pager = {};
			jsonRequest.pager.size = 6;
			
			var params = {
				url: clientPrincipal.global.urlApp+"offers/getAllOfertasItems",
				requestBody: jsonRequest,
				customLoad: "getResultOfertas",
				customResult: "globla.result",
				divRender: "divOfertas"
			};
			
			template.ajax.post.get(params, clientPrincipal.ofertas, null);
		},
		getResultOfertas: function(result) {

			var arrayData;
			var html = '';
			
			arrayData = result.data.content;
			
			if (arrayData.length > 0) {

				var options = { style: 'currency', currency: 'COP' };
				var numberFormat = new Intl.NumberFormat('co-CO', options);
				$.each(arrayData, function(index, ofertaData) {

					var precioIva = numberFormat.format(ofertaData.ivaPrice).split(",")[0];
					var precioDecuento = numberFormat.format(ofertaData.finalPrice).split(",")[0];
					html += '<div id="divOferta'+index+'" class="col mb-5">';
					html += '    <div class="card h-100 sin-borde">';
					html += '        <img src="extrasources/images/' + ofertaData.url + '" class="card-img-top img-producto" alt="...">';
					html += '        <div class="card-body">';
					html += '            <h5 class="card-title title-corporativo"> ' + ofertaData.itemDescription + '  </h5>';
					html += '            <p class="card-text">' + (ofertaData.itemObservation != undefined && ofertaData.itemObservation != null ? ofertaData.itemObservation : "") + '</p>';
					html += '            <h6 class="card-title">antes :$' + precioIva + '</h6>';
					html += '            <h5 class="card-title antes">Ahora: $' + precioDecuento + '</h5>';
					html += '            <a data-id=' + ofertaData.idItem + ' class="btn btn-danger btn-sm btn-add-car">Añadir</a>';
					html += '        </div>';
					html += '    </div>';
					html += '</div>';
				});
			}else{
				$("#divCardOfertas").hide();
			}

			$("#" + result.paramsGet.divRender).html(html);
			
			clientPrincipal.main.client.openViewProduct();
			
		}

	},
	top: {

		init: function() {
			clientPrincipal.top.nuevos.init()
		},
		nuevos: {

			global: {
				result: null
			},
			init: function() {

				var jsonRequest = {};
				jsonRequest.pager = {};
				jsonRequest.pager.size = 4;
				
				var params = {
					url: clientPrincipal.global.urlApp+"items/get/getAllItemsPhoto",
					requestBody: jsonRequest,
					customLoad: "getResultItems",
					customResult: "globla.result",
					divRender: "divItemsNuevos"
				};
				
				template.ajax.post.get(params, clientPrincipal.top.nuevos, null);
			},
			getResultItems: function(result) {

				var arrayData;
				var html = '';
				
				arrayData = result.data.content;
				
				if (arrayData.length > 0) {

					var options = { style: 'currency', currency: 'COP' };
					var numberFormat = new Intl.NumberFormat('co-CO', options);
					$.each(arrayData, function(index, itemData) {

						var precioIva = numberFormat.format(itemData.ivaPrice).split(",")[0];
						
						html += '<div id="divNuevos'+index+'" class="col mb-4">';
						html += '                <div class="card h-100 sin-borde">';
						html += '                    <img src="extrasources/images/' + itemData.url + '" class="card-img-top img-producto" alt="...">';
						html += '                    <div class="card-body">';
						html += '                        <h5 class="card-title title-corporativo">' + itemData.description + '</h5>';
						html += '                        <p class="card-text">' + (itemData.observation != null ? itemData.observation : "") + '</p>';
						html += '                        <h4 class="card-title">$' + precioIva + '</h4>';
						html += '                        <a data-id=' + itemData.idItem + ' class="btn btn-danger btn-sm btn-add-car">Añadir</a>';
						html += '                    </div>';
						html += '                </div>';
						html += '            </div>';
					});
				}else{
					$("#divCardItems").hide();
				}

				$("#" + result.paramsGet.divRender).html(html);
				
				clientPrincipal.main.client.openViewProduct();
				
			}

		},
		ventas: {

		},
		buscado: {

		}

	},
	sorprendelo: {

		init: function() {
			clientPrincipal.sorprendelo.lenceria.init()
		},
		global: {
			result: null
		},
		lenceria: {

			init: function() {

				var jsonRequest = {};
				jsonRequest.pager = {};
				jsonRequest.pager.size = 4;
				jsonRequest.type = 'LENCERIA'

				var params = {
					url: clientPrincipal.global.urlApp+"items/get/getAllItemsPhoto",
					requestBody: jsonRequest,
					customLoad: "getLenceria",
					customResult: "globla.result",
					divRender: "divSorprendelo"
				};
				
				template.ajax.post.get(params, clientPrincipal.sorprendelo.lenceria, null);
			},
			getLenceria: function(result) {

				var arrayData;
				var html = '';
				
				arrayData = result.data.content;
				
				if (arrayData.length > 0) {

					var options = { style: 'currency', currency: 'COP' };
					var numberFormat = new Intl.NumberFormat('co-CO', options);
					
					$.each(arrayData, function(index, itemData) {

						var precioIva = numberFormat.format(itemData.ivaPrice).split(",")[0];
						html += '<div id="divSorprendelo'+index+'" class="col mb-6 text-center">';
						html += '        <div class="card h-100 sin-borde">';
						html += '            <img src="extrasources/images/' + itemData.url + '" class="card-img-top img-producto" alt="...">';
						html += '            <div class="card-body">';
						html += '                <h6 class="card-title title-corporativo">' + itemData.description + '</h6>';
						html += '                <div>';
						html += '                    <h4 class="card-title">$' + precioIva + '</h4>';
						html += '                    <a  data-id=' + itemData.idItem + ' class="btn btn-danger btn-sm btn-add-car">Añadir</a>';
						html += '                </div>';
						html += '            </div>';
						html += '        </div>';
						html += '    </div>';
					});
					
				}else{
					$("#divCardSorprendelo").hide();
				}

				$("#" + result.paramsGet.divRender).html(html);
				
				clientPrincipal.main.client.openViewProduct();
				
			}

		}


	},
	tips: {


		init: function() {

		}

	},
	carrito: {

		init: function() {

			var jsonRequest = {};
			
			clientPrincipal.global.idShop = localStorage.getItem('idShop')

			jsonRequest.idShop = clientPrincipal.global.idShop;
			
			var params = {
				url: clientPrincipal.global.urlApp+"shops/listCar",
				requestBody: jsonRequest,
				customLoad: "getCarrito",
				customResult: "globla.result",
				divRender: "contenidoCarrito"
			};
			
			template.ajax.post.get(params, clientPrincipal.carrito, null);
		},
		
		global: {
			result: null
		},
		
		getCarrito: function(result) {

			var arrayData;
			var html = "De momento no tiene articulos en su carrito.";
			var cantidadItemsCompras = 0;
			var lastIdItem = localStorage.getItem('lastIdItem') === null ? 0 : localStorage.getItem('lastIdItem');
			var lastAction = localStorage.getItem('lastAction') === null ? "NON" : localStorage.getItem('lastAction');
			
			arrayData = result.data;
			
			if (arrayData.length > 0) {

				var options = { style: 'currency', currency: 'COP' };
				var numberFormat = new Intl.NumberFormat('co-CO', options);
				
				clientPrincipal.global.itemsCarrito = arrayData;
				html = '';
				
				$.each(arrayData, function(index, itemData) {

					var precioIva = numberFormat.format(itemData.totalPrice).split(",")[0];
					
					cantidadItemsCompras = cantidadItemsCompras + itemData.amount;
					
					html += '<div class="card">';
					html += '	<div class="card-body">';
					html += '        	<div class="row">';
					html += '        		<div class="col-6">';
					html += '        			<h6 class="card-title modal-title-item-1">' + itemData.description + '</h6>';
					html += '        			<img src="extrasources/images/' + itemData.url + '" class="card-img-top" alt="..." style="width: 120px">';
					html += '        		</div>';
					html += '        		<div class="col-6 text-center">';
					html += '        			<h6 class="card-title modal-title-item-1">$' + precioIva + '</h6>';
					html += '            		<div>';
					html += '                	<div class="row">';
					html += '                		<div class="col-3">';
					html += '                			<a data-id=' + itemData.idItemShopTemp.idItem + ' class="btn btn-light btn-sm btn-less-car">-</a>';
					html += '                		</div>';
					html += '                		<div class="col-6">';
					html += '                			<p class="card-title"><input id="amount-' + itemData.idItemShopTemp.idItem + '"  class="form-control text-center" value="' + itemData.amount + '"></p>';
					html += '                		</div>';
					html += '                		<div class="col-3">';
					html += '                			<a data-id=' + itemData.idItemShopTemp.idItem + ' class="btn btn-light btn-sm btn-more-car">+</a>';
					html += '                		</div>';
					html += '                	</div>';
					html += '                	<div class="row">';
					html += '                		<div class="col-12">';
					html += '                			<button data-id=' + itemData.idItemShopTemp.idItem + ' class="btn btn-sm btn-delete-car">Eliminar</button>';
					html += '                		</div>';
					html += '                	</div>';
					html += '            		</div>';
					html += '        		</div>';
					html += '        	</div>';
					html += '    </div>';
					html += '</div><br>';
					
					if (lastAction === "add" && parseInt(lastIdItem) === itemData.idItemShopTemp.idItem) {
						$("#toast-image-add-car").html('<img src="extrasources/images/' + itemData.url + '" class="card-img-top" alt="..." style="width: 120px">');
						$("#toast-title-add-car").html(itemData.description);
						$("#liveToast").toast('show');
						localStorage.removeItem('lastIdItem');
						localStorage.removeItem('lastAction');
					}

				});
				
				$("#cantidadItemsCompras").html(cantidadItemsCompras);
				
			} else {
				$("#cantidadItemsCompras").html(cantidadItemsCompras);
			}

			$("#" + result.paramsGet.divRender).html(html);
			
			setTimeout(function() {

				clientPrincipal.carrito.buttonsCarrito();
			}, 1000);
		},
		
		buttonsCarrito: function() {

			$(".btn-delete-car").click(function() {

				var idItem = this.dataset.id;
				localStorage.setItem('lastAction', "delete");
				clientPrincipal.carrito.deleteCarrito(idItem);
			});
			
			$(".btn-less-car").click(function() {

				var idItem = this.dataset.id;
				var amount = $("#amount-" + idItem).val();
				
				amount = parseInt(amount) - 1;
				localStorage.setItem('lastAction', "delete");
				
				if (amount === 0) {
					clientPrincipal.carrito.deleteCarrito(idItem);
				} else {
					clientPrincipal.carrito.addCarrito(idItem, amount, true);
				}


			});
			
			$(".btn-more-car").click(function() {

				var idItem = this.dataset.id;
				var amount = $("#amount-" + idItem).val();
				
				amount = parseInt(amount) + 1;
				localStorage.setItem('lastAction', "add");
				clientPrincipal.carrito.addCarrito(idItem, amount, true);
			});
			
			$("#btn-clear-car").click(function() {

				if (clientPrincipal.global.itemsCarrito != null && clientPrincipal.global.itemsCarrito.length > 0) {

					$.each(clientPrincipal.global.itemsCarrito, function(index, itemData) {

						clientPrincipal.carrito.deleteCarrito(itemData.idItemShopTemp.idItem);
					});
					$("#modalCarrito").modal('hide');
					$("#contenidoCarrito").html("De momento no tiene articulos en su carrito.");
				}

			});
			
			$("#btn-to-pay").click(function() {

				clientPrincipal.carrito.sendBeforeShop();
			});
		},
		
		deleteCarrito: function(idItem) {

			var jsonRequest = {};
			
			clientPrincipal.global.idShop = localStorage.getItem('idShop')

			jsonRequest.idShop = clientPrincipal.global.idShop;
			jsonRequest.idItem = idItem;
			
			var params = {
				url: clientPrincipal.global.urlApp+"shops/deleteCar",
				requestBody: jsonRequest,
				customLoad: "afterDeleteCarrito",
				customResult: "globla.result",
				divRender: "contenidoCarrito"
			};
			
			template.ajax.post.get(params, clientPrincipal.carrito, null);
		},
		
		addCarrito: function(idItem, amount, isCarrito) {

			var jsonRequest = {};
			
			clientPrincipal.global.idShop = localStorage.getItem('idShop');
			jsonRequest.idShop = clientPrincipal.global.idShop;
			jsonRequest.idItem = idItem;
			jsonRequest.amount = amount;
			jsonRequest.isCarrito = isCarrito == undefined ? false : isCarrito

			var params = {
				url: clientPrincipal.global.urlApp+"shops/addCar",
				requestBody: jsonRequest,
				customLoad: "afterAddCarrito",
				customResult: "globla.result",
				divRender: "contenidoCarrito"
			};
			
			template.ajax.post.get(params, clientPrincipal.carrito, null);
		},
		
		afterAddCarrito: function(result) {

			if (result.succes) {

				clientPrincipal.global.idShop = result.data.idItemShopTemp.idShop;
				localStorage.setItem('idShop', clientPrincipal.global.idShop);
				localStorage.setItem('lastIdItem', result.data.idItemShopTemp.idItem);
				localStorage.setItem("shopStatus", "INPROCESS");
				clientPrincipal.carrito.init();
			}

		},
		
		afterDeleteCarrito: function(result) {

			if (result.succes) {
				clientPrincipal.carrito.init();
			}


		},
		
		sendBeforeShop: function() {

			var jsonRequest = {};
			
			clientPrincipal.global.idShop = localStorage.getItem('idShop');
			jsonRequest.idShop = clientPrincipal.global.idShop;
			
			var params = {
				url: clientPrincipal.global.urlApp+"shops/sendBeforeShop",
				requestBody: jsonRequest,
				customLoad: "afterSendBeforeShop",
				customResult: "globla.result",
				divRender: "contenidoCarrito"
			};
			
			template.ajax.post.get(params, clientPrincipal.carrito, null);
		},
		
		afterSendBeforeShop: function(result) {

			if (result.succes) {

				var total = null;
				var subTotal = null;
				var tax = null;
				var discount = null;
				var options = { style: 'currency', currency: 'COP' };
				var numberFormat = null;
				
				clientPrincipal.global.idBeforeShop = result.data.idPreMercadoLibre;
				clientPrincipal.global.initialURL = result.data.initialURL;
				
				$("#modalCarrito").modal('hide');
				numberFormat = new Intl.NumberFormat('co-CO', options);
				subTotal = numberFormat.format(result.data.subTotal);
				tax = numberFormat.format(result.data.tax);
				discount = numberFormat.format(result.data.discount);
				total = numberFormat.format(result.data.total);
				
				localStorage.setItem('subTotalCheckout', subTotal);
				localStorage.setItem('impuestosCheckout', tax);
				localStorage.setItem('descuentoCheckout', discount);
				localStorage.setItem('totalCheckout', total);
				localStorage.setItem('idBeforeShop', clientPrincipal.global.idBeforeShop);
				localStorage.setItem('initialURL', clientPrincipal.global.initialURL);
				
				if (localStorage.getItem('idUser') === null) {

					localStorage.setItem('urlAnterior', clientPrincipal.global.urlApp+"checkout");
					window.location.href = clientPrincipal.global.urlApp+"login";
					
				} else {
					window.location.href = clientPrincipal.global.urlApp+"checkout";
				}
			}


		},
		
		createCheckoutButton: function() {

			$("#contenido-pago-btn").html('<a class="btn btn-primary" onclick="clientPrincipal.carrito.beforePayMercadoLibre()">Realizar pedido</a>');
		},
		
		beforePayMercadoLibre: function() {
			
			var jsonValidateResponse = null;
			
			jsonValidateResponse = template.html.validateInputs("contenidoPago", true);
				
			if(jsonValidateResponse.valid){
				localStorage.setItem('city', $("#city").val());
				localStorage.setItem('descriptionCity', $("#selectCode").val());
				window.location.href = clientPrincipal.global.initialURL;
			}
			
		}
	},
	
	checkout: {

		init: function(){

			var subTotal = null;
			var tax = null;
			var discount = null;
			var total = null;
			var identification = null;
			var phone = null;
			
			clientPrincipal.checkout.inputsCheckout();
			
			if (localStorage.getItem('idUser') === null) {

				localStorage.setItem('urlAnterior', location.href);
				window.location.href = clientPrincipal.global.urlApp+"login";
			} else {

				if (localStorage.getItem("shopStatus") != null && localStorage.getItem("shopStatus") === "INPROCESS") {
					clientPrincipal.shop.saveShop();
				}

			}

			identification = localStorage.getItem('identification');
			phone = localStorage.getItem('phone');
			
			if (identification != null) {
				$("#contenidoPago #identification").val(identification);
			}

			if (phone != null) {
				$("#contenidoPago #phone").val(phone);
			}

			subTotal = localStorage.getItem('subTotalCheckout');
			tax = localStorage.getItem('impuestosCheckout');
			discount = localStorage.getItem('descuentoCheckout');
			total = localStorage.getItem('totalCheckout');
			
			if (total === null) {
				window.location.href = clientPrincipal.global.urlApp+"";
			}

			clientPrincipal.global.firstName = localStorage.getItem('firstName');
			clientPrincipal.global.lastName = localStorage.getItem('lastName');
			clientPrincipal.global.email = localStorage.getItem('email');
			
			$("#contenidoPago #fullname").val(clientPrincipal.global.firstName + " " + clientPrincipal.global.lastName);
			$("#contenidoPago #email").val(clientPrincipal.global.email);
			$("#contenidoPago #fullname").prop("disabled", true);
			$("#contenidoPago #email").prop("disabled", true);
			
			clientPrincipal.global.idBeforeShop = localStorage.getItem('idBeforeShop');
			clientPrincipal.global.initialURL = localStorage.getItem('initialURL');
			
			$("#subTotalCheckout").html(subTotal);
			$("#impuestosCheckout").html(tax);
			$("#descuentoCheckout").html(discount);
			$("#totalCheckout").html(total);
			
			clientPrincipal.carrito.createCheckoutButton();
			
			clientPrincipal.user.getUser("afterGetUserCheckout", clientPrincipal.checkout);
			
			 $('.basicAutoSelect').autoComplete({
				    resolver: 'custom',
				    events: {
				        search: function (qry, callback) {
				            $.ajax(
				                'http://localhost:8080/client/js/ciudadesColombia.json'
				            ).done(function (res) {
								
								var valorActual = $("#selectCode").val().toUpperCase();
								var dataConvert = [];
								
								$.each(res, function(index, data){
									
									if(data.text.indexOf(valorActual) > -1 ){
										dataConvert.push(data);
									}
									
								});
								
				                callback(dataConvert)
				            });
				        },
						
				    }
				});
				
			$('#contenidoPago #selectCode').on('autocomplete.select', function(event, item) {
				
				if(item){
					$('#contenidoPago #city').val(item.value);
				}else{
					$('#contenidoPago #city').val("");
				}
				
			});	
		},
		
		afterGetUserCheckout : function(result){
			
			if(result.success){
				
				clientPrincipal.user.afterGetUser(result);
				clientPrincipal.checkout.validateInputs();
				
			}
			
		},
		
		inputsCheckout: function() {

			$("#contenidoPago #identification").change(function() {
				localStorage.setItem('identification', this.value);
				clientPrincipal.checkout.validateInputs();
			});
			
			$("#contenidoPago #phone").change(function() {
				localStorage.setItem('phone', this.value);
				clientPrincipal.checkout.validateInputs();
			});
			
			$("#contenidoPago #city").change(function() {
				localStorage.setItem('city', this.value);
				clientPrincipal.checkout.validateInputs();
			});
			
			$("#contenidoPago #address").change(function() {
				localStorage.setItem('address', this.value);
				clientPrincipal.checkout.validateInputs();
			});
			
			$("#contenidoPago #futurePurchases").change(function() {
				localStorage.setItem('futurePurchases', $(this).is(":checked"));
				clientPrincipal.checkout.validateInputs();
			});
		},
		
		validateInputs : function(){
			
			var jsonValidateResponse = null;
			
			jsonValidateResponse = template.html.validateInputs("contenidoPago", false);
				
			if(jsonValidateResponse.valid){
				$("#divInfoCheckout").hide();
			}else{
				$("#divInfoCheckout").show();
			}
			
		}

	},
	user: {

		init: function() {
			clientPrincipal.user.buttonsUser();
		},
		
		createUser: function() {

			var contentId = "datosUsuario";
			var jsonRequest = template.html.getAllValues(contentId);
			
			var params = {
				url: clientPrincipal.global.urlApp+"users/userRegister",
				requestBody: jsonRequest,
				customLoad: "afterCreateUser",
				customResult: "globla.user"
			};
			
			template.ajax.post.get(params, clientPrincipal.user, null);
		},
		
		afterCreateUser: function(result) {

			if (result.success) {

				var urlAnterior = localStorage.getItem("urlAnterior");
				clientPrincipal.global.idUser = result.data.idUser;
				clientPrincipal.global.firstName = result.data.firstName;
				clientPrincipal.global.lastName = result.data.lastName;
				clientPrincipal.global.email = result.data.email;
				localStorage.setItem('idUser', clientPrincipal.global.idUser);
				localStorage.setItem('firstName', clientPrincipal.global.firstName);
				localStorage.setItem('lastName', clientPrincipal.global.lastName);
				localStorage.setItem('email', clientPrincipal.global.email);
				
				if (urlAnterior == null) {

					window.location.href = clientPrincipal.global.urlApp+"";
				} else {

					localStorage.removeItem("urlAnterior");
					window.location.href = urlAnterior;
				}

			} else if (!result.success) {
				$("#infoLogin").html(result.error);
			}

		},
		
		loginUser: function() {

			var contentId = "datosUsuario";
			var jsonRequest = template.html.getAllValues(contentId);
			var params = {
				url: clientPrincipal.global.urlApp+"users/userValidate",
				requestBody: jsonRequest,
				customLoad: "afterLoginUser",
				customResult: "globla.user"
			};
			template.ajax.post.get(params, clientPrincipal.user, null);
		},
		
		afterLoginUser: function(result) {

			if (result.success) {

				var urlAnterior = localStorage.getItem("urlAnterior");
				clientPrincipal.global.idUser = result.data.idUser;
				clientPrincipal.global.firstName = result.data.firstName;
				clientPrincipal.global.lastName = result.data.lastName;
				clientPrincipal.global.email = result.data.email;
				localStorage.setItem('idUser', clientPrincipal.global.idUser);
				localStorage.setItem('firstName', clientPrincipal.global.firstName);
				localStorage.setItem('lastName', clientPrincipal.global.lastName);
				localStorage.setItem('email', clientPrincipal.global.email);
				//pensar si el metodo de id de usuario para shop va aqui o despues

				if (urlAnterior == null) {

					window.location.href = clientPrincipal.global.urlApp+"";
				} else {

					localStorage.removeItem("urlAnterior");
					window.location.href = urlAnterior;
				}

			} else {
				var html = '';
				html = '<div class="alert alert-danger" role="alert">';
				html += '<small>Los datos suministrados de inicio de sesión son incorrectos</small>';
				html += '</div>';
				$(".btn-login-user").html('Iniciar Sesi&oacute;n');
				$("#divAlertStatusLogin").html(html);
			}

		},
		
		buttonsUser: function() {

			$(".btn-add-user").click(function() {

				var html = '<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Guardando Informacion';
				$(this).html(html);
				clientPrincipal.user.createUser();
			});
			$(".btn-login-user").click(function() {

				var html = '<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Iniciando sesi&oacute;n...';
				$(this).html(html);
				clientPrincipal.user.loginUser();
			});
		},
		
		getUser: function(customLoad, afterFunction) {

			var jsonRequest = {};
			
			jsonRequest.email = localStorage.getItem("email"); 

			var params = {
				url: clientPrincipal.global.urlApp+"users/getUser",
				requestBody: jsonRequest,
				customLoad: customLoad,
				customResult: "globla.result.userInformation"
			};
			
			template.ajax.post.get(params, afterFunction, null);
		},
		
		afterGetUser: function(result) {
			
			if (result.success) {
				
				if(result.data.hasOwnProperty('identification')){
					$("#identification").val(result.data.identification);
					$("#identification").prop("disabled", true)
				}
				
				if(result.data.hasOwnProperty('phone')){
					$("#phone").val(result.data.phone);
					$("#phone").prop("disabled", true)
				}
				
				$('#selectCode').autoComplete('set', { value: result.data.addresses[0].city, text: result.data.addresses[0].descriptionCity });
				
				$('#address').val(result.data.addresses[0].address);
				
			}

		}

	},
	shop: {

		global: {
			shopParams: null
		},
		init: function() {


			var urlLocation = location.href.toString().split("?");
			var parametros = urlLocation[1].split("&");
			var jsonData = {};
			
			$.each(parametros, function(index, parametro) {

				var obj = parametro.split("=");
				eval('jsonData.' + obj[0] + '="' + obj[1] + '"');
			});
			
			clientPrincipal.shop.global.shopParams = jsonData;
			
			if (clientPrincipal.shop.global.shopParams != {} &&
				clientPrincipal.shop.global.shopParams.status != undefined &&
				clientPrincipal.shop.global.shopParams.status === "approved") {

				clientPrincipal.shop.success.init();
			} else if (clientPrincipal.shop.global.shopParams != {} &&
				clientPrincipal.shop.global.shopParams.status != undefined &&
				clientPrincipal.shop.global.shopParams.status === "rejected") {

				clientPrincipal.shop.failure.init();
			}



		},
		success: {

			init: function() {
				clientPrincipal.shop.sendShop();
			}

		},
		failure: {

			init: function() {

			}

		},
		pending: {

			init: function() {

			}

		},
		sendShop: function() {

			var jsonRequest = {};
			var jsonAddress = {};
			var identification = null;
			var phone = null;
			var address = null;
			var futurePurchases = null;
			var city = null;
			var descriptionCity = null;
			var email = null;
			var idUser = null;

			clientPrincipal.global.idShop = localStorage.getItem('idShop');
			address = localStorage.getItem('address');
			futurePurchases = localStorage.getItem('futurePurchases');
			identification = localStorage.getItem('identification');
			phone = localStorage.getItem('phone');
			city = localStorage.getItem('city');
			descriptionCity = localStorage.getItem('descriptionCity');
			email = localStorage.getItem('email');
			idUser = localStorage.getItem('idUser');

			jsonRequest.idShop = clientPrincipal.global.idShop;
			
			jsonAddress.address = address;
			jsonAddress.futurePurchases = futurePurchases == null ? false : futurePurchases;
			jsonAddress.identification = identification;
			jsonAddress.phone = phone;
			jsonAddress.city = city;
			jsonAddress.descriptionCity = descriptionCity;
			jsonAddress.idUser = idUser;
			jsonAddress.emailUser = email;
			
			jsonRequest.address = jsonAddress;
			jsonRequest.email = email;
			jsonRequest.identification = identification;
			jsonRequest.phone = phone;
			jsonRequest.responseApi = JSON.stringify(clientPrincipal.shop.global.shopParams);

			var params = {
				url: clientPrincipal.global.urlApp+"shops/sendShop",
				requestBody: jsonRequest,
				customLoad: "afterSendShop",
				customResult: "globla.result"
			};
			
			template.ajax.post.get(params, clientPrincipal.shop, null);
		},
		
		afterSendShop: function(result) {

			if (result.success) {
				
				clientPrincipal.global.idShop = localStorage.getItem('idShop');
				
				clientPrincipal.shop.getShop();

				localStorage.removeItem('idShop');
				localStorage.removeItem('address');
				localStorage.removeItem('futurePurchases');
				localStorage.removeItem('identification');
				localStorage.removeItem('phone');
			}

		},
		
		saveShop: function() {

			var jsonRequest = {};
			var idUser = localStorage.getItem('idUser')

			clientPrincipal.global.idShop = localStorage.getItem('idShop');
			jsonRequest.idShop = clientPrincipal.global.idShop;
			jsonRequest.idUser = idUser;
			
			var params = {
				url: clientPrincipal.global.urlApp+"shops/saveShop",
				requestBody: jsonRequest,
				customLoad: "afterSaveShop",
				customResult: "globla.result"
			};
			
			template.ajax.post.get(params, clientPrincipal.shop, null);
		},
		
		afterSaveShop: function(result) {

			if (result.succes) {
				localStorage.setItem("shopStatus", result.data.status);
			}

		},
		
		getShop: function() {

			var jsonRequest = {};
			
			jsonRequest.idShop = clientPrincipal.global.idShop; 

			var params = {
				url: clientPrincipal.global.urlApp+"shops/getShop",
				requestBody: jsonRequest,
				customLoad: "afterGetShop",
				customResult: "globla.result"
			};
			
			template.ajax.post.get(params, clientPrincipal.shop, null);
		},
		
		afterGetShop: function(result) {
			
			if (result.succes) {
	
				result.data;
			}

		}

	}

}

return clientPrincipal;

});