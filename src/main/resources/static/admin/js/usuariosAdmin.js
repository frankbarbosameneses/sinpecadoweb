
{}

var principal = {
    
	global : {
		editResult : null
	},
    
    init: function () {
        
        
        principal.users.init();
        
    },

	users : {
		
		init : function(){
			
			
			principal.users.listarUsers();
			//principal.users.listeners.init();
		},
		
		openModalCreate : function(data){
			
			$("#createModal").modal('show');
			
		},
		
		getModalUser : function(jsonRequest){
			
			var params = {
							url : "http://localhost:8080/users/getAllUsers",
							requestBody : jsonRequest
				
						};
			
			
			template.ajax.post.get(params, principal, "modal-create-user-content");
			
			principal.users.openModalCreate();
			
		},
		
		createUser : function(jsonUser, contentId){
			
			var jsonRequest = {};
			var result		= null;
			
			jsonRequest.users = [];
			jsonRequest.users.push(jsonUser);
			
			var params = {
							url : "http://localhost:8080/users/userPublishing",
							requestBody : jsonRequest,
							contentId : contentId,
							resetValue : true
						};
			
			
			result = template.ajax.post.send(params, principal.users.listarUsers);
			
			$("#createModal").modal('hide');
			
		},
		
		listarUsers: function(filterParams) {
			
			if(filterParams == undefined || filterParams == null){
				filterParams = {};
			}else{
				filterParams = filterParams;
			}
        
			var params = {
							url : "http://localhost:8080/users/getAllUsers",
							filter : filterParams,
							div : "adminBody", 
				            paginator : {
											size : 10
										},
							editKey : "code",
							classEdit : "edit-user",
							afterrender: {
								listeners: {
									action : principal.users.listeners.init
								}
							},
				            columns : [
				                        {
				                            title : "id",
				                            column : "idUser"
				                        },
				                        {
				                            title : "Login",
				                            column : "login"
				                        },
				                        {
				                            title : "Nombres",
				                            column : "firstName"
				                        },
				                        {
				                            title : "Apellidos",
				                            column : "lastName"
				                        },
				                        {
				                            title : "Estado",
				                            column : "status"
				                        }
				
				                    ],
				             editar : ""
				            
				        };
	
			template.html.autoTable(params);
	        
	    },

		listeners : {
			
			init : function(){
				principal.users.listeners.buttons.init();
			},
			
			buttons : {
				
				init : function(){
					
					principal.users.listeners.buttons.btnCreateUser();
					principal.users.listeners.buttons.btnCreate();
					principal.users.listeners.buttons.btnFilter();
					principal.users.listeners.buttons.btnFilterUser();
					principal.users.listeners.buttons.btnEditUser();
				},
				
				btnFilter : function(){
					
					$("#btnFilter").click(function(){
						
						$("#filtroModal").modal('show');
						
					});
				},
				
				btnCreate : function(){
					
					$("#btnCreate").click(function(){
						
						$("#createModal").modal('show');
						
					});
				},
				
				btnFilterUser : function(){
					
					$("#btnFilterUser").click(function(){
						
						var parametros = template.html.getAllValues("modal-filter-user-content");
						
						principal.users.listarUsers(parametros);
						
						$("#filtroModal").modal('hide');
					});
					
				},
				
				btnCreateUser : function(){
					
					$("#btnCreateUser").click(function(){
						
						var contentId = "modal-create-user-content";
						var parametros = template.html.getAllValues(contentId);
						
						principal.users.createUser(parametros, contentId);
						
						
					});
					
				},
				
				btnEditUser : function(){
					
					$(".edit-user").click(function(){
						
						var parametros = {};
						
						parametros.code = this.lang; 
						
						principal.users.getModalUser(parametros);
						
						
					});
					
				}
				
			}
		}
		
		

	}

    

};
