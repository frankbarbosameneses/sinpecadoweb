package com.sinpecado.api.shop.api.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sinpecado.api.admin.users.data.AddressesData;
import com.sinpecado.api.admin.users.data.UserData;
import com.sinpecado.api.admin.users.services.UsersServices;
import com.sinpecado.api.shop.api.model.ShopBody;
import com.sinpecado.api.shop.data.ShopData;
import com.sinpecado.api.shop.items.api.model.ItemShopBody;
import com.sinpecado.api.shop.items.data.ItemsShopTemp;
import com.sinpecado.api.shop.items.data.ItemsShopTempPK;
import com.sinpecado.api.shop.items.services.ItemsShopServices;
import com.sinpecado.api.shop.services.ShopServices;
import com.sinpecado.api.sourcing.offers.data.OfferData;
import com.sinpecado.api.sourcing.offers.services.Offers;
import com.sinpecado.api.sourcing.offers.services.OffersServices;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("shops")
public class ShopController {
	
	@Autowired
	ItemsShopServices itemsShopServices;
	
	@Autowired
	ShopServices shopServices;
	
	@Autowired
	UsersServices usersServices;
	
	
	@PostMapping("/saveShop")
	public ResponseEntity<?> saveShop(@RequestBody ShopBody shopBody) throws Exception{
		
		ShopData shopData 	= null;
		JSONObject response = null;
		
		try {
			
			
			if(shopBody.getIdShop() != null) {
				shopData = this.shopServices.findShopById(shopBody);
			}
			
			if(shopData == null || shopData.getIdShop() == null) {
				
				shopData = new ShopData();
				
				shopData.setRegisterDate(new Timestamp(System.currentTimeMillis()));
				shopData.setStatus("INPROCESS");
			}
			
			if(shopBody.getIva() != null) {
				shopData.setIva(shopBody.getIva());
			}
			
			if(shopBody.getObservation() != null) {
				shopData.setObservation(shopBody.getObservation());		
			}
			
			if(shopBody.getTotal() != null) {
				shopData.setTotal(shopBody.getTotal());
			}
			
			if(shopBody.getSubTotal() != null) {
				shopData.setSubTotal(shopBody.getSubTotal());
			}
			
			if(shopBody.getIdUser() != null) {
				shopData.setIdUser(shopBody.getIdUser());
				shopData.setStatus("ASSOCIATED");
			}
			
			shopData.setUpdateDate(new Timestamp(System.currentTimeMillis()));
			
			shopData = this.shopServices.saveShop(shopData);
			
			response = new JSONObject();
			response.put("succes", true);
			response.put("data", shopData);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("succes", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/addCar")
	public ResponseEntity<?> saveItemTemp(@RequestBody ItemShopBody itemShopBody) throws Exception{
		
		JSONObject response 					= null;
		ItemsShopTemp itemsShopTemp				= null;
		ItemsShopTempPK itemsShopTempPK			= null;
		ItemsShopTempPK idItemShopTemp			= null;
		
		try {
			
			idItemShopTemp = new ItemsShopTempPK();
			idItemShopTemp.setIdItem(itemShopBody.getIdItem());
			idItemShopTemp.setIdShop(itemShopBody.getIdShop());
			
			itemsShopTemp = this.itemsShopServices.findItemsShopTempById(idItemShopTemp);
			
			if(itemsShopTemp.getIdItemShopTemp() == null || itemsShopTemp.getIdItemShopTemp().getIdShop() == null) {
			
				ShopData shopData = new ShopData();
				
				if(itemShopBody.getIdShop() == null) {
					shopData.setStatus("REGISTERED");
					shopData = shopServices.saveShop(shopData);
				}else {
					shopData.setIdShop(itemShopBody.getIdShop());
				}
				
				itemsShopTemp = new ItemsShopTemp();
				
				itemsShopTempPK = new ItemsShopTempPK();
				itemsShopTempPK.setIdItem(itemShopBody.getIdItem());
				itemsShopTempPK.setIdShop(shopData.getIdShop());
				
				itemsShopTemp.setIdItemShopTemp(itemsShopTempPK);
				
			}
			
			if(itemShopBody.getIsCarrito() != null && itemShopBody.getIsCarrito()) {
				itemsShopTemp.setAmount(itemShopBody.getAmount());
			}else {
				if(itemsShopTemp.getAmount() == null) {
					itemsShopTemp.setAmount(itemShopBody.getAmount());
				}else {
					itemsShopTemp.setAmount(itemsShopTemp.getAmount() + itemShopBody.getAmount());
				}
				
			}
			
			itemsShopTemp = this.itemsShopServices.saveItemShopTemp(itemsShopTemp);
			
			response = new JSONObject();
			response.put("succes", true);
			response.put("data", itemsShopTemp);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("succes", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins ={"*"})
	@PostMapping("/listCar")
	public ResponseEntity<?> listItemTemp(@RequestBody ItemShopBody itemShopBody) throws Exception{
		
		JSONObject response 					= null;
		List<ItemsShopTemp> listItemsShopTemp 	= null;
		
		try {
		
			listItemsShopTemp = this.itemsShopServices.listItemsShopTemp(itemShopBody);
			
			response = new JSONObject();
			response.put("succes", true);
			response.put("data", listItemsShopTemp);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("succes", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/deleteCar")
	public ResponseEntity<?> deleteItemTemp(@RequestBody ItemShopBody itemShopBody) throws Exception{
		
		ItemsShopTemp itemsShopTemp 	= null;
		JSONObject response 			= null;
		ItemsShopTempPK idItemShopTemp 	= null;
		
		try {
			
			idItemShopTemp = new ItemsShopTempPK();
			idItemShopTemp.setIdItem(itemShopBody.getIdItem());
			idItemShopTemp.setIdShop(itemShopBody.getIdShop());
			
			itemsShopTemp = this.itemsShopServices.findItemsShopTempById(idItemShopTemp);
			
			this.itemsShopServices.deleteItemShopTemp(itemsShopTemp);
		
			response = new JSONObject();
			response.put("succes", true);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("succes", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/sendBeforeShop")
	public ResponseEntity<?> sendBeforeShop(@RequestBody ShopBody shopBody) throws Exception{
		
		JSONObject response 					= null;
		List<ItemsShopTemp> listItemsShopTemp 	= null;
		
		try {
			
			listItemsShopTemp = this.itemsShopServices.findByIdShopTemp(shopBody.getIdShop());
			
			shopBody = this.shopServices.recalculateShop(shopBody, listItemsShopTemp);
			
			shopBody = this.shopServices.sendBeforeShop(shopBody, listItemsShopTemp);
			
			response = new JSONObject();
			response.put("succes", true);
			response.put("data", shopBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("succes", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/sendShop")
	public ResponseEntity<?> sendShop(@RequestBody ShopBody shopBody) throws Exception{
		
		UserData userData						= null;
		ShopData shopData 						= null;
		JSONObject response 					= null;
		List<ItemsShopTemp> listItemsShopTemp 	= null;
		
		try {
			
			response = new JSONObject();
			
			if(shopBody.getIdShop() != null) {
				
				shopData = this.shopServices.findShopById(shopBody);
			
				shopData.setUpdateDate(new Timestamp(System.currentTimeMillis()));
				shopData.setDateOfPurchase(new Timestamp(System.currentTimeMillis()));
				
				listItemsShopTemp = this.itemsShopServices.findByIdShopTemp(shopBody.getIdShop());
				
				shopBody = this.shopServices.recalculateShop(shopBody, listItemsShopTemp);
				
				shopData.setDiscount(shopBody.getDiscount());
				shopData.setIva(shopBody.getTax());
				shopData.setSubTotal(shopBody.getSubTotal());
				shopData.setTotal(shopBody.getTotal());
				
				for(ItemsShopTemp itemShopTemp : listItemsShopTemp) {
					this.itemsShopServices.saveItemShopTemp(itemShopTemp);
				}
				
				if(shopBody.getAddress().getIdAddresses() == null) {
					
					if(shopBody.getAddress().getFuturePurchases()) {
						
						AddressesData addressesData = this.usersServices.addressRegister(shopBody.getAddress());
						shopData.setIdAddresses(addressesData.getIdAddresses());
						
					}else {
						shopData.setDescriptionAddress(shopBody.getAddress().getCity()+", "+shopBody.getAddress().getAddress());
					}
					
				}else {
					shopData.setIdAddresses(shopBody.getAddress().getIdAddresses());
				}
				
				if(shopBody.getEmail() == null) {
					throw new Exception("Error al procesar venta por usuario.");
				}else {
					userData = this.usersServices.findByEmail(shopBody.getEmail());
				}
				
				if(shopBody.getPhone() != null) {
					
					userData.setPhone(shopBody.getPhone());
				}
				
				if(shopBody.getResponseApi() != null) {
					shopData.setResponseApi(shopBody.getResponseApi());
				}
				
				if( userData.getIdentification() == null && shopBody.getIdentification() != null) {
					
					userData.setIdentification(shopBody.getIdentification());
				}
				
				userData = this.usersServices.saveUser(userData);
				
				shopData = this.shopServices.saveShop(shopData);
				
				response.put("success", true);
				response.put("data", shopData);
			
			}else {
				response.put("success", false);
				response.put("error", "Error al procesar venta.");
			}
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/getShop")
	public ResponseEntity<?> getShop(@RequestBody ShopBody shopBody) throws Exception{
		
		Gson gson = new Gson();
		JSONObject response 					= null;
		List<ItemsShopTemp> listItemsShopTemp 	= null;
		ShopData shopData						= null;
		
		try {
			
			listItemsShopTemp = this.itemsShopServices.findByIdShopTemp(shopBody.getIdShop());
			
			shopData = this.shopServices.findShopById(shopBody);
			
			shopBody = gson.fromJson(gson.toJson(shopData), ShopBody.class);
			shopBody.setItems(listItemsShopTemp);
			
			response = new JSONObject();
			response.put("succes", true);
			response.put("data", shopBody);
			
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("succes", false);
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
		
	}

}
