package com.sinpecado.api.shop.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import com.sinpecado.api.admin.users.api.model.AddressBody;
import com.sinpecado.api.shop.items.data.ItemsShopTemp;

import lombok.Data;

@Data
public class ShopBody {

	private Integer idShop;
	
	private String observation;
	
	private Float total;
	
	private Float subTotal;
	
	private Float iva;
	
	private Float discount;
	
	private Float tax;
	
	private String status;
	
	private String registerDate;
	
	private String updateDate;
	
	private Integer idUser;
	
	private String idPreMercadoLibre;
	
	private String initialURL;
	
	private String city;
	
	private String identification;
	
	private String phone;
	
	private String email;
	
	private AddressBody address;
	
	private String descriptionAddress;
    
    private Integer idAddresses;
    
    private String responseApi;
    
    private List<ItemsShopTemp> items;
	
}
