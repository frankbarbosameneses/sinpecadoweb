/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.shop.items.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@Table(name = "items_shop")
public class ItemsShop implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ItemsShopPK idItemShop;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fdcAmount")
    private Integer amount;
    
    @Column(name = "fdcUnitPrice")
    private Float unitPrice;
    
    @Column(name = "fdcTotalPrice")
    private Float totalPrice;
    
    @Column(name = "fdcDiscount")
    private Float discount;
    
}
