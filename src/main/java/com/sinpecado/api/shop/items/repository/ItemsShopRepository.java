package com.sinpecado.api.shop.items.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sinpecado.api.shop.items.data.ItemsShop;
import com.sinpecado.api.shop.items.data.ItemsShopPK;

@Repository
public interface ItemsShopRepository extends CrudRepository<ItemsShop, ItemsShopPK>{

}
