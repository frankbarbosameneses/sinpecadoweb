package com.sinpecado.api.shop.items.services;

import java.util.List;

import com.sinpecado.api.shop.items.api.model.ItemShopBody;
import com.sinpecado.api.shop.items.data.ItemsShop;
import com.sinpecado.api.shop.items.data.ItemsShopTemp;
import com.sinpecado.api.shop.items.data.ItemsShopTempPK;

public interface ItemsShopServices {
	
	ItemsShopTemp saveItemShopTemp(ItemsShopTemp itemShopTemp) throws Exception;
	
	ItemsShop saveItemShop(ItemsShop itemShop) throws Exception;
	
	void deleteItemShopTemp(ItemsShopTemp itemShopTemp) throws Exception;
	
	List<ItemsShopTemp> listItemsShopTemp(ItemShopBody itemShopTemp) throws Exception;
	
	List<ItemsShop> listItemsShop(ItemShopBody itemShopTemp) throws Exception;
	
	List<ItemsShopTemp> findByIdShopTemp(Integer idShop) throws Exception;

	ItemsShopTemp findItemsShopTempById(ItemsShopTempPK idItemShopTemp) throws Exception;

}
