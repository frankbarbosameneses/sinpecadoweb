package com.sinpecado.api.shop.items.services;

import java.io.File;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sinpecado.api.shop.items.api.model.ItemShopBody;
import com.sinpecado.api.shop.items.data.ItemsShop;
import com.sinpecado.api.shop.items.data.ItemsShopTemp;
import com.sinpecado.api.shop.items.data.ItemsShopTempPK;
import com.sinpecado.api.shop.items.repository.ItemsShopRepository;
import com.sinpecado.api.shop.items.repository.ItemsShopTempRepository;
import com.sinpecado.api.sourcing.items.data.ItemData;
import com.sinpecado.api.sourcing.items.data.ItemPhotoData;
import com.sinpecado.api.sourcing.items.repository.ItemPhotoRepository;
import com.sinpecado.api.sourcing.items.repository.ItemsRepository;
import com.sinpecado.api.sourcing.offers.data.OfferData;
import com.sinpecado.api.sourcing.offers.repository.OffersRepository;

@Service
public class ItemShop implements ItemsShopServices {
	
	@Autowired
	ItemsShopRepository itemsShopRepository;
	
	@Autowired
	ItemsShopTempRepository itemsShopTempRepository;
	
	@Autowired
	ItemsRepository itemsRepository;
	
	@Autowired
	ItemPhotoRepository itemPhotoRepository;
	
	@Autowired
	OffersRepository offersRepository;
	
	@Override
	public ItemsShopTemp saveItemShopTemp(ItemsShopTemp itemsShopTemp) throws Exception {

		try {
			
			return this.itemsShopTempRepository.save(itemsShopTemp);
			
		} catch (Exception e) {
			throw e;
		}
		
		
	}

	@Override
	public ItemsShop saveItemShop(ItemsShop itemShop) throws Exception {
		
		try {
			
			return this.itemsShopRepository.save(itemShop);
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ItemsShopTemp> listItemsShopTemp(ItemShopBody itemShopBody) throws Exception {
		
		List<ItemsShopTemp> listItemsShopTemp 	= null;
		
		try {
			
			listItemsShopTemp = this.findByIdShopTemp(itemShopBody.getIdShop());
			
			listItemsShopTemp.forEach(a-> {
				
				Integer pagePhoto				= 0;
				Integer sizePhoto				= 1;
				Sort sortPhoto 					= Sort.by("idItemPhotoData.idPhoto").ascending();
				
				Pageable pageablePhoto = PageRequest.of(pagePhoto, sizePhoto, sortPhoto);
				
				List<ItemPhotoData> listItemPhoto = itemPhotoRepository.findAllItemPhoto("%", "%", a.getIdItemShopTemp().getIdItem(), pageablePhoto);
				
				if(listItemPhoto != null && listItemPhoto.size() > 0) {
					a.setUrl(listItemPhoto.get(0).getUrl()+ File.separator +listItemPhoto.get(0).getName());
				}
				
			});
			
			return listItemsShopTemp;
			
		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	public List<ItemsShop> listItemsShop(ItemShopBody itemsShopTemp) throws Exception {
		
		try {
			
			return null;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void deleteItemShopTemp(ItemsShopTemp itemShopTemp) throws Exception {
		
		try {
			
			this.itemsShopTempRepository.delete(itemShopTemp);
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ItemsShopTemp findItemsShopTempById(ItemsShopTempPK idItemShopTemp) throws Exception {
		
		ItemsShopTemp itemsShopTemp				= null;
		Optional<ItemsShopTemp> opItemsShopTemp = null;
		
		try {
			
			opItemsShopTemp = this.itemsShopTempRepository.findById(idItemShopTemp);
			
			if(opItemsShopTemp.isPresent()) {
				itemsShopTemp = opItemsShopTemp.get();
			}else {
				itemsShopTemp = new ItemsShopTemp();
			}
			
			return itemsShopTemp;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ItemsShopTemp> findByIdShopTemp(Integer idShop) throws Exception {
		
		List<ItemsShopTemp> listItemsShopTemp 	= null;
		
		try {
			
			listItemsShopTemp = this.itemsShopTempRepository.findByIdShopTemp(idShop);
			
			listItemsShopTemp.forEach(itemTemp ->{
				
				Optional<ItemData> opItemData 	= itemsRepository.findById(itemTemp.getIdItemShopTemp().getIdItem());
				
				if(opItemData.isPresent()) {
					
					ItemData itemData = null;
					
					itemData = opItemData.get();
					
					itemTemp.setIva(itemData.getIva());
					itemTemp.setUnitPrice(itemData.getPrice());
					
					Optional<OfferData> opOfferData = offersRepository.findActiveOfertaByidItem(itemTemp.getIdItemShopTemp().getIdItem());
					
					if(opOfferData.isPresent()) {
						
						OfferData offerData = opOfferData.get();
						
						itemTemp.setDiscount(offerData.getDiscount());
						itemTemp.setDiscountValue(((itemData.getPrice() * itemTemp.getAmount()) * (itemTemp.getDiscount() / 100)));
						
					}else {
						itemTemp.setDiscount(0.0f);
						itemTemp.setDiscountValue(0.0f);
					}
					
					if(itemTemp.getDiscount() == null) {
						itemTemp.setTotalPrice(((itemData.getPrice() * itemTemp.getAmount()) + ((itemData.getPrice() * itemTemp.getAmount()) * (itemData.getIva() / 100))));
						itemTemp.setTotalUnitePrice((itemData.getPrice()  + (itemData.getPrice()  * (itemData.getIva() / 100))));
						itemTemp.setIvaPrice((itemData.getPrice() * (itemData.getIva() / 100)));
					}else {
						itemTemp.setTotalPrice(((itemData.getPrice() * itemTemp.getAmount()) + ((itemData.getPrice() * itemTemp.getAmount()) * (itemData.getIva() / 100)) - ((itemData.getPrice() * itemTemp.getAmount()) * (itemTemp.getDiscount() / 100))));
						itemTemp.setTotalUnitePrice((itemData.getPrice() + (itemData.getPrice() * (itemData.getIva() / 100)) - (itemData.getPrice() * (itemTemp.getDiscount() / 100))));
						itemTemp.setIvaPrice(((itemData.getPrice() * itemTemp.getAmount()) * (itemData.getIva() / 100)));
						itemTemp.setDiscount(0.0f);
						itemTemp.setDiscountValue(0.0f);
					}
					
					itemTemp.setDescription(itemData.getDescription());
				}
				
			});
			
			return listItemsShopTemp;
			
		}catch (Exception e) {
			throw e;
		}
	}

}
