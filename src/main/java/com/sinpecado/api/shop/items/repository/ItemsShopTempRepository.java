package com.sinpecado.api.shop.items.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.sinpecado.api.shop.items.data.ItemsShopTemp;
import com.sinpecado.api.shop.items.data.ItemsShopTempPK;

public interface ItemsShopTempRepository extends CrudRepository<ItemsShopTemp, ItemsShopTempPK>{
	
	List<ItemsShopTemp> findByIdItemShopTemp(ItemsShopTempPK idItemShopTemp);
	
	@Query(value = "select ist "
			+ "from ItemsShopTemp ist where ist.idItemShopTemp.idShop = :idShop ")
	List<ItemsShopTemp> findByIdShopTemp(@Param("idShop") Integer idShop);

}
