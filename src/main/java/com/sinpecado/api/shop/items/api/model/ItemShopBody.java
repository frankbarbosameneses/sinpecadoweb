package com.sinpecado.api.shop.items.api.model;

import lombok.Data;

@Data
public class ItemShopBody {
	
	private Integer idItem;
    
    private Integer idShop;
    
	private Integer amount;
    
    private Float unitPrice;
    
    private Float totalPrice;
    
    private Float discount;
    
    private Boolean isCarrito;

}
