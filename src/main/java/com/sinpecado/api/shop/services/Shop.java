package com.sinpecado.api.shop.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadopago.MercadoPago;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.advancedpayment.Payment;
import com.mercadopago.resources.datastructures.preference.Address;
import com.mercadopago.resources.datastructures.preference.BackUrls;
import com.mercadopago.resources.datastructures.preference.Identification;
import com.mercadopago.resources.datastructures.preference.Item;
import com.mercadopago.resources.datastructures.preference.Payer;
import com.mercadopago.resources.datastructures.preference.PaymentMethods;
import com.mercadopago.resources.datastructures.preference.Phone;
import com.sinpecado.api.shop.api.model.ShopBody;
import com.sinpecado.api.shop.data.ShopData;
import com.sinpecado.api.shop.items.data.ItemsShopTemp;
import com.sinpecado.api.shop.repository.ShopRepository;

import net.minidev.json.JSONArray;

@Service
public class Shop implements ShopServices {
	
	@Autowired
	ShopRepository shopRepository;

	@Override
	public ShopData saveShop(ShopData shopData) throws Exception {
		
		try {
			
			if(shopData.getIdShop() == null) {
				
				shopData.setStatus("INPROCESS");
				shopData.setRegisterDate(new Timestamp(System.currentTimeMillis()));
			}
			
			shopData.setUpdateDate(new Timestamp(System.currentTimeMillis()));
			
			shopData = this.shopRepository.save(shopData);
			
			return shopData;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ShopData findShopById(ShopBody shopBody) throws Exception {
		
		ShopData shopData 				= null;
		Optional<ShopData> opShopData 	= null;
		
		try {
			
			opShopData = this.shopRepository.findById(shopBody.getIdShop());
			
			if(opShopData.isPresent()) {
				shopData = opShopData.get();
			}else {
				shopData = new ShopData();
			}
			
			return shopData;
			
		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	public ShopBody sendBeforeShop(ShopBody shopBody, List<ItemsShopTemp> listItemShopTemp) throws Exception {
		
		try {
			
			MercadoPago.SDK.setAccessToken("TEST-4826109964622026-041804-f5758b1be47c75b443b512023f809800-745721473");
			
			// Crea un objeto de preferencia
			Preference preference = new Preference();

			for(ItemsShopTemp itemsShopTemp: listItemShopTemp) {
				
				Item item = new Item();
				
				item.setTitle(itemsShopTemp.getDescription())
				    .setQuantity(itemsShopTemp.getAmount())
				    .setUnitPrice(itemsShopTemp.getTotalUnitePrice());
				
				preference.appendItem(item);
				
			}
			
			if(shopBody.getCity() != null) {
				
				Item itemDomicilio = new Item();
				
				itemDomicilio.setTitle("Domicilio")
				.setDescription("Cobro por domicilio del producto a tu ciudad de residencia.")
			    .setQuantity(1);
				
				if(shopBody.getCity().equals("76001")) {
					itemDomicilio.setUnitPrice((float)5000);
				}else {
					itemDomicilio.setUnitPrice((float)10000);
				}
				
				preference.appendItem(itemDomicilio);
			}
			
			
			BackUrls urls = new BackUrls();
			
			urls.setSuccess("http://localhost:8080/ventaExitosa");
			urls.setFailure("http://localhost:8080/ventaFallida");
			urls.setPending("http://localhost:8080/ventaPendiente");
			
			preference.setBackUrls(urls);
			
			preference.setMarketplace("AAATUM");
			
			preference = preference.save();
			
			shopBody.setIdPreMercadoLibre(preference.getId());
			shopBody.setInitialURL(preference.getInitPoint());
			
			return shopBody;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public JSONArray getPayMethods() throws Exception {
		
		JSONArray jsonResult = null;
		
		try {
			
			MercadoPago.SDK.configure("ENV_ACCESS_TOKEN");

			MercadoPago.SDK.Get("");
			
			return jsonResult;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ShopBody recalculateShop(ShopBody shopBody, List<ItemsShopTemp> listItemShopTemp) throws Exception {
		
		Float subTotalPrice 		= null;
		Float totalPrice			= null;
		Float totalIvaPrice			= null;
		Float totalDiscountValue	= null;
		
		try {
			
			subTotalPrice = (float) listItemShopTemp.stream().filter(itemTemp-> itemTemp.getUnitPrice() != null).mapToDouble(itemTemp-> itemTemp.getUnitPrice()).sum();
			totalPrice = (float) listItemShopTemp.stream().filter(itemTemp-> itemTemp.getTotalPrice() != null).mapToDouble(itemTemp-> itemTemp.getTotalPrice()).sum();
			totalIvaPrice = (float) listItemShopTemp.stream().filter(itemTemp-> itemTemp.getIvaPrice() != null).mapToDouble(itemTemp-> itemTemp.getIvaPrice()).sum();
			totalDiscountValue =(float) listItemShopTemp.stream().filter(itemTemp-> itemTemp.getDiscountValue() != null).mapToDouble(itemTemp-> itemTemp.getDiscountValue()).sum();
			
			shopBody.setSubTotal(subTotalPrice);
			shopBody.setTotal(totalPrice);
			shopBody.setDiscount(totalDiscountValue);
			shopBody.setTax(totalIvaPrice);
			
			return shopBody;
			
		} catch (Exception e) {
			throw e;
		}
	}

}
