package com.sinpecado.api.shop.services;

import java.util.List;

import com.sinpecado.api.shop.api.model.ShopBody;
import com.sinpecado.api.shop.data.ShopData;
import com.sinpecado.api.shop.items.data.ItemsShopTemp;

import net.minidev.json.JSONArray;

public interface ShopServices {

	ShopData saveShop(ShopData shopData) throws Exception;
	
	ShopData findShopById(ShopBody shopBody) throws Exception;
	
	ShopBody sendBeforeShop(ShopBody shopBody, List<ItemsShopTemp> listItemShopTemp) throws Exception;
	
	ShopBody recalculateShop(ShopBody shopBody, List<ItemsShopTemp> listItemShopTemp) throws Exception;
	
	JSONArray getPayMethods() throws Exception;
}
