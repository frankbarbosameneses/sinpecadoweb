/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.shop.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@Table(name = "shop")
public class ShopData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "finIdShop")
    private Integer idShop;
    
    @Column(name = "fvcObservation")
    private String observation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @Column(name = "fdcDiscount")
    private Float discount;
	
    @Column(name = "fdcTax")
	private Float tax;
    
    @Column(name = "fdcTotal")
    private Float total;
    
    @Column(name = "fdcSubTotal")
    private Float subTotal;
    
    @Column(name = "fdcIVA")
    private Float iva;
    
    @Column(name = "fvcStatus")
    private String status;
    
    @Column(name = "fdtRegisterDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerDate;
    
    @Column(name = "fdtUpdateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "fdtDateOfPurchase")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfPurchase;
    
    @Column(name = "finIdUser")
    private Integer idUser;
    
    @Column(name = "fvcDescriptionAddress")
    private String descriptionAddress;
    
    @Column(name = "finIdAddresses")
    private Integer idAddresses;
    
    @Column(name = "fvcResponseAPI")
    private String responseApi;
    
    
}
