package com.sinpecado.api.shop.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.sinpecado.api.shop.data.ShopData;

public interface ShopRepository extends CrudRepository<ShopData, Integer>{
	


}
