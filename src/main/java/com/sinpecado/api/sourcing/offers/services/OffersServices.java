package com.sinpecado.api.sourcing.offers.services;

import java.util.List;
import java.util.Optional;

import com.sinpecado.api.sourcing.offers.api.model.OfferBody;
import com.sinpecado.api.sourcing.offers.data.OfferData;

import net.minidev.json.JSONObject;

public interface OffersServices {

	JSONObject offerRegister(OfferBody offerBody) throws Exception;
	
	JSONObject findAllOferLimited(OfferBody offerBody) throws Exception;
	
	OfferData saveOffer(OfferData offer) throws Exception;
	
	JSONObject findAllOferItemsLimited(OfferBody offerBody) throws Exception;
	
	Optional<OfferData> findActiveOfertaByidItem(Integer idItem) throws Exception;
	
}
