/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.sourcing.offers.data;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@Table(name = "offer")
public class OfferData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected OfferDataPK idOfferPK;
    
    @Column(name = "fvcDescription")
    private String description;
    
    @Column(name = "fvcObservation")
    private String observation;
    
    @Column(name = "fdtInitialDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date initialDate;
    
    @Column(name = "fdtFinalDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalDate;
    
    @Column(name = "fvcStatus")
    private String status;
    
    @Column(name = "fdtRegisterDate")
    private String registerDate;
    
    @Column(name = "fdcDiscount")
    private Float discount;
    
    @Transient
    private Integer idOffer;
    
    @Transient
    private Integer idItem;
    
    @Transient
    private String itemDescription;
    
    @Transient
    private String itemCode;
    
    @Transient
    private String url;
    
    @Transient
    private Float price;
    
    @Transient
    private Float iva;
    
    @Transient
    private Float ivaPrice;
    
    @Transient
    private Float finalPrice;
    
    public OfferData() {
    	
    }
    
    public OfferData(String itemCode, String itemDescription, String url, Float price, Float iva, Float discount, String namePhoto, Integer idItem ) {
    	
    	this.idItem = idItem;
    	this.itemCode = itemCode;
    	this.itemDescription = itemDescription;
    	this.url = url + File.separator + namePhoto;
    	this.discount = discount;
    	this.price = price;
    	this.iva = iva;
    	
    	this.ivaPrice = (price + (price * (iva / 100)));
    	
    	this.finalPrice = (price + (price * (iva / 100))) - (price * (discount / 100));
    }
    
}
