package com.sinpecado.api.sourcing.offers.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sinpecado.api.sourcing.items.data.ItemData;
import com.sinpecado.api.sourcing.items.services.ItemsServices;
import com.sinpecado.api.sourcing.offers.api.model.OfferBody;
import com.sinpecado.api.sourcing.offers.data.OfferData;
import com.sinpecado.api.sourcing.offers.data.OfferDataPK;
import com.sinpecado.api.sourcing.offers.repository.OffersRepository;

import net.minidev.json.JSONObject;

@Service
public class Offers implements OffersServices {
	
	@Autowired
	ItemsServices itemsSevices;
	
	@Autowired
	OffersRepository offersRepository;

	@Override
	public JSONObject offerRegister(OfferBody offerBody) throws Exception {
		
		Gson gson						= new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm").create();
				//.setDateFormat("yyyy-MM-dd  HH:mm:ss").create();
		JSONObject result 				= null;
		ItemData item					= null;
		OfferData offer					= null;
		OfferDataPK offerPk				= null;
		
		try {
			
			if(offerBody.getIdOffer() == null) {
				
				item = itemsSevices.findItemByCode(offerBody.getCode());
				
				if(item == null || item.getIdItem() == null) {
					throw new Exception("El item no existe.");
				}else {
					
					offer = gson.fromJson(gson.toJson(offerBody), OfferData.class) ;
					
					offerPk = new OfferDataPK();
					offerPk.setIdItem(item.getIdItem());
					
					offer.setIdOfferPK(offerPk);
					
				}
				
			}else {
				
				offer = offersRepository.findOfertaByIdOffer(offerBody.getIdOffer());
				
				if(offer != null && offer.getIdOfferPK().getIdOffer() != null) {
					offer.setDescription(offerBody.getDescription());
					offer.setStatus(offerBody.getStatus());
					offer.setObservation(offerBody.getObservation());
				}
				
			}
			
			offer = this.saveOffer(offer);
			
			result = new JSONObject();
			result.put("success", true);
			result.put("offer", offer);
			
		} catch (Exception e) {
			result = new JSONObject();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		
		return result;
	}

	@Override
	public OfferData saveOffer(OfferData offer) throws Exception {
		
		try {
			return offersRepository.save(offer);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public JSONObject findAllOferLimited(OfferBody offerBody) throws Exception {
		JSONObject result 			= null;
		Page<OfferData> listOfertas = null;
		Integer page				= null;
		Integer size				= null;
		
		try {
			
			page = offerBody.getPager() != null && offerBody.getPager().getPage() != null ? offerBody.getPager().getPage() : 0;
			size = offerBody.getPager() != null && offerBody.getPager().getSize() != null ? offerBody.getPager().getSize() : 10;
			
			Pageable pageable = PageRequest.of(page, size);
			
			if(offerBody.getIdOffer() == null ) {
				listOfertas = offersRepository.findAllOfertas( 	offerBody.getDescription() != null ?  "%"+offerBody.getDescription()+ "%" :"%", 
						offerBody.getStatus() != null ?  "%"+offerBody.getStatus()+ "%" : "%", 
					    pageable);
			}else {
				listOfertas = offersRepository.findAllOfertasByIdOffer(offerBody.getIdOffer(), pageable);
			}
			
			listOfertas.forEach(a-> {
				
				a.setIdOffer(a.getIdOfferPK().getIdOffer());
				a.setIdItem(a.getIdOfferPK().getIdItem());
				
				try {
					
					ItemData itemData = itemsSevices.findItemById(a.getIdOfferPK().getIdItem());
					
					a.setItemDescription(itemData.getDescription());
					a.setItemCode(itemData.getCode());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			
			result = new JSONObject();
			result.put("success", true);
			result.put("data", listOfertas);
			
			return result;
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public JSONObject findAllOferItemsLimited(OfferBody offerBody) throws Exception {
		JSONObject result 			= null;
		Page<OfferData> listOfertas = null;
		Integer page				= null;
		Integer size				= null;
		
		try {
			
			page = offerBody.getPager() != null && offerBody.getPager().getPage() != null ? offerBody.getPager().getPage() : 0;
			size = offerBody.getPager() != null && offerBody.getPager().getSize() != null ? offerBody.getPager().getSize() : 10;
			
			Pageable pageable = PageRequest.of(page, size);
			
			listOfertas = offersRepository.findAllOfertasItems(pageable);
			
			result = new JSONObject();
			result.put("success", true);
			result.put("data", listOfertas);
			
			return result;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Optional<OfferData> findActiveOfertaByidItem(Integer idItem) throws Exception {
		
		
		Optional<OfferData> opOfferData = null;
		
		try {
			
			opOfferData = offersRepository.findActiveOfertaByidItem(idItem);
			
			return opOfferData;
			
		} catch (Exception e) {
			throw e;
		}
	}

}
