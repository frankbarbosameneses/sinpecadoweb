/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.sourcing.offers.data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class OfferDataPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "finIdOffer")
    private Integer idOffer;
    
    @Basic(optional = false)
    @Column(name = "finIdItem")
    private Integer idItem;
    
}
