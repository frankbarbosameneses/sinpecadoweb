package com.sinpecado.api.sourcing.offers.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sinpecado.api.sourcing.items.api.model.ItemBody;
import com.sinpecado.api.sourcing.offers.api.model.OfferBody;
import com.sinpecado.api.sourcing.offers.services.OffersServices;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("offers")
public class OffersController {
	
	@Autowired
	OffersServices offersServices;

	@PostMapping("/offerRegister")
	public ResponseEntity<?> offerRegister(@RequestBody OfferBody offerBody) throws Exception{
		
		JSONObject response = null;
		
		try {
			
			response = offersServices.offerRegister(offerBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", true);
			response.put("error", e.getCause());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins ={"*"})
	@PostMapping("/getAllOfertas")
	public ResponseEntity<?> getAllOfertas(@RequestBody OfferBody offerBody) throws Exception{
		
		JSONObject response = null;
		
		try {
			
			response = offersServices.findAllOferLimited(offerBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins ={"*"})
	@GetMapping("/getAllOffers")
	public ResponseEntity<?> getAllOffers(@RequestParam(value = "description", defaultValue = "") String description) throws Exception{
		
		OfferBody offerBody	= null;
		JSONObject response = null;
		
		try {
			
			offerBody = new OfferBody();
			offerBody.setDescription(description);
			//
			response = offersServices.findAllOferLimited(offerBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins ={"*"})
	@PostMapping("/getAllOfertasItems")
	public ResponseEntity<?> getAllOfertasItems(@RequestBody OfferBody offerBody) throws Exception{
		
		JSONObject response = null;
		
		try {
			
			response = offersServices.findAllOferItemsLimited(offerBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
}
