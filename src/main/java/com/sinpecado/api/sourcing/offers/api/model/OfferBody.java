package com.sinpecado.api.sourcing.offers.api.model;

import com.sinpecado.api.general.resources.model.PagerBody;

import lombok.Data;

@Data
public class OfferBody {
	
	private String description;
	
	private String observation;
	
	private String initialDate;
	
	private String finalDate;
	
	private String status;
	
	private Integer idItem;
	
	private Integer idOffer;
	
	private String code;
	
	private Double discount;
	
	private PagerBody pager;

}
