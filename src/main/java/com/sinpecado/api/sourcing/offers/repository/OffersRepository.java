package com.sinpecado.api.sourcing.offers.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sinpecado.api.sourcing.offers.data.OfferData;
import com.sinpecado.api.sourcing.offers.data.OfferDataPK;

@Repository
public interface OffersRepository extends CrudRepository<OfferData , OfferDataPK>{
	
	@Query(value = "select offer from OfferData offer where offer.description like :description and offer.status like :status")
	Page<OfferData> findAllOfertas(@Param("description") String description, @Param("status") String status, Pageable pageable);
	
	@Query(value = "select offer from OfferData offer where offer.idOfferPK.idOffer = :idOffer")
	Page<OfferData> findAllOfertasByIdOffer(@Param("idOffer") Integer idOffer, Pageable pageable);
	
	@Query(value = "select offer from OfferData offer where offer.idOfferPK.idOffer = :idOffer")
	OfferData findOfertaByIdOffer(@Param("idOffer") Integer idOffer);
	
	@Query(value = "select new com.sinpecado.api.sourcing.offers.data.OfferData(it.code, it.description, ph.url, it.price, it.iva, offer.discount, ph.name, it.idItem) "
			+ "from OfferData offer "
			+ "inner join ItemData it on it.idItem = offer.idOfferPK.idItem " + 
			"left join ItemPhotoData ip on ip.idItemPhotoData.idItem = it.idItem " + 
			"left join PhotoData ph on ph.idPhoto = ip.idItemPhotoData.idPhoto " + 
			"where offer.status = 'ACTIVO' ")
	Page<OfferData> findAllOfertasItems(Pageable pageable);
	
	@Query(value = "select offer from OfferData offer where offer.idOfferPK.idItem = :idItem and offer.status = 'ACTIVO'")
	Optional<OfferData> findActiveOfertaByidItem(@Param("idItem") Integer idItem);

}
