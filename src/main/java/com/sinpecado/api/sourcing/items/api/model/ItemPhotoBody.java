package com.sinpecado.api.sourcing.items.api.model;


import lombok.Data;

@Data
public class ItemPhotoBody {

	private byte[] fileItem;
	
	private String extension;

	private Integer idItem;
	
	private String url;
	
	private String name;
	
	private Integer idPhoto;
	
	private String status;
	
	private String type;
	
}
