package com.sinpecado.api.sourcing.items.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.sinpecado.api.general.photos.services.PhotosServices;
import com.sinpecado.api.sourcing.items.api.model.ItemBody;
import com.sinpecado.api.sourcing.items.api.model.ItemPhotoBody;
import com.sinpecado.api.sourcing.items.api.model.ItemsBody;
import com.sinpecado.api.sourcing.items.data.ItemData;
import com.sinpecado.api.sourcing.items.data.ItemPhotoData;
import com.sinpecado.api.sourcing.items.services.ItemsServices;
import com.sinpecado.api.sourcing.offers.api.model.OfferBody;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("items")
public class ItemsController {

	@Autowired
	ItemsServices itemsServices;
	
	@Autowired
	PhotosServices photosServices;
	
	//@PostMapping("/itemsPublishing" )
	@CrossOrigin(origins ={"*"})
	@RequestMapping(value = "/itemsPublishing" , method = RequestMethod.POST, consumes = { "multipart/form-data" })
	public ResponseEntity<?> itemsPublishing(@RequestParam("files") MultipartFile[] multipartFiles, @RequestParam("items") String items) throws Exception{
		
		Gson gson				= null;
		JSONObject response 	= null;
		ItemsBody itemsBody		= null;
		
		try {
			
			gson = new Gson();
			
			itemsBody = gson.fromJson(items, ItemsBody.class);
			
			for(ItemBody itemBody : itemsBody.getItems()) {
				
				List<ItemPhotoBody> listPhotos = new ArrayList<ItemPhotoBody>();
				
				for(MultipartFile multipartFile : multipartFiles) {
					
					ItemPhotoBody photos = new ItemPhotoBody();
					
					photos.setFileItem(multipartFile.getBytes());
					photos.setName(multipartFile.getOriginalFilename());
					
					listPhotos.add(photos);
				}
				
				itemBody.setPhotos(listPhotos);
			}
			
			response = itemsServices.itemsPublishing(itemsBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins ={"*"})
	@PostMapping("/getAllItems")
	public ResponseEntity<?> getAllItems(@RequestBody ItemBody itemBody) throws Exception{
		
		Page<ItemData> listItems 	= null;
		JSONObject response 		= null;
		
		try {
			
			listItems = itemsServices.findAllItemLimited(itemBody);
			
			for(ItemData item : listItems.getContent()) {
				
				ItemPhotoBody itemPhotoBody = new ItemPhotoBody();
				
				itemPhotoBody.setIdItem(item.getIdItem());
				
				List<ItemPhotoData> listItemPhotos = this.photosServices.listPhotos(itemPhotoBody);
				
				item.setListItemPhotos(listItemPhotos);
			}
			
			
			response = new JSONObject();
			response.put("success", true);
			response.put("data", listItems);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins ={"*"})
	@GetMapping("/get/getAllItems")
	public ResponseEntity<?> getAllOffers(@RequestParam(value = "description", defaultValue = "%") String description) throws Exception{
		
		Page<ItemData> listItems 	= null;
		ItemBody itemBody			= null;
		JSONObject response 		= null;
		
		try {
			
			itemBody = new ItemBody();
			itemBody.setDescription(description);
			
			listItems = itemsServices.findAllItemLimited(itemBody);
			
			response = new JSONObject();
			response.put("success", true);
			response.put("data", listItems);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@CrossOrigin(origins ={"*"})
	@PostMapping("/get/getAllItemsPhoto")
	public ResponseEntity<?> getAllOffersPhoto(@RequestBody ItemBody itemBody) throws Exception{
		
		JSONObject response = null;
		
		try {
			
			response = itemsServices.findAllItemsPhotoLimited(itemBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
}
