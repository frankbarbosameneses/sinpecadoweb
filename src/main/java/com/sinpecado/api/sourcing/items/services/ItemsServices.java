package com.sinpecado.api.sourcing.items.services;


import org.springframework.data.domain.Page;

import com.sinpecado.api.sourcing.items.api.model.ItemBody;
import com.sinpecado.api.sourcing.items.api.model.ItemPhotoBody;
import com.sinpecado.api.sourcing.items.api.model.ItemsBody;
import com.sinpecado.api.sourcing.items.data.ItemData;
import com.sinpecado.api.sourcing.items.data.ItemPhotoData;

import net.minidev.json.JSONObject;

public interface ItemsServices {
	
	JSONObject itemsPublishing(ItemsBody itemsBody) throws Exception;
	
	ItemData saveItems(ItemData items) throws Exception;

	ItemData findItemByCode(String code) throws Exception;
	
	ItemData findItemById(Integer id) throws Exception;
	
	Page<ItemData> findAllItemLimited(ItemBody itemBody) throws Exception;
	
	ItemPhotoData saveItemPhoto(ItemPhotoBody itemPhotoBody) throws Exception;
	
	JSONObject findAllItemsPhotoLimited(ItemBody itemBody) throws Exception;
	
}
