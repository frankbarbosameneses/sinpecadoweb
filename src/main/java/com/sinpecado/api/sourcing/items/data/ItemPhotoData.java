/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.sourcing.items.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@Table(name = "item_photo")

public class ItemPhotoData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    protected ItemPhotoDataPK idItemPhotoData;
    
    @Column(name = "fvcStatus")
    private String status;
    
    @Transient
    private String url;
    
    @Transient
    private String name;
    
    @Transient
    private Float ivaPrice;
    
    public ItemPhotoData() {
    	
    }
    
    public ItemPhotoData(String url, String name) {
    	this.url = url;
    	this.name = name;
    }
    
}
