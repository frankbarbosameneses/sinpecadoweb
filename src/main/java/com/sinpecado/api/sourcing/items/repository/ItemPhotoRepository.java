package com.sinpecado.api.sourcing.items.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sinpecado.api.sourcing.items.data.ItemPhotoData;
import com.sinpecado.api.sourcing.items.data.ItemPhotoDataPK;

@Repository
public interface ItemPhotoRepository extends CrudRepository<ItemPhotoData, ItemPhotoDataPK>{
	
	@Query(value = "select new com.sinpecado.api.sourcing.items.data.ItemPhotoData(ph.url, ph.name) "
					+ "from ItemPhotoData itp "
					+ "inner join ItemData it on it.idItem = itp.idItemPhotoData.idItem "
					+ "left join PhotoData ph on ph.idPhoto = itp.idItemPhotoData.idPhoto "
					+ "where itp.status like :status "
					+ "and itp.idItemPhotoData.idItem = :idItem "
					+ "and it.type like :type ")
	List<ItemPhotoData> findAllItemPhoto(@Param("status") String status, @Param("type") String type, @Param("idItem") Integer idItem, Pageable pageable);
	
	@Query(value = "select new com.sinpecado.api.sourcing.items.data.ItemPhotoData(ph.url, ph.name) "
			+ "from ItemPhotoData itp "
			+ "inner join ItemData it on it.idItem = itp.idItemPhotoData.idItem "
			+ "left join PhotoData ph on ph.idPhoto = itp.idItemPhotoData.idPhoto "
			+ "where itp.status like :status "
			+ "and itp.idItemPhotoData.idItem = :idItem "
			+ "and it.type like :type ")
	List<ItemPhotoData> findAllItemPhoto(@Param("status") String status, @Param("type") String type, @Param("idItem") Integer idItem);

}
