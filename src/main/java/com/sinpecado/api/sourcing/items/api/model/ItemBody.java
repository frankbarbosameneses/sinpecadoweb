package com.sinpecado.api.sourcing.items.api.model;

import java.util.List;

import com.sinpecado.api.general.resources.model.PagerBody;

import lombok.Data;
import net.minidev.json.JSONArray;

@Data
public class ItemBody {
	
	private Integer idItem;
    
    private String code;
    
    private String description;
    
    private String observation;
    
    private String status;
    
    private String registrationDate;
    
    private Float price;
    
    private Float iva;
    
    private String type;
    
    private String tags;
    
    private List<ItemPhotoBody> photos;
    
    private JSONArray files;
    
    private PagerBody pager;
    
    private String orderBy;
    
    private Boolean desc;
    
    private Boolean asc;

}
