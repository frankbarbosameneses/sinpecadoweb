package com.sinpecado.api.sourcing.items.repository;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sinpecado.api.sourcing.items.data.ItemData;
import com.sinpecado.api.sourcing.offers.data.OfferData;

@Repository
public interface ItemsRepository extends CrudRepository<ItemData, Integer>{

	ItemData findByCode(String code);
	
	Page<ItemData> findAll(Pageable pageable);
	
	@Query(value = "select it from ItemData it where it.code like :code and it.description like :description and it.status like :status")
	Page<ItemData> findAllItems(@Param("code") String code, @Param("description") String description, @Param("status") String status, Pageable pageable);
	
	@Query(value = "select it from ItemData it where it.type like :type")
	Page<ItemData> findAllItemsByType(@Param("type") String type, Pageable pageable);
	
	@Query(value = "select new com.sinpecado.api.sourcing.items.data.ItemData(it.code, it.description, it.price, it.iva, ph.url, ph.name) "
					+ "from ItemData it " + 
					"left join ItemPhotoData ip on ip.idItemPhotoData.idItem = it.idItem"
					+ " and ip.idItemPhotoData.idPhoto = :idPhoto " + 
					"left join PhotoData ph on ph.idPhoto = ip.idItemPhotoData.idPhoto " )
	Page<ItemData> findAllItemsPhoto(@Param("idPhoto") Integer idPhoto, Pageable pageable);
}
