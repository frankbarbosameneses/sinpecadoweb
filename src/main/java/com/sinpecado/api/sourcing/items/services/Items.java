package com.sinpecado.api.sourcing.items.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sinpecado.api.general.photos.data.PhotoData;
import com.sinpecado.api.general.photos.services.PhotosServices;
import com.sinpecado.api.general.resources.model.Path;
import com.sinpecado.api.sourcing.items.api.model.ItemBody;
import com.sinpecado.api.sourcing.items.api.model.ItemPhotoBody;
import com.sinpecado.api.sourcing.items.api.model.ItemsBody;
import com.sinpecado.api.sourcing.items.data.ItemData;
import com.sinpecado.api.sourcing.items.data.ItemPhotoData;
import com.sinpecado.api.sourcing.items.data.ItemPhotoDataPK;
import com.sinpecado.api.sourcing.items.repository.ItemPhotoRepository;
import com.sinpecado.api.sourcing.items.repository.ItemsRepository;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Service
public class Items implements ItemsServices {
	
	@Autowired
	ItemsRepository itemsRepository;
	
	@Autowired
	PhotosServices photosServices;
	
	@Autowired
	ItemPhotoRepository itemPhotoRepository;

	@Override
	public JSONObject itemsPublishing(ItemsBody itemsBody) throws Exception {
		
		JSONObject result 				= null;
		JSONArray arrayItems			= null;
		Gson gson						= new Gson();
		
		try {
			
			arrayItems = new JSONArray();

			for(ItemBody itemBody : itemsBody.getItems()) {
				
				ItemData item = null;
				
				if(itemBody.getIdItem() == null && itemBody.getCode() != null) {
					
					item = this.findItemByCode(itemBody.getCode());
					
					if(item != null && item.getIdItem() != null) {
						throw new Exception("El item ya existe en la BD con el codigo: " + itemBody.getCode());
					}else {
						
						item = gson.fromJson(gson.toJson(itemBody), ItemData.class) ;
						
						item.setRegistrationDate(new Timestamp(System.currentTimeMillis()));
						
						item = itemsRepository.save(item);
						
						arrayItems.add(item);
							
						itemBody.setIdItem(item.getIdItem());
					}
					
				}else {
					
					item = this.findItemById(itemBody.getIdItem());
					
					if(item.getCode() == null) {
						throw new Exception("El item no exite con id: " + itemBody.getIdItem());
					}
					
					if(itemBody.getDescription() != null) {
						item.setDescription(itemBody.getDescription());
					}
					
					if(itemBody.getObservation() != null) {
						item.setObservation(itemBody.getObservation());					
					}
					
					if(itemBody.getIva() != null) {
						item.setIva(itemBody.getIva());
					}
					
					if(itemBody.getPrice() != null) {
						item.setPrice(itemBody.getPrice());
					}
					
					if(itemBody.getType() != null) {
						item.setType(itemBody.getType());
					}
					
					if(itemBody.getStatus() != null) {
						item.setStatus(itemBody.getStatus());
					}
					
					if(itemBody.getTags() != null) {
						item.setTags(itemBody.getTags());
					}
					
					item = itemsRepository.save(item);
					
					arrayItems.add(item);
					
				}
				
				
				for(ItemPhotoBody photo : itemBody.getPhotos()) {
					
					photo.setIdItem(itemBody.getIdItem());
					
					PhotoData photoData = this.savePhoto(photo);
					
					photo.setIdPhoto(photoData.getIdPhoto());
					
					this.saveItemPhoto(photo);
				}
				
			}
			
			result = new JSONObject();
			result.put("success", true);
			result.put("items", arrayItems);
			
		} catch (Exception e) {
			
			result = new JSONObject();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		
		return result;
	}

	@Override
	public ItemData saveItems(ItemData items) throws Exception {
		
		try {
			return itemsRepository.save(items);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ItemData findItemByCode(String code) throws Exception {
		
		try {
			return itemsRepository.findByCode(code);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Page<ItemData> findAllItemLimited(ItemBody itemBody) throws Exception {
		
		Page<ItemData> listItems 	= null;
		Integer page				= null;
		Integer size				= null;
		
		try {
			
			page = itemBody.getPager() != null && itemBody.getPager().getPage() != null ? itemBody.getPager().getPage() : 0;
			size = itemBody.getPager() != null && itemBody.getPager().getSize() != null ? itemBody.getPager().getSize() : 10;
			
			Pageable pageable = PageRequest.of(page, size);
			
			listItems = itemsRepository.findAllItems( 	itemBody.getCode() != null ?  "%"+itemBody.getCode()+ "%" : "%", 
														itemBody.getDescription() != null ?  "%"+itemBody.getDescription()+ "%" :"%", 
														itemBody.getStatus() != null ?  "%"+itemBody.getStatus()+ "%" : "%", 
														pageable);
			
			return listItems;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ItemData findItemById(Integer id) throws Exception {
		
		Optional<ItemData> oItemData = null;
		
		try {
			
			oItemData = itemsRepository.findById(id);
			
			if(oItemData.isPresent()) {
				return oItemData.get();
			}else{
				return new ItemData();
			}
			
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	public PhotoData savePhoto(ItemPhotoBody itemPhotoBody) throws Exception{
		
		PhotoData photo = new PhotoData();
		
		try {
			
			itemPhotoBody = this.crearAnexo(itemPhotoBody);
			
			photo.setOrginalName(itemPhotoBody.getName());
			photo.setRegisterDate(new Timestamp(System.currentTimeMillis()));
			photo.setStatus("ACTIVE");
			photo.setName(itemPhotoBody.getName());
			photo.setUrl(itemPhotoBody.getUrl());
			
			photo = photosServices.savePhoto(photo);
			
			
			
			return photosServices.savePhoto(photo);
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	public ItemPhotoBody crearAnexo(ItemPhotoBody itemPhotoBody) throws Exception{
		
		Path basePath 		= null;
		File validarFolder 	= null;
		String realPath		= null;
		String path 		= null;
		String resourcePath	= null;
		String basepathUrl	= null;
		
		try {
			
			basePath = new Path();
	    	basePath.iniciarBasePath();
			
			path = basePath.getBasePath() + File.separator + "images" + File.separator; 
			
			resourcePath = "item" + File.separator + itemPhotoBody.getIdItem() + File.separator + "photos";
			
			realPath = path + resourcePath;
			
			validarFolder = new File(realPath);
			
			if(!validarFolder.exists()) {
				validarFolder.mkdirs();
			}
			
			//itemPhotoBody.setUrl(resourcePath);
			itemPhotoBody.setUrl(resourcePath);
			
			byte[] dataFile = itemPhotoBody.getFileItem();
			
			File filePhoto = new File(realPath + File.separator + itemPhotoBody.getName());
			
			OutputStream os = new FileOutputStream(filePhoto);
			
			os.write(dataFile);
			os.close();
			os.flush();
			
			return itemPhotoBody;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ItemPhotoData saveItemPhoto(ItemPhotoBody itemPhotoBody) throws Exception {
		
		ItemPhotoDataPK itemPhotoDataPK = null;
		ItemPhotoData itemPhotoData		= null;
		
		try {
			
			
			itemPhotoDataPK = new ItemPhotoDataPK();
			itemPhotoDataPK.setIdItem(itemPhotoBody.getIdItem());
			itemPhotoDataPK.setIdPhoto(itemPhotoBody.getIdPhoto());
			
			itemPhotoData = new ItemPhotoData();
			itemPhotoData.setIdItemPhotoData(itemPhotoDataPK);
			itemPhotoData.setStatus("ACTIVE");
			
			return itemPhotoRepository.save(itemPhotoData);
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	public JSONObject findAllItemsPhotoLimited(ItemBody itemBody) throws Exception {
		
		JSONObject result 			= null;
		Page<ItemData> listItems 	= null;
		Sort sort 					= null;
		Integer page				= null;
		Integer size				= null;
		String type					= null;
		
		try {
			
			page = itemBody.getPager() != null && itemBody.getPager().getPage() != null ? itemBody.getPager().getPage() : 0;
			size = itemBody.getPager() != null && itemBody.getPager().getSize() != null ? itemBody.getPager().getSize() : 10;
			type = itemBody.getType() != null ? itemBody.getType() : null;
			
			if(itemBody.getDesc() != null && itemBody.getDesc()) {
				sort = Sort.by("firstname").descending();
			}else if(itemBody.getAsc() != null && itemBody.getAsc()) {
				sort = Sort.by("firstname").ascending();
			}else {
				sort = Sort.by("registrationDate").descending();
			}
			
			Pageable pageable = PageRequest.of(page, size, sort);
			
			if(type != null) {
				listItems = itemsRepository.findAllItemsByType(type, pageable);
			}else {
				listItems = itemsRepository.findAll( pageable);
			}
			
			listItems.getContent().forEach(a-> {
				
				Integer pagePhoto				= 0;
				Integer sizePhoto				= 1;
				Sort sortPhoto 					= Sort.by("idItemPhotoData.idPhoto").ascending();
				
				Pageable pageablePhoto = PageRequest.of(pagePhoto, sizePhoto, sortPhoto);
				
				List<ItemPhotoData> listItemPhoto = itemPhotoRepository.findAllItemPhoto("%", "%", a.getIdItem(), pageablePhoto);
				
				if(listItemPhoto != null && listItemPhoto.size() > 0) {
					a.setUrl(listItemPhoto.get(0).getUrl()+ File.separator +listItemPhoto.get(0).getName());
				}
				
				a.setIvaPrice((a.getPrice() + (a.getPrice() * (a.getIva() / 100))));
				
			});
			
			result = new JSONObject();
			result.put("success", true);
			result.put("data", listItems);
			
			return result;
			
		} catch (Exception e) {
			throw e;
		}
	}

}
