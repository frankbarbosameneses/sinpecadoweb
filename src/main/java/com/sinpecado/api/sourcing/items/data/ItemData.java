/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.sourcing.items.data;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name = "item")

public class ItemData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "finIdItem")
    private Integer idItem;
    
    @Column(name = "fvcCode")
    private String code;
    
    @Column(name = "fvcDescription")
    private String description;
    
    @Column(name = "fvcObservation")
    private String observation;
    
    @Column(name = "fvcStatus")
    private String status;
    
    @Column(name = "fdtRegistrationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fdcPrice")
    private Float price;
    
    @Column(name = "fdcIVA")
    private Float iva;
    
    @Column(name = "fvcType")
    private String type;
    
    @Column(name = "fvcTags")
    private String tags;
    
    @Transient
    private Float ivaPrice;
    
    @Transient
    private String url;
    
    @Transient
    private List<ItemPhotoData> listItemPhotos;
    
    public ItemData() {
    	
    }
    
    public ItemData(String itemCode, String itemDescription, Float price, Float iva, String url, String namePhoto){
    	
    	this.code = itemCode;
    	this.description = itemDescription;
    	this.url = url + File.separator + namePhoto;
    	this.price = price;
    	this.iva = iva;
    	
    	this.ivaPrice = (price + (price * (iva / 100)));
    }
    
}
