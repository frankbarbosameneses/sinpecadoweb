package com.sinpecado.api.sourcing.items.api.model;

import java.util.List;

import lombok.Data;

@Data
public class ItemsBody {
	
	private String key;
	
	private List<ItemBody> items;

}
