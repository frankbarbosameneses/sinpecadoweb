package com.sinpecado.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.sinpecado.api.general.resources.model.Path;

@Configuration
public class CustomWebMvcAutoConfig implements WebMvcConfigurer {
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	
    	Path path = new Path();
    	
    	path.iniciarBasePath();
    	
        registry.addResourceHandler("/extrasources/**").addResourceLocations("file:"+path.getBasePath());
    }
}
