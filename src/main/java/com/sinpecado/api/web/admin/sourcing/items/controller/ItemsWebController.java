package com.sinpecado.api.web.admin.sourcing.items.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ItemsWebController {
	
	@GetMapping("/")
	public String initial(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "index";
	}
	
	@GetMapping("/admin")
	public String admin(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "admin";
	}
	
	@GetMapping("/items")
	public String items(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "items/items";
	}
	
	@GetMapping("/crearItems")
	public String crearItems(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "items/modalCrearItem";
	}
	
	@GetMapping("/filtrarItems")
	public String filtrarItems(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "items/modalFiltrarItem";
	}
	
	@GetMapping("/ofertas")
	public String ofertas(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "ofertas/ofertas";
	}
	
	@GetMapping("/ciudadesColombia")
	public String ciudadesColombia(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "json/ciudadesColombia.json";
	}
	
	@GetMapping("/checkout")
	public String checkout(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "checkout/checkout";
	}
	
	@GetMapping("/login")
	public String login(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "login/login";
	}
	
	@GetMapping("/registro")
	public String registro(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "login/register";
	}
	
	//successShop
	@GetMapping("/ventaExitosa")
	public String ventaExitosa(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "shop/successShop";
	}
	
	//pendingShop
	@GetMapping("/ventaPendiente")
	public String ventaPendiente(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "shop/pendingShop";
	}
	
	@GetMapping("/ventaFallida")
	public String ventaFallida(@RequestParam(name="name", required=false, defaultValue="Frank") String name, Model model) {
		model.addAttribute("name", name);
		return "shop/failureShop";
	}
	

}
