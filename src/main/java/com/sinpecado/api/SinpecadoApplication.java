package com.sinpecado.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinpecadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinpecadoApplication.class, args);
	}

}
