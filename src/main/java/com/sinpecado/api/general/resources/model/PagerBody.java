package com.sinpecado.api.general.resources.model;

import lombok.Data;

@Data
public class PagerBody {
	
	Integer page;
	Integer size;

}
