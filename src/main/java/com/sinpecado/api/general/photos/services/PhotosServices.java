package com.sinpecado.api.general.photos.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sinpecado.api.general.photos.data.PhotoData;
import com.sinpecado.api.sourcing.items.api.model.ItemPhotoBody;
import com.sinpecado.api.sourcing.items.data.ItemPhotoData;

import net.minidev.json.JSONArray;


public interface PhotosServices {
	
	PhotoData savePhoto(PhotoData photo) throws Exception;
	
	List<ItemPhotoData> photoItemPublishing(JSONArray arrayPhotos);
	
	List<ItemPhotoData> listPhotos(ItemPhotoBody itemPhotoBody) throws Exception;
	
}
