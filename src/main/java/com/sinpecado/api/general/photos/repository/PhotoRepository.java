package com.sinpecado.api.general.photos.repository;

import org.springframework.data.repository.CrudRepository;

import com.sinpecado.api.general.photos.data.PhotoData;

public interface PhotoRepository extends CrudRepository<PhotoData, String> {

}
