/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.general.photos.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;

@Data
@Entity
@Table(name = "photo")
public class PhotoData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "finIdPhoto")
    private Integer idPhoto;
    
    @Column(name = "fvcName")
    private String name;
    
    @Column(name = "fvcUrl")
    private String url;
    
    @Column(name = "fvcStatus")
    private String status;
    
    @Column(name = "fvcOrginalName")
    private String orginalName;
    
    @Column(name = "fvcExtension")
    private String extension;
    
    @Column(name = "fvcType")
    private String type;
    
    @Column(name = "fdtRegisterDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerDate;
    
}
