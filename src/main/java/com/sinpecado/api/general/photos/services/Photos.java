package com.sinpecado.api.general.photos.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinpecado.api.general.photos.data.PhotoData;
import com.sinpecado.api.general.photos.repository.PhotoRepository;
import com.sinpecado.api.sourcing.items.api.model.ItemPhotoBody;
import com.sinpecado.api.sourcing.items.data.ItemPhotoData;
import com.sinpecado.api.sourcing.items.repository.ItemPhotoRepository;

import net.minidev.json.JSONArray;

@Service
public class Photos implements PhotosServices {
	
	@Autowired
	PhotoRepository photoRepository;
	
	@Autowired
	ItemPhotoRepository itemPhotoRepository;

	@Override
	public PhotoData savePhoto(PhotoData photo) throws Exception {
		
		try {
			return photoRepository.save(photo);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ItemPhotoData> photoItemPublishing(JSONArray arrayPhotos) {
		
		List<ItemPhotoData> listItemPhoto = null;
		
		try {
			
			listItemPhoto = new ArrayList<ItemPhotoData>();
			
			if(arrayPhotos.size() > 0) {
				
				for(Object object : arrayPhotos) {
					FileItem fileItem = (FileItem) object;
					
					
				}
				
			}
			
			
			return listItemPhoto;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ItemPhotoData> listPhotos(ItemPhotoBody itemPhotoBody) throws Exception{

		List<ItemPhotoData> listItemPhoto = null;
		
		try {
			
			if(itemPhotoBody.getIdItem() == null) {
				throw new Exception("Es necesario el id del Item para la busqueda");
			}
			
			if(itemPhotoBody.getStatus() == null) {
				itemPhotoBody.setStatus("%");
			}
			
			if(itemPhotoBody.getType() == null) {
				itemPhotoBody.setType("%");
			}
			
			listItemPhoto = this.itemPhotoRepository.findAllItemPhoto(itemPhotoBody.getStatus(), itemPhotoBody.getType(), itemPhotoBody.getIdItem());
			
			return listItemPhoto;
			
		} catch (Exception e) {
			throw e;
		}
		
	}

}
