package com.sinpecado.api.general.email.services;

import com.sinpecado.api.general.email.api.model.EmailBody;
import com.sinpecado.api.general.email.data.EmailTemplateData;

public interface EmailServices {
	
	public void sendSimpleMessage (EmailBody emailBody) throws Exception;
	
	public EmailTemplateData findByCodeAndLanguage(String code, String language) throws Exception;

}
