package com.sinpecado.api.general.email.api.model;

import lombok.Data;

@Data
public class EmailBody {

	private String subject;
	
	private String from;
	
	private String to;
	
	private String template;
	
	private String copy;
	
	private String emailCode;
	
	private String emailLenguage;
}
