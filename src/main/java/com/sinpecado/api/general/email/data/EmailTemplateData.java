/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.general.email.data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@Table(name = "email_template")

public class EmailTemplateData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "finIdEmailTemplate")
    private Integer idEmailTemplate;
    
    @Column(name = "fvcCode")
    private String code;
    
    @Column(name = "fvcBody")
    private String body;
    
    @Column(name = "fvcSubject")
    private String subject;
    
    @Column(name = "fvcLanguage")
    private String language;
    
    @Column(name = "fvcDescription")
    private String description;
    
}
