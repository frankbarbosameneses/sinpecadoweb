package com.sinpecado.api.general.email.services;

import java.util.Optional;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.sinpecado.api.general.email.api.model.EmailBody;
import com.sinpecado.api.general.email.data.EmailTemplateData;
import com.sinpecado.api.general.email.repository.EmailTemplateRepository;

@Service
public class Email implements EmailServices {

	@Autowired
    private JavaMailSender emailSender;
	
	@Autowired
	private EmailTemplateRepository emailTemplateRepository;
	
	@Override
	public void sendSimpleMessage(EmailBody emailBody) throws Exception {
		
		EmailTemplateData emailTemplateData = null;
		MimeMessage message 				= null;
		MimeMessageHelper helper 			= null;
		
		try {
			
			
			emailTemplateData = findByCodeAndLanguage(emailBody.getEmailCode(), emailBody.getEmailLenguage());
			
			message = emailSender.createMimeMessage();
			helper = new MimeMessageHelper(message);
			
			helper.setFrom(emailBody.getFrom());
			helper.setTo(emailBody.getTo()); 
			helper.setSubject(emailTemplateData.getSubject()); 
			helper.setText(emailTemplateData.getBody(), true);
	        
	        emailSender.send(message);
	        
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public EmailTemplateData findByCodeAndLanguage(String code, String language) throws Exception {

		Optional<EmailTemplateData> opEmailTemplate = null;
		EmailTemplateData emailTemplateData 		= null;
		
		try {
			
			opEmailTemplate = emailTemplateRepository.findByCodeAndLanguage(code, language);
			
			if(opEmailTemplate.isPresent()) {
				emailTemplateData = opEmailTemplate.get();
			}else {
				emailTemplateData = new EmailTemplateData();
			}
			
			return emailTemplateData;
			
		} catch (Exception e) {
			throw e;
		}
		
	}

}
