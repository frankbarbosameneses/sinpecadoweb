package com.sinpecado.api.general.email.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.sinpecado.api.general.email.data.EmailTemplateData;

public interface EmailTemplateRepository extends CrudRepository<EmailTemplateData, Integer> {
	
	Optional<EmailTemplateData> findByCodeAndLanguage(String code, String language);

}
