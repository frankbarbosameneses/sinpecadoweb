package com.sinpecado.api.admin.users.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sinpecado.api.admin.users.api.model.AddressBody;
import com.sinpecado.api.admin.users.api.model.UserBody;
import com.sinpecado.api.admin.users.data.AddressesData;
import com.sinpecado.api.admin.users.data.UserData;
import com.sinpecado.api.admin.users.services.UsersServices;
import com.sinpecado.api.general.email.api.model.EmailBody;
import com.sinpecado.api.general.email.services.EmailServices;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("users")
public class UsersController {
	
	@Autowired
	UsersServices userServices;
	
	@Autowired
	EmailServices emailServices;
	
	@PostMapping("/userRegister")
	public ResponseEntity<?> userRegister(@RequestBody UserBody userBody) throws Exception{
		
		JSONObject response = null;
		
		try {
			
			response = userServices.userRegister(userBody);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getCause());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@PostMapping("/addressRegister")
	public ResponseEntity<?> addressRegister(@RequestBody AddressBody addressBody) throws Exception{
		
		AddressesData addressesData = null;
		JSONObject response 		= null;
		
		try {
			
			addressesData = userServices.addressRegister(addressBody);
			
			response = new JSONObject();
			response.put("success", true);
			response.put("data", addressesData);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getCause());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@PostMapping("/userValidate")
	public ResponseEntity<?> userValidate(@RequestBody UserBody userBody) throws Exception{
		
		EmailBody emailBody = null;
		JSONObject response = null;
		String htmlEmail	= null;
		
		try {
			
			UserData userData = userServices.userValidate(userBody);
			
			if(userData == null || userData.getIdUser() == null) {
				
				htmlEmail = "<html>";
				htmlEmail += "<body> <div style=\"color:#6ebcff\"><ul> <li> datos</li> <li> datos 2</li></ul> </div></body> </html>";
				
				emailBody = new EmailBody();
				emailBody.setFrom("frankbarbosameneses@gmail.com");
				emailBody.setTo("frankmeneses10@gmail.com");
				emailBody.setSubject("validacion email");
				emailBody.setTemplate(htmlEmail);
				emailBody.setEmailLenguage("es");
				emailBody.setEmailCode("REG-ADD-001");
				
				emailServices.sendSimpleMessage(emailBody);
				
				throw new Exception("Usuario no encongtrado en nuestra aplicacion.");
			}
			
			response = new JSONObject();
			response.put("success", true);
			response.put("data", userData);
			
		} catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getCause());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}
	
	@PostMapping("/getUser")
	public ResponseEntity<?> getUser(@RequestBody UserBody userBody) throws Exception{
		
		List<AddressesData> addresses = null;
		Gson gson = new Gson();
		JSONObject response = null;
		
		try {
			
			UserData userData = userServices.findByEmail(userBody.getEmail());
			
			userBody = gson.fromJson(gson.toJson(userData), UserBody.class);
			userBody.setPassword(null);
			
			addresses = userServices.findAddressesByIdUser(userBody.getIdUser());
			
			userBody.setAddresses(addresses);
			
			response = new JSONObject();
			response.put("success", true);
			response.put("data", userBody);
			
		}catch (Exception e) {
			response = new JSONObject();
			response.put("success", false);
			response.put("error", e.getCause());
		}
		
		return new ResponseEntity<JSONObject>(response, HttpStatus.OK);
	}

}
