/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.admin.users.data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@Entity
@Table(name = "addresses")

public class AddressesData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "finIdAddresses")
    private Integer idAddresses;
    
    @Column(name = "fvcAddress")
    private String address;
    
    @Column(name = "fvcDescription")
    private String description;
    
    @Column(name = "fvcObservation")
    private String observation;
    
    @Column(name = "fvcPhone")
    private String phone;
    
    @Column(name = "fvcStatus")
    private String status;
    
    @Column(name = "finIdUser")
    private Integer idUser;
    
    @Column(name = "fvcCity")
    private String city;
    
    @Column(name = "fvcDescriptionCity")
    private String descriptionCity;
    
}
