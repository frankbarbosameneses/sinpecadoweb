package com.sinpecado.api.admin.users.services;

import java.util.List;

import com.sinpecado.api.admin.users.api.model.AddressBody;
import com.sinpecado.api.admin.users.api.model.UserBody;
import com.sinpecado.api.admin.users.data.AddressesData;
import com.sinpecado.api.admin.users.data.UserData;
import com.sinpecado.api.sourcing.items.api.model.ItemBody;

import net.minidev.json.JSONObject;

public interface UsersServices {

	JSONObject userRegister(UserBody userBody) throws Exception;
	
	UserData userValidate(UserBody userBody) throws Exception;
	
	UserData saveUser(UserData userData) throws Exception;
	
	AddressesData saveAddress(AddressesData addressesData) throws Exception;
	
	AddressesData addressRegister(AddressBody addressBody) throws Exception;
	
	UserData findUserByEmailOrIdentification(String email, String identification) throws Exception;
	
	UserData findByEmail(String email) throws Exception;
	
	JSONObject findAllUserLimited(UserBody userBody) throws Exception;
	
	List<AddressesData> findAddressesByIdUser(Integer idUser) throws Exception;
	
}
