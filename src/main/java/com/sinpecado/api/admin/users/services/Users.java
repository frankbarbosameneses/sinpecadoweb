package com.sinpecado.api.admin.users.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sinpecado.api.admin.users.api.model.AddressBody;
import com.sinpecado.api.admin.users.api.model.UserBody;
import com.sinpecado.api.admin.users.data.AddressesData;
import com.sinpecado.api.admin.users.data.UserData;
import com.sinpecado.api.admin.users.repository.AddressesRespository;
import com.sinpecado.api.admin.users.repository.UserRepository;
import com.sinpecado.api.sourcing.items.data.ItemData;

import net.minidev.json.JSONObject;

@Service
public class Users implements UsersServices {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AddressesRespository addressesRespository;

	@Override
	public JSONObject userRegister(UserBody userBody) throws Exception {
		
		Gson gson						= new Gson();
		JSONObject result 				= null;
		UserData user 					= null;
		
		try {
			
			user = this.findUserByEmailOrIdentification(userBody.getEmail(), userBody.getIdentification());
			
			if(user == null || user.getIdUser() == null) {
				
				user = gson.fromJson(gson.toJson(userBody), UserData.class) ;
				
				if(user.getLogin() == null) {
					user.setLogin(user.getEmail());
				}
				
				user.setRegistrationDate(new Timestamp(System.currentTimeMillis()));
				user.setStatus("PENDING");
				user.setType("USER");
				
				user = this.saveUser(user);
				
				
			}else {
				
				throw new Exception("Usuario ya registrado.");
			}
			
			result = new JSONObject();
			result.put("success", true);
			result.put("data", user);
			
		} catch (Exception e) {
			
			result = new JSONObject();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		
		return result;
	}

	@Override
	public UserData userValidate(UserBody userBody) throws Exception {
		
		UserData userData = null;

		try {
			
			if(userBody.getEmail() != null && userBody.getPassword() != null) {
				
				userData = this.userRepository.findByEmailAndPassword(userBody.getEmail(), userBody.getPassword());
				
			}else if(userBody.getPhone() != null && userBody.getPassword() != null) {
				
				userData = this.userRepository.findByPhoneAndPassword(userBody.getPhone(), userBody.getPassword());
			}
			
			return userData;
			
		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	public UserData saveUser(UserData userData) throws Exception {
		
		try {
			return userRepository.save(userData);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public AddressesData saveAddress(AddressesData addressesData) throws Exception {
		
		try {
			return addressesRespository.save(addressesData);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public AddressesData addressRegister(AddressBody addressBody) throws Exception {
		
		Gson gson						= new Gson();
		JSONObject result 				= null;
		UserData user					= null;
		AddressesData addresses			= null;
		
		try {
			
			user = this.findByEmail(addressBody.getEmailUser());
			
			if(user == null || user.getIdUser() == null) {
				throw new Exception("Usuario no existe.");
			}else {
				
				addressBody.setIdUser(user.getIdUser());
				
				addresses = gson.fromJson(gson.toJson(addressBody), AddressesData.class) ;
				
				addresses.setStatus("ACTIVE");
				
				addresses = this.saveAddress(addresses);
				
				return addresses;
			}
			
		} catch (Exception e) {
			
			throw e;
		}
	}

	@Override
	public UserData findUserByEmailOrIdentification(String email, String identification) throws Exception {
		
		try {
			return userRepository.findByEmailOrIdentification(email, identification);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public UserData findByEmail(String email) throws Exception {
		
		try {
			return userRepository.findByEmail(email);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public JSONObject findAllUserLimited(UserBody userBody) throws Exception {
		
		JSONObject result 			= null;
		Page<ItemData> listItems 	= null;
		Integer page				= null;
		Integer size				= null;
		
		try {
			
			page = userBody.getPager() != null && userBody.getPager().getPage() != null ? userBody.getPager().getPage() : 0;
			size = userBody.getPager() != null && userBody.getPager().getSize() != null ? userBody.getPager().getSize() : 10;
			
			Pageable pageable = PageRequest.of(page, size);
			
			listItems = userRepository.findAllUsers( 	userBody.getLogin() != null ?  "%"+userBody.getLogin()+ "%" : "%", 
														userBody.getFirstName() != null ?  "%"+userBody.getFirstName()+ "%" :"%",
														userBody.getLastName() != null ?  "%"+userBody.getLastName()+ "%" :"%", 
														userBody.getStatus() != null ?  "%"+userBody.getStatus()+ "%" : "%", 
														userBody.getEmail() != null ?  "%"+userBody.getEmail()+ "%" : "%", 
														pageable);
			
			result = new JSONObject();
			result.put("success", true);
			result.put("data", listItems);
			
			return result;
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<AddressesData> findAddressesByIdUser(Integer idUser) throws Exception {
		
		try {
			
			return addressesRespository.findByIdUser(idUser);
			
		} catch (Exception e) {
			throw e;
		}
	}

}
