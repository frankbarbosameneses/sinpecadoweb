package com.sinpecado.api.admin.users.api.model;

import lombok.Data;

@Data
public class AddressBody {
	
	private Integer idAddresses;
	private String address;
	private String description;
	private String observation;
	private String phone;
	private Integer idUser;
	private String emailUser;
	private Boolean futurePurchases;
	private String city;

}
