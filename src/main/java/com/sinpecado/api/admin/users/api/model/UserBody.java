package com.sinpecado.api.admin.users.api.model;

import java.util.List;

import com.sinpecado.api.admin.users.data.AddressesData;
import com.sinpecado.api.general.resources.model.PagerBody;

import lombok.Data;

@Data
public class UserBody {
	
	private Integer idUser;
	
	private String login;

	private String email;
	
	private String firstName;
	
	private String lastName;
	
	private String identification;
	
	private String phone;
	
	private String password;
	
	private String status;
	
	private PagerBody pager;
	
	private List<AddressesData> addresses;
	
}
