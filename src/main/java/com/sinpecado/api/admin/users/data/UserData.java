/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinpecado.api.admin.users.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;

@Data
@Entity
@Table(name = "user")

public class UserData implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "finIdUser")
    private Integer idUser;
    
    @Basic(optional = false)
    @Column(name = "fvcLogin")
    private String login;
    
    @Column(name = "fvcPassword")
    private String password;
    
    @Column(name = "fvcFirstName")
    private String firstName;
    
    @Column(name = "fvcLastName")
    private String lastName;
    
    @Column(name = "fvcEmail")
    private String email;
    
    @Column(name = "fvcPhone")
    private String phone;
    
    @Basic(optional = false)
    @Column(name = "fvcType")
    private String type;
    
    @Basic(optional = false)
    @Column(name = "fvcStatus")
    private String status;
    
    @Basic(optional = false)
    @Column(name = "fdtRegistrationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    
    @Column(name = "fvcIdentification")
    private String identification;
}
