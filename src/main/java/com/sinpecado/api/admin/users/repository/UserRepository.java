package com.sinpecado.api.admin.users.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sinpecado.api.admin.users.data.UserData;
import com.sinpecado.api.sourcing.items.data.ItemData;

@Repository
public interface UserRepository extends CrudRepository<UserData, String>{

	UserData findByEmailOrIdentification(String email, String identification);
	
	UserData findByEmailAndPassword(String email, String identification);
	
	UserData findByPhoneAndPassword(String email, String identification);
	
	UserData findByEmail(String email);
	
	@Query(value = "select us "
					+ "from UserData us "
					+ "where us.login like :login "
					+ "and us.firstName like :firstName "
					+ "and us.lastName like :lastName "
					+ "and us.status like :status "
					+ "and us.email like :email")
	Page<ItemData> findAllUsers(@Param("login") String login, 
								@Param("firstName") String firstName, 
								@Param("lastName") String lastName, 
								@Param("status") String status,
								@Param("email") String email, 
								Pageable pageable);
}
