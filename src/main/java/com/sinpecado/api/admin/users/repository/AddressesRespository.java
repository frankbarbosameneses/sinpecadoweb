package com.sinpecado.api.admin.users.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sinpecado.api.admin.users.data.AddressesData;

@Repository
public interface AddressesRespository extends CrudRepository<AddressesData, String>{
	
	List<AddressesData> findByIdUser(Integer idUser);

}
